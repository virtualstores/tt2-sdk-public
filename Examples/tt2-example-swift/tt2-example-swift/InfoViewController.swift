//
//  InfoViewController.swift
//  tt2-example-swift
//
//  Created by Jesper Lundqvist on 2020-06-12.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit
import Domain
import TT2_SDK

class InfoViewController: UIViewController {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet var icon: UIImageView!
    @IBOutlet var navigationButton: AnimatedButton!
    
    var pointOfInterest: PointOfInterest!
    var onCancel: (() -> Void)?
    var onNavigation: (() -> Void)?
    
    var positionStateObserverToken: Any?
    
    var isNavigating: Bool = false {
        didSet {
            navigationButton.backgroundColor = isNavigating ? .systemRed : .systemGreen
            navigationButton.setTitle(isNavigating ? "Stop navigation" : "Navigate to item", for: .normal)
            navigationButton.setImage(isNavigating ? UIImage(systemName: "arrow.up.right.diamond") : UIImage(systemName: "arrow.up.right.diamond.fill"), for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = pointOfInterest.name
        subtitleLabel.text = "\(pointOfInterest.subtitle)"
        icon.kf.setImage(with: pointOfInterest.imageUrl, placeholder: UIImage(systemName: "mappin.circle.fill"))
        
        TT2.shared.assistant.add(pointOfInterest)
        
        isNavigating = TT2.shared.assistant.pointOfInterestOverride == pointOfInterest
        
        if !TT2.shared.positionManager.isUpdatingPosition {
            navigationButton.isHidden = true
            navigationButton.setTitle("Scan code to begin navigation", for: .normal)
            navigationButton.backgroundColor = .systemBlue
            navigationButton.setImage(nil, for: .normal)
            positionStateObserverToken = TT2.shared.positionManager.observeState { [weak self] state in
                switch state {
                case .ready:
                    self?.startNavigation()
                default:
                    break
                }
            }
        }
        else {
            navigationButton.isHidden = true
            startNavigation()
        }
    }
    
    @IBAction func didPressCloseButton(_ sender: Any) {
        TT2.shared.navigationManager.stopNavigation()
        TT2.shared.assistant.clearOverride()
        TT2.shared.assistant.delete(pointOfInterest)
        onCancel?()
        dismiss(animated: true)
    }
    
    @IBAction func didPressNavigationButton(_ sender: Any) {
        print("Here")
        print(pointOfInterest)
        if isNavigating {
            TT2.shared.navigationManager.stopNavigation()
            TT2.shared.assistant.clearOverride()
            TT2.shared.assistant.add(pointOfInterest)
            isNavigating = false
        }
        else if TT2.shared.positionManager.isUpdatingPosition {
            startNavigation()
        }
        else {
            let barcodeScanner = storyboard!.instantiateViewController(identifier: "barcode_scanner")
            present(barcodeScanner, animated: true)
        }
    }
    
    func startNavigation() {
        print(pointOfInterest)
        isNavigating = true
        TT2.shared.assistant.clear()
        TT2.shared.assistant.clearOverride()
        TT2.shared.assistant.override(with: pointOfInterest)
        TT2.shared.navigationManager.stopNavigation()
        TT2.shared.navigationManager.startNavigation()
        onNavigation?()
    }
}
