//
//  PassThroughView.swift
//  tt2-example-swift
//
//  Created by Théodore Roos on 2021-01-14.
//  Copyright © 2021 Virtual Stores. All rights reserved.
//

import UIKit

class PassThroughView: UIView {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for subview in subviews {
            if !subview.isHidden && subview.isUserInteractionEnabled && subview.point(inside: convert(point, to: subview), with: event) {
                return true
            }
        }
        return false
    }
}
