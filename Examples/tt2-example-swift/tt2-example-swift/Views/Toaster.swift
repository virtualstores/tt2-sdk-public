//
//  Toaster.swift
//  Hemkop
//
//  Created by Jesper Lundqvist on 2020-02-27.
//  Copyright © 2020 VirtualStores. All rights reserved.
//

import UIKit
import Domain

class Toast: UIView, NibLoadable {
    @IBOutlet var icon: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var subtitle: UILabel!
    @IBOutlet var dropShadowView: DropShadowView!
    @IBOutlet var roundedView: RoundedView!
    
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.loadFromNib()
    }
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		self.loadFromNib()
	}
}

class Toaster: UIView {
	static var shared: Toaster?
	
	private var toast: Toast!
	private var isToasting = false
	private var hidingToast = true
	private var toastDismissTimer = Timer()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		initComponents()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		initComponents()
	}
	
	static func createShared(from view: UIView) {
		let sharedToaster = Toaster()
		view.add(view: sharedToaster) {
			$0.edges.equalToSuperview()
		}
		shared = sharedToaster
	}
	
	static func createLocal(from view: UIView) -> Toaster {
		let localToaster = Toaster()
		view.add(view: localToaster) {
			$0.edges.equalToSuperview()
		}
		return localToaster
	}
	
	private func initComponents() {
		let toastHeight = 60.0
		
		toast = Toast()
		toast.isUserInteractionEnabled = false
		//toast.roundedView.cornerRadius = CGFloat(toastHeight / 2)
		
		add(view: toast) {
			$0.topMargin.equalToSuperview(8)
			$0.width.equalTo(250)
			$0.height.equalTo(toastHeight)
			$0.centerX.equalToSuperview()
		}
		
		let tap = UITapGestureRecognizer(target: self, action: #selector(didPressToast))
		toast.addGestureRecognizer(tap)
		
		setHidden(true)
	}
	
	@objc private func didPressToast() {
		dismiss()
	}
	
	override func layoutSubviews() {
		setHidden(hidingToast)
	}
	
	override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
		for view in self.subviews {
			if view.isUserInteractionEnabled && view.point(inside: convert(point, to: view), with: event) {
				return true
			}
		}
		
		return false
	}
	
	private func setHidden(_ hidden: Bool) {
		if hidden {
			hidingToast = true
			
			toast.alpha = 0.0
			toast.bounds.center.y = (toast.bounds.height/2.0) + 2.0*toast.bounds.height
			toast.isUserInteractionEnabled = false
		}
		else {
			toast.bounds.center.y = toast.bounds.height/2.0
			toast.alpha = 1.0
			toast.isUserInteractionEnabled = true
			
			hidingToast = false
		}
	}
	
	private func present(completion: (() -> Void)? = nil) {
		isToasting = true
		UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
			self.setHidden(false)
		}, completion: { _ in
			completion?()
		})
	}
	
	private func dismiss(completion: (() -> Void)? = nil) {
		toastDismissTimer.invalidate()
		toast.isUserInteractionEnabled = false
		UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
			self.setHidden(true)
		}, completion: { _ in
			self.isToasting = false
			completion?()
		})
	}
	
	private func updateToast(title: String, subtitle: String?, iconUrl: URL?) {
		toast.title.text = title
		
        if let subtitle = subtitle {
			toast.subtitle.isHidden = false
            toast.subtitle.text = subtitle
		}
		else {
            toast.subtitle.isHidden = true
        }
		
		if let iconUrl = iconUrl {
			toast.icon.isHidden = false
			toast.icon.kf.setImageWithFallback(with: iconUrl)
		}
		else {
			toast.icon.isHidden = true
		}
	}
	
	public func toast(pointOfInterest: PointOfInterest, state: AssistantObserverState) {
		let subtitle: String
		
		switch state {
		case .added:
			subtitle = "Lade till på kartan"
		case .removed:
			subtitle = "Tog bort från kartan"
		case .edited:
			subtitle = "Uppdaterade på kartan"
		}
		
		toast(title: pointOfInterest.name, subtitle: subtitle, iconUrl: pointOfInterest.imageUrl)
	}
	
	public func toast(pointsOfInterest: [PointOfInterest], state: AssistantObserverState) {
		if pointsOfInterest.count == 1, let poi = pointsOfInterest.first {
			Toaster.shared?.toast(pointOfInterest: poi, state: state)
		}
		else if pointsOfInterest.count > 1 {
			let title: String
			
			switch state {
			case .added:
				title = "Lade till \(pointsOfInterest.count) varor på kartan"
			case .removed:
				title = "Tog bort \(pointsOfInterest.count) varor på kartan"
			case .edited:
				title = "Uppdaterade \(pointsOfInterest.count) varor på kartan"
			}
			
			toast(title: title, subtitle: nil, iconUrl: nil)
		}
	}
	
    public func toast(title: String, subtitle: String?, iconUrl: URL?) {
		if isToasting {
			toastDismissTimer.invalidate()
			self.dismiss {
				self.updateToast(title: title, subtitle: subtitle, iconUrl: iconUrl)
				self.present()
			}
		}
		else {
			updateToast(title: title, subtitle: subtitle, iconUrl: iconUrl)
			present()
		}
		
		toastDismissTimer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) {_ in
			self.dismiss()
		}
	}
}
