//
//  DropShadowView.swift
//  Hemkop
//
//  Created by Jesper Lundqvist on 2019-11-15.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import UIKit

@IBDesignable
class DropShadowView: UIView {
	@IBInspectable var shadowOffset: CGSize = CGSize(width: 0.0, height: 0.0) {
		didSet {
			self.layer.masksToBounds = false
			self.layer.shadowOffset = self.shadowOffset
		}
	}
	
	@IBInspectable var shadowRadius: CGFloat = 5.0 {
		didSet {
			self.layer.masksToBounds = false
			self.layer.shadowRadius = self.shadowRadius
		}
	}
	
	@IBInspectable var shadowOpacity: Float = 0.2 {
		didSet {
			self.layer.masksToBounds = false
			self.layer.shadowOpacity = self.shadowOpacity
		}
	}
	
	@IBInspectable var shadowColor: UIColor = UIColor.black {
		didSet {
			self.layer.masksToBounds = false
			self.layer.shadowColor = self.shadowColor.cgColor
		}
	}
}
