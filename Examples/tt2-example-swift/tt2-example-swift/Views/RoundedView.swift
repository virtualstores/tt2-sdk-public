//
//  RoundedView.swift
//  Hemkop
//
//  Created by Jesper Lundqvist on 2019-11-08.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedView: UIView {
	@IBInspectable var borderWidth: CGFloat = 4.0 {
		didSet {
			self.layer.masksToBounds = true
			self.layer.borderWidth = self.borderWidth
		}
	}
	
	@IBInspectable var borderColor: UIColor = .black {
		didSet {
			self.layer.masksToBounds = true
			self.layer.borderColor = self.borderColor.cgColor
		}
	}
	
	@IBInspectable var cornerRadius: CGFloat = 8.0 {
		didSet {
			self.layer.cornerRadius = self.cornerRadius
		}
	}
}
