//
//  NibLoadable.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-11-08.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import UIKit

protocol NibLoadable: class {
	static var nibName: String { get }
	static var nib: UINib { get }
}

extension NibLoadable {
	
	static var nibName: String {
		return String(describing: Self.self)
	}
	
	static var nib: UINib {
		let bundle = Bundle(for: Self.self)
		return UINib(nibName: Self.nibName, bundle: bundle)
	}
}

extension NibLoadable where Self: UIView {
	
	func loadFromNib() {
		guard let view = Self.nib.instantiate(withOwner: self, options: nil).first as? UIView else {
			fatalError("Error loading \(Self.nibName) from nib")
		}
		
		self.add(view: view) {
			$0.edges.equalToSuperview()
		}
	}
}
