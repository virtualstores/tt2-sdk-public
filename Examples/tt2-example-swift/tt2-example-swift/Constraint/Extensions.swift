//
//  Extensions.swift
//  Func
//
//  Created by Philip Fryklund on 25/Dec/17.
//

import UIKit



public extension UIView {
	
	var lac: ConstraintView {
		return ConstraintView(item: self)
	}
}


public extension UILayoutGuide {
	
	var lac: ConstraintLayoutGuide {
		return ConstraintLayoutGuide(item: self)
	}
}


public extension UILayoutSupport {
	
	var lac: ConstraintLayoutSupport {
		return ConstraintLayoutSupport(item: self)
	}
}


public extension UIView {
	
	func add(view: UIView, instructions: (ConstraintView) -> Void) {
		self.addSubview(view)
		view.lac.make(instructions)
	}
    
    func setHeight(_ height: CGFloat) {
        if let constraint = self.constraints.first(where: { $0.firstAttribute == .height && $0.relation == .equal }) {
            constraint.constant = height
        }
    }
    
    func findViewController() -> UIViewController? {
        if let nextResponder = self.next as? UIViewController {
            return nextResponder
        } else if let nextResponder = self.next as? UIView {
            return nextResponder.findViewController()
        } else {
            return nil
        }
    }
}

public extension UIStackView {
	
	func add(arrangedView: UIView, instructions: (ConstraintView) -> Void) {
		self.addArrangedSubview(arrangedView)
		arrangedView.lac.make(instructions)
	}
	
	func insert(arrangedView: UIView, at index: Int, instructions: (ConstraintView) -> Void) {
		self.insertArrangedSubview(arrangedView, at: index)
		arrangedView.lac.make(instructions)
	}
	
	func insert(arrangedView: UIView, before beforeView: UIView, instructions: (ConstraintView) -> Void) {
		self.insertSubview(arrangedView, aboveSubview: beforeView)
		arrangedView.lac.make(instructions)
	}
	
	func insert(arrangedView: UIView, after afterView: UIView, instructions: (ConstraintView) -> Void) {
		self.insertSubview(arrangedView, belowSubview: afterView)
		arrangedView.lac.make(instructions)
	}
}
