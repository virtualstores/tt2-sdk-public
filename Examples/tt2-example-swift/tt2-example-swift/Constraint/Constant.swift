//
//  Constant.swift
//  Func
//
//  Created by Philip Fryklund on 25/Dec/17.
//

import UIKit



/// Make sure the intended value gets returned for NSLayoutConstraint's constant
public protocol ConstraintConstant {
	
	func constant(for attribute: NSLayoutConstraint.Attribute) -> CGFloat
}


extension Int: ConstraintConstant {
	
	public func constant(for attribute: NSLayoutConstraint.Attribute) -> CGFloat {
		return CGFloat(self)
	}
}

extension Double: ConstraintConstant {
	
	public func constant(for attribute: NSLayoutConstraint.Attribute) -> CGFloat {
		return CGFloat(self)
	}
}

extension CGFloat: ConstraintConstant {
	
	public func constant(for attribute: NSLayoutConstraint.Attribute) -> CGFloat {
		return self
	}
}

extension CGSize: ConstraintConstant {
	
	public func constant(for attribute: NSLayoutConstraint.Attribute) -> CGFloat {
		switch attribute {
		case .width: return self.width
		case .height: return self.height
		default: fatalError("`CGSize` Should only be used for size")
		}
	}
}

extension CGPoint: ConstraintConstant {
	
	public func constant(for attribute: NSLayoutConstraint.Attribute) -> CGFloat {
		switch attribute {
		case .centerX, .centerXWithinMargins: return self.x
		case .centerY, .centerYWithinMargins: return self.y
		default: fatalError("`CGPoint` Should only be used for points")
		}
	}
}

extension UIEdgeInsets: ConstraintConstant {
	
	public func constant(for attribute: NSLayoutConstraint.Attribute) -> CGFloat {
		switch attribute {
		case .top, .topMargin: return self.top
		case .bottom, .bottomMargin: return -self.bottom
		case .left, .leftMargin, .leading, .leadingMargin: return self.left
		case .right, .rightMargin, .trailing, .trailingMargin: return -self.left
		default: fatalError("`UIEdgeInsets` Should only be used for egdes")
		}
	}
}
