//
//  MyListAndBonusView.swift
//  tt2-example-swift
//
//  Created by Théodore Roos on 2020-12-04.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit

class MyListAndBonusContainer: UIView {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return subviews.contains(where: { $0.hitTest(convert(point, to: $0), with: event) != nil })
    }
}

class MyListAndBonusView: UIViewController {
    
    @IBOutlet var headerView: UIView!
    @IBOutlet var tableView: UITableView!
    
    private var fullStop: CGFloat = 24.0
    
    private var partialStop: CGFloat {
        view.bounds.height - headerView.bounds.height - 20.0
    }
    
    private var hasAnimatedIn = false
    
    var yOffset: CGFloat {
        get {
            self.view.frame.origin.y
        }
        
        set {
            self.view.frame = CGRect(x: 0, y: newValue, width: view.frame.width, height: view.frame.height)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        panGesture.delegate = self
        view.addGestureRecognizer(panGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !hasAnimatedIn {
            yOffset = UIScreen.main.bounds.height
        }
    }
    
    func continuePan(translation: CGFloat) {
        guard yOffset + translation >= fullStop else {
            yOffset = fullStop
            return
        }
        
        yOffset += translation
    }
    
    func finishPan(translation: CGFloat, velocity: CGFloat) {
        let targetY = velocity > 0 ? partialStop : fullStop
        UIView.animate(withDuration: 0.4) {
            self.yOffset = targetY
            self.view.layoutIfNeeded()
        }
    }
    
    func expand(animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.2) {
                self.yOffset = self.fullStop
            }
        }
        else {
            self.yOffset = self.fullStop
        }
    }
    
    func collapse(animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.2) {
                self.yOffset = self.partialStop
            }
        }
        else {
            self.yOffset = self.partialStop
        }
    }
    
    @objc func handlePanGesture(_ gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: view)
        let velocity = gesture.velocity(in: view)
        switch gesture.state {
        case .changed:
            continuePan(translation: translation.y)
            gesture.setTranslation(.zero, in: view)
        case .ended, .failed, .cancelled:
            finishPan(translation: translation.y, velocity: velocity.y)
        default:
            break
        }
    }
}

extension MyListAndBonusView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        let gesture = (gestureRecognizer as! UIPanGestureRecognizer)
        let direction = gesture.velocity(in: view).y
        
        let y = view.frame.minY
        if (y == fullStop && tableView.contentOffset.y == 0 && direction > 0) {
            tableView.isScrollEnabled = false
        } else {
            tableView.isScrollEnabled = true
        }
        
        return false
    }
}
