//
//  StorePickerView.swift
//  tt2-example-swift
//
//  Created by Théodore Roos on 2020-10-16.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit
import Domain
import TT2_SDK

class StorePickerView: UIViewController {
    @IBOutlet var storeList: UITableView!
    @IBOutlet var storeListContainer: UIView!
    
    var presenter: StorePickerPresenter?
    
    private var animating: Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        if animated {
            view.backgroundColor = .clear
            storeListContainer.transform = .init(translationX: 0.0, y: self.view.frame.height)
            animating = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.4, animations: {
                self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
                self.storeListContainer.transform = .init(translationX: 0.0, y: 0.0)
            }, completion: { _ in
                self.animating = false
            })
        }
    }
    
    func animateAway(completion: (() -> Void)? = nil) {
        animating = true
        UIView.animate(withDuration: 0.4, animations: {
            self.view.backgroundColor = .clear
            self.storeListContainer.transform = .init(translationX: 0.0, y: self.view.frame.height)
        }, completion: { _ in
            self.animating = false
            completion?()
        })
    }
    
    override func viewDidLoad() {
        presenter?.viewIsReady()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapBackground))
        tap.delegate = self
        view.addGestureRecognizer(tap)
    }
    
    func exit(animated: Bool) {
        guard !animating else {
            return
        }
        
        if animated {
            animateAway {
                self.dismiss(animated: true)
            }
        }
        else {
            self.dismiss(animated: false)
        }
    }
    
    @objc func didTapBackground() {
        exit(animated: true)
    }
}

extension StorePickerView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == view
    }
}

class StorePickerListCell: UITableViewCell {
    @IBOutlet var storeNameLabel: UILabel!
    @IBOutlet var roundedBackgroundView: RoundedView!
    @IBOutlet var iconView: UIImageView!
}

class StorePickerPresenter: NSObject {
    var interactor: StorePickerInteractor!
    var view: StorePickerView!
    
    func viewIsReady() {
        view.storeList.dataSource = self
        view.storeList.delegate = self
        view.storeList.reloadData()
    }
    
    func exit() {
        if !interactor.hasToChooseStore {
            view?.exit(animated: true)
            
            if let exit = interactor.onExit {
                exit()
            }
        }
    }
}

extension StorePickerPresenter: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if interactor.stores.count > 2 {
            var height = (tableView.superview!).frame.size.height
            for _ in 2...interactor.stores.count {
                height += 44.0
            }
            
            view.storeListContainer.setHeight(height)
        }
        return interactor.stores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! StorePickerListCell
        let store = interactor.stores[indexPath.row]
        
        cell.storeNameLabel.text = store.name
        
        if let selectedStoreIndex = interactor.selectedStoreIndex, selectedStoreIndex == indexPath.row {
            cell.roundedBackgroundView.backgroundColor = interactor.activeColor
            cell.roundedBackgroundView.borderWidth = 0.0
//            cell.storeNameLabel.font = UIFont.CircularPro.bold.size(18)
            let ciColor = CIColor(color: interactor.activeColor)
            let primaryColor: UIColor = (ciColor.red*0.299 + ciColor.green*0.587 + ciColor.blue*0.114) > 0.729 ? .black : .white
            cell.storeNameLabel.textColor = primaryColor
            cell.iconView.tintColor = primaryColor
            cell.iconView.isHidden = false
        }
        else {
            cell.roundedBackgroundView.backgroundColor = .white
            cell.roundedBackgroundView.borderWidth = 1.0
//            cell.storeNameLabel.font = UIFont.CircularPro.book.size(18)
            cell.storeNameLabel.textColor = .black
            cell.iconView.tintColor = .black
            cell.iconView.isHidden = true
        }
        
        return cell
    }
}

extension StorePickerPresenter: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let store = interactor.stores[indexPath.row]
        
        guard interactor.activeStore?.name != store.name else {
            view.exit(animated: true)
            return
        }
        
        interactor.didSelectStore(store)
        interactor.selectedStoreIndex = indexPath.row
        tableView.reloadData()
        TT2.shared.positionManager.stopUpdatingLocation(uploadRecording: false)
        self.exit()
    }
}

class StorePickerInteractor {
    var selectedStoreIndex: Int?
    var stores: [Store] = []
    
    var activeStore: Store?
    var didSelectStore: (Store) -> Void
    var onExit: (() -> Void)?
    var hasToChooseStore: Bool
    var activeColor: UIColor
    
    init(activeStore: Store?, stores: [Store], hasToChooseStore: Bool, didSelectStore: @escaping (Store) -> Void, activeColor: UIColor) {
        self.activeStore = activeStore
        self.stores = stores
        self.selectedStoreIndex = stores.firstIndex() { $0.id == activeStore?.id }
        self.hasToChooseStore = hasToChooseStore
        self.didSelectStore = didSelectStore
        self.activeColor = activeColor
    }
}
