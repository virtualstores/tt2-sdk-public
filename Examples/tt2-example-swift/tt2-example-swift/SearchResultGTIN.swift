
import Foundation

// MARK: - Welcome
struct SearchResult {
    let status: Int
    let message: String
    let data: [Datum]
    let pagination: Pagination
}

// MARK: - Datum
struct Datum {
    let wi: Int
    let title: String
    let images: [Image]
    let price: Price
    let propertyIcons: [Any?]
    let theme: String
    let bonus: NSNull
    let datumDescription: String
    let hqid: Int
}

// MARK: - Image
struct Image {
    let title: String
    let url: String
    let width, height: Int
}

// MARK: - Price
struct Price {
    let price: Double
    let unitSize: String
}

// MARK: - Pagination
struct Pagination {
    let pageSize, pageNumber, totalElements, totalPages: Int
}
