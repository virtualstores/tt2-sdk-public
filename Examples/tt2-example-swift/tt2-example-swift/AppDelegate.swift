//
//  AppDelegate.swift
//  tt2-example-swift
//
//  Created by Jesper Lundqvist on 2020-03-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit
import AWSS3
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.initializeS3()
        self.initializeKeyboardManager()
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func initializeS3() {
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: "AKIAWNN36WTI33XVCFRW", secretKey: "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")
        let configuration = AWSServiceConfiguration.init(region: AWSRegionType.EUWest1, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }
    
    func initializeKeyboardManager() {
        let keyboardManager = IQKeyboardManager.shared
        keyboardManager.enable = true
        keyboardManager.shouldShowToolbarPlaceholder = false
        keyboardManager.shouldResignOnTouchOutside = true
        keyboardManager.enableAutoToolbar = false
//        keyboardManager.toolbarManageBehaviour = .byTag
//        keyboardManager.previousNextDisplayMode = .alwaysShow
//        keyboardManager.enableDebugging = true
    }
}

