//
//  QuestionSheetView.swift
//  tt2-example-swift
//
//  Created by Théodore Roos on 2020-11-13.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Mapbox
import TT2_SDK

class QuestionSheetView: UIViewController {
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var recordedTimeLabel: UILabel!
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    @IBOutlet var ageTextField: UITextField!
    @IBOutlet var commentTextField: UITextField!
    @IBOutlet var selectGenderButton: UIButton!
    @IBOutlet var selectRouteButton: UIButton!
    @IBOutlet var firstNameRequiredLabel: UILabel!
    @IBOutlet var lastNameRequiredLabel: UILabel!
    @IBOutlet var ageRequiredLabel: UILabel!
    @IBOutlet var genderRequiredLabel: UILabel!
    @IBOutlet var routeRequiredLabel: UILabel!
    
    var mainViewController: ViewController?
    var selectedPath: Int = -1
    var selectedGender = -1
    var routeTitle = ""
    
    let defaults = UserDefaults.standard
    let uploadTimeFormatter = DateFormatter()
    let uploadDayFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uploadTimeFormatter.dateFormat = "HHmmss"
        uploadDayFormatter.dateFormat = "yyMMdd"
        
        firstNameRequiredLabel.isHidden = true
        lastNameRequiredLabel.isHidden = true
        ageRequiredLabel.isHidden = true
        genderRequiredLabel.isHidden = true
        routeRequiredLabel.isHidden = true
        
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        ageTextField.delegate = self
        commentTextField.delegate = self
        
        let date = TT2.shared.date
        let dayFormatter = DateFormatter()
        dayFormatter.dateFormat = "yyyy-MM-dd"
        dateLabel.text = dayFormatter.string(from: date)
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm:ss"
        recordedTimeLabel.text = "Recorded at \(timeFormatter.string(from: date))"
        
        firstNameTextField.text = defaults.string(forKey: "FirstName")
        lastNameTextField.text = defaults.string(forKey: "LastName")
        ageTextField.text = defaults.string(forKey: "Age")
        
        //selectedGender = defaults.integer(forKey: "Gender")
        
        (mainViewController?.mapView.style?.layer(withIdentifier: "start-pos-img") as? MGLSymbolStyleLayer)?.iconOpacity = NSExpression(forConstantValue: 1.0)
        
        switch selectedGender {
        case 0: selectGenderButton.setTitle("Man", for: .normal)
        case 1: selectGenderButton.setTitle("Woman", for: .normal)
        default:
            break
        }
        
        if routeTitle.contains("4") {
            selectedPath = 4
        }
        else if routeTitle.contains("5") {
            selectedPath = 5
        }
        else if routeTitle.contains("6") {
            selectedPath = 6
        }
        else if routeTitle.contains("7") {
            selectedPath = 7
        }
        else if routeTitle.contains("8") {
            selectedPath = 8
        }
        else if routeTitle.contains("9"){
            selectedPath = 9
        }
        
        selectedPath -= 1
        if !routeTitle.isEmpty {
            selectRouteButton.setTitle(routeTitle, for: .normal)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    @IBAction func handleExitButtonPressed(_ sender: UIButton) {
        if let delegate = mainViewController, let routeButton = delegate.selectRouteButton {
            routeButton.setTitle("Record", for: .normal)
            routeButton.isSelected = false
            
            self.dismiss(animated: true) {
                delegate.selectPath(route: 0, sender)
            }
        }
        else {
            self.dismiss(animated: true)
        }
    }
    
    @IBAction func handleSelectGenderButtonPressed(_ sender: UIButton) {
        selectGenderButton.backgroundColor = .systemGray6
        genderRequiredLabel.isHidden = true
        self.view.endEditing(true)
        let vc = self.storyboard?.instantiateViewController(identifier: "routePickerView") as! RoutePickerView
        vc.questionSheetViewControler = self
        vc.view.alpha = 0.0
        vc.gender = true
        self.present(vc, animated: true) {
            let view = vc.view.subviews.first(where: { $0 == $0.viewWithTag(1) })
            
            guard let stackView = view else {
                return
            }
            
            for subview in stackView.subviews {
                subview.isHidden = true
            }
            
            vc.imageView.isHidden = true
            vc.viewImage.isHidden = true
            vc.routeOneButton.setTitleColor(.black, for: .normal)
            vc.routeOneButton.backgroundColor = .white
            vc.routeOneButton.setTitle("Man", for: .normal)
            vc.routeTwoButton.setTitle("Woman", for: .normal)
            vc.routeOneButton.isHidden = false
            vc.routeTwoButton.isHidden = false
            vc.gender = true
            
            if let title = self.selectGenderButton.currentTitle {
                switch title {
                case "Man": vc.routeOneButton.isSelected = true
                case "Woman": vc.routeTwoButton.isSelected = true
                default:
                    break
                }
            }
            
            UIView.animate(withDuration: 0.5) {
                vc.view.alpha = 1.0
            }
        }
    }
    
    @IBAction func handleSelectRouteButtonPressed(_ sender: UIButton) {
        selectRouteButton.backgroundColor = .systemGray6
        routeRequiredLabel.isHidden = true
        self.view.endEditing(true)
        let vc = self.storyboard?.instantiateViewController(identifier: "routePickerView") as! RoutePickerView
        vc.questionSheetViewControler = self
        vc.selectedPath = selectedPath
        if let title = sender.currentTitle {
            vc.routeTitle = title
        }
        vc.view.alpha = 0.0
        self.present(vc, animated: true) {
            UIView.animate(withDuration: 0.5) {
                vc.view.alpha = 1.0
            }
        }
    }
    
    @IBAction func handleSendDataButtonPressed(_ sender: UIButton) {
        let alpa: CGFloat = 0.2
        #if !TT2ALBERTHEIJN
        guard let firstName = firstNameTextField.text, !firstName.isEmpty else {
            firstNameRequiredLabel.isHidden = false
            firstNameTextField.backgroundColor = UIColor.red.withAlphaComponent(alpa)
            return
        }
        
        guard let lastName = lastNameTextField.text, !lastName.isEmpty else {
            lastNameRequiredLabel.isHidden = false
            lastNameTextField.backgroundColor = UIColor.red.withAlphaComponent(alpa)
            return
        }
        
        guard let age = ageTextField.text, !age.isEmpty else {
            ageRequiredLabel.isHidden = false
            ageTextField.backgroundColor = UIColor.red.withAlphaComponent(alpa)
            return
        }
        #endif
        
        guard let gender = selectGenderButton.currentTitle, gender != "Select a gender from the list" else {
            genderRequiredLabel.isHidden = false
            selectGenderButton.backgroundColor = UIColor.red.withAlphaComponent(alpa)
            return
        }
        
        guard let route = selectRouteButton.currentTitle, let comments = commentTextField.text, route != "Select a route from the list" else {
            routeRequiredLabel.isHidden = false
            selectRouteButton.backgroundColor = UIColor.red.withAlphaComponent(alpa)
            return
        }
        
        #if !TT2ALBERTHEIJN
        TT2.shared.sendData(firstName: firstName, lastName: lastName, route: route, gender: gender, age: age, comments: comments)
        
        defaults.set(firstName, forKey: "FirstName")
        defaults.set(lastName, forKey: "LastName")
        defaults.set(age, forKey: "Age")
        #else
        TT2.shared.sendData(firstName: nil, lastName: nil, route: nil, gender: gender, age: nil, comments: comments)
        #endif
        defaults.set(selectedGender, forKey: "Gender")
        
        if let delegate = mainViewController, let routeButton = delegate.selectRouteButton, let startButton = delegate.startButton {
            routeButton.setTitle("Record", for: .normal)
            routeButton.isSelected = false
            startButton.setTitle("Start", for: .normal)
            
            self.dismiss(animated: true) {
                delegate.selectPath(route: 0, sender)
            }
        }
        else {
            self.dismiss(animated: true)
        }
    }
}

extension QuestionSheetView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case firstNameTextField: lastNameTextField.becomeFirstResponder()
        case lastNameTextField: ageTextField.becomeFirstResponder()
        case ageTextField: ageTextField.resignFirstResponder()
        case commentTextField: commentTextField.resignFirstResponder()
        default:
            break
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case firstNameTextField:
            firstNameTextField.backgroundColor = .systemGray6
            firstNameRequiredLabel.isHidden = true
        case lastNameTextField:
            lastNameTextField.backgroundColor = .systemGray6
            lastNameRequiredLabel.isHidden = true
        case ageTextField:
            ageTextField.backgroundColor = .systemGray6
            ageRequiredLabel.isHidden = true
        default:
            break
        }
    }
}
