//
//  AnimatedButton.swift
//  tt2-example-swift
//
//  Created by Jesper Lundqvist on 2020-06-12.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit

class AnimatedButton: UIButton {

    private let touchedDownScale: CGFloat = 0.95
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.1) {
            self.transform = CGAffineTransform(scaleX: self.touchedDownScale, y: self.touchedDownScale)
        }
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
            self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: nil)
        super.touchesEnded(touches, with: event)
    }
}
