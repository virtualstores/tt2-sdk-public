//
//  StartPageView.swift
//  tt2-example-swift
//
//  Created by Théodore Roos on 2020-12-04.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit
import Domain
import TT2_SDK

class StartPageView: UIViewController {
    let serverSettings = Bundle.main.infoDictionary?["ServerSettings"] as! Dictionary<String, Any>
    
    var stores: [Store]?
    var selectedStore: Store!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        load()
    }
    
    @IBAction func handleChooseAStoreButtonPressed(_ sender: UIButton) {
        guard let stores = self.stores else {
            let alert = UIAlertController(title: "Could not load in stores", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default))
            self.present(alert, animated: true)
            return
        }
        let vc = self.storyboard?.instantiateViewController(identifier: "ahStorePicker") as! AHStorePickerView
        vc.delegate = self
        vc.stores = stores
        vc.handleSelectedStore = {
            TT2.shared.activeStore = self.selectedStore
            let vc = self.storyboard?.instantiateViewController(identifier: "main") as! ViewController
            vc.modalPresentationStyle = .fullScreen
            vc.isModalInPresentation = true
            self.present(vc, animated: true)
        }
        self.present(vc, animated: true)
    }
    
    func load() {
        let serverURL = serverSettings["ServerURL"] as! String
        let apiKey = serverSettings["APIKey"] as! String
        let clientId = serverSettings["ClientID"] as! Int64
        
        TT2.shared.initiate(clientId: clientId, serverUrl: URL(string: serverURL)!, apiKey: apiKey)
        
        TT2.shared.getStores(onlyActive: true) { error in
            switch error {
            case .none:
                var stores: [Store] = TT2.shared.stores
                stores.sort(by: { $0.name < $1.name })
                self.stores = stores
            case .some(let error):
                print(error.localizedDescription)
            }
        }
    }
}
