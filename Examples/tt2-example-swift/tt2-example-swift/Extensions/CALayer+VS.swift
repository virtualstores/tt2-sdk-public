//
//  CALayer+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-11-08.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import UIKit



extension CALayer {
	
	@objc var borderUIColor: UIColor? {
		get { self.borderColor.map(UIColor.init(cgColor: )) }
		set { self.borderColor = newValue?.cgColor }
	}
}
