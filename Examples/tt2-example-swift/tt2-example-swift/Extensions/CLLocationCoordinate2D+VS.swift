//
//  CLLocationCoordinate2D+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-12-18.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import CoreLocation



extension CLLocationCoordinate2D: Equatable {
	
	func distance(to coordinate: CLLocationCoordinate2D) -> CLLocationDistance {
		let from = CLLocation(coordinate: self)
		let to = CLLocation(coordinate: coordinate)
		return from.distance(from: to)
	}
	
	public static func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
		lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
	}
}
