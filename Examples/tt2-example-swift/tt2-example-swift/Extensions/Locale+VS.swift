//
//  Locale+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-11-17.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import Foundation


extension Locale {
	
	static let se = Locale(identifier: "SV_se")
}
