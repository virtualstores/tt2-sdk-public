//
//  UINavigationController+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-12-06.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import UIKit



extension UINavigationController {
	
	var rootViewController: UIViewController? {
		self.viewControllers.first
	}
	
	func popViewController(animated: Bool, completion: @escaping (UINavigationController) -> Void) {
		CATransaction.setCompletionBlock { completion(self) }
		CATransaction.begin()
		self.popViewController(animated: animated)
		CATransaction.commit()
	}
	
	func pushViewController(_ viewController: UIViewController, animated: Bool, completion: @escaping (UINavigationController) -> Void) {
		CATransaction.setCompletionBlock { completion(self) }
		CATransaction.begin()
		self.pushViewController(viewController, animated: animated)
		CATransaction.commit()
	}
}
