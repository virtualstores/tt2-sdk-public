//
//  UIImage+VS.swift
//  Hemkop
//
//  Created by Jesper Lundqvist on 2019-11-29.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import UIKit

extension UIImage {
	
    func withInsets(_ insets: UIEdgeInsets) -> UIImage {
		let size = CGSize(width: self.size.width + insets.left + insets.right, height: self.size.height + insets.top + insets.bottom)
        UIGraphicsBeginImageContextWithOptions(size, false, self.scale)
        let origin = CGPoint(x: insets.left, y: insets.top)
        self.draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()?.withRenderingMode(self.renderingMode)
        UIGraphicsEndImageContext()
        return imageWithInsets!
    }
	
	func withSize(_ size: CGSize) -> UIImage {
		var scaledImageRect = CGRect.zero
		
		let aspectWidth: CGFloat = size.width / self.size.width
		let aspectHeight: CGFloat = size.height / self.size.height
		let aspectRatio: CGFloat = min(aspectWidth, aspectHeight)
		
		scaledImageRect.size.width = self.size.width * aspectRatio
		scaledImageRect.size.height = self.size.height * aspectRatio
		scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0
		scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0
		
		UIGraphicsBeginImageContextWithOptions(size, false, 0)
		defer { UIGraphicsEndImageContext() }
		
		self.draw(in: scaledImageRect)
		
		return UIGraphicsGetImageFromCurrentImageContext()!
	}
    
	func withTint(_ tintColor: UIColor) -> UIImage {
		UIGraphicsBeginImageContextWithOptions(self.size, false, 0.0)
		defer { UIGraphicsEndImageContext() }
		let ctx = UIGraphicsGetCurrentContext()!
		ctx.translateBy(x: 0, y: self.size.height)
		ctx.scaleBy(x: 1.0, y: -1.0)
		ctx.setBlendMode(CGBlendMode.normal) //Color, Hue
		let rect = CGRect(origin: .zero, size: self.size)
		ctx.clip(to: rect, mask: self.cgImage!)
		tintColor.setFill()
		ctx.fill(rect)
		return UIGraphicsGetImageFromCurrentImageContext()!
	}
}
