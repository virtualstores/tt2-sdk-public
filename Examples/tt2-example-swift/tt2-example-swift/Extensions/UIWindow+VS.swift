//
//  UIWindow+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-12-10.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import UIKit



extension UIWindow {
	
	static var current: UIWindow? {
		UIApplication.shared.keyWindow
	}
}
