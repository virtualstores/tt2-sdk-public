//
//  UIEdgeInsets+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-11-14.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import UIKit



extension UIEdgeInsets {
	
	var inverted: UIEdgeInsets {
		return UIEdgeInsets(top: -self.top, left: -self.left, bottom: -self.bottom, right: -self.right)
	}
	
	var vertical: CGFloat {
		self.top + self.bottom
	}
	
	var horizontal: CGFloat {
		self.left + self.right
	}
	
	var cgSize: CGSize {
		CGSize(width: horizontal, height: vertical)
	}
	
	
	init(insets: CGFloat) {
		self.init(top: insets, left: insets, bottom: insets, right: insets)
	}
}


extension UIEdgeInsets {
	
	static func + (lhs: UIEdgeInsets, rhs: UIEdgeInsets) -> UIEdgeInsets {
		UIEdgeInsets(top: lhs.top + rhs.top, left: lhs.left + rhs.left, bottom: lhs.bottom + rhs.bottom, right: lhs.right + rhs.right)
	}
}
