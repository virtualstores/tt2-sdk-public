//
//  UIAlertController+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-11-27.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import UIKit



extension UIAlertController {
	
	func addDefault(_ title: String?, action: (() -> Void)?) {
		self.addAction(UIAlertAction(title: title, style: .default) { (_) in
			action?()
		})
	}
	
	func addDestructive(_ title: String?, action: (() -> Void)?) {
		self.addAction(UIAlertAction(title: title, style: .destructive) { (_) in
			action?()
		})
	}
	
	func addCancel(_ title: String?, action: (() -> Void)?) {
		self.addAction(UIAlertAction(title: title, style: .cancel) { (_) in
			action?()
		})
	}
}
