//
//  URL+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2020-01-10.
//  Copyright © 2020 Philip Fryklund. All rights reserved.
//

import Foundation



extension URL: ExpressibleByStringLiteral {
	
	public init(stringLiteral value: StringLiteralType) {
		guard let url = URL(string: value) else {
			fatalError("Could not instantiate URL from string '\(value)'")
		}
		
		self = url
	}
}

extension URL: ExpressibleByStringInterpolation {
	
	public init(stringInterpolation: DefaultStringInterpolation) {
		let value = stringInterpolation.description
		guard let url = URL(string: value) else {
			fatalError("Could not instantiate URL from string '\(value)'")
		}
		
		self = url
	}
}
