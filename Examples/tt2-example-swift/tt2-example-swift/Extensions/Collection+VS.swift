//
//  Collection+VS.swift
//  tt2-retail
//
//  Created by Théodore Roos on 2020-06-29.
//  Copyright © 2020 VirtualStores. All rights reserved.
//

import Foundation
import CoreGraphics

extension Collection where Element == CGPoint {
	var width: CGFloat {
		guard self.isNotEmpty else {
			return 0.0
		}
		
		let shapeX = self.sorted { first, second in
			return first.x < second.x
		}
		
		return shapeX.last!.x - shapeX.first!.x
	}
	
	var height: CGFloat {
		guard self.isNotEmpty else {
			return 0.0
		}
		
		let shapeY = self.sorted { first, second in
			return first.y < second.y
		}
		
		return shapeY.last!.y - shapeY.first!.y
	}
}
