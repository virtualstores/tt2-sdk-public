//
//  String+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-11-17.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import Foundation


extension String {
	
	subscript(i: Int) -> String {
		return String(self[index(startIndex, offsetBy: i)])
	}
	
	// Barcodes
	
	public var isWeightedBarcode: Bool {
		return self.count == 13 && self[0] == "2" && self[1] >= "0" && self[1] <= "5" && self[12] == "\(self.weightedBarcodeChecksum)"
	}
	
	public var withoutWeight: String {
		guard isWeightedBarcode else {
			return self
		}
		
		let newBarcode = "20\(self.dropFirst(2).prefix(6))0000"
		return "\(newBarcode)\(newBarcode.weightedBarcodeChecksum)"
	}
	
	public var weightedBarcodeChecksum: Int {
		let characters = self.prefix(12).reversed().map { String($0) }.map { Int($0)! }.enumerated()
		let calculated = characters.map { ($0 % 2 == 0 ? 3 : 1) * $1 }.reduce(0, +)
		
		let checkDigit = Int(ceil(Double(calculated) / 10.0) * 10)
		
		return checkDigit - calculated
	}
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
