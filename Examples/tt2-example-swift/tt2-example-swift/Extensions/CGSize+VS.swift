//
//  CGSize+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2020-01-10.
//  Copyright © 2020 Philip Fryklund. All rights reserved.
//

import CoreGraphics



extension CGSize {
	
	/**
	Returns an aspect ratio of `width / height`
	
	## Usage:
	let width = height * aspectRatio
	*/
	var aspectRatio: CGFloat {
		return self.width / self.height
	}
}


extension CGSize {

	static func + (lhs: CGSize, rhs: CGSize) -> CGSize {
		return CGSize(width: lhs.width + rhs.width, height: lhs.height + rhs.height)
	}
	
	static func + (lhs: CGSize, rhs: CGFloat) -> CGSize {
		return CGSize(width: lhs.width + rhs, height: lhs.height + rhs)
	}
	
	static func - (lhs: CGSize, rhs: CGSize) -> CGSize {
		return CGSize(width: lhs.width - rhs.width, height: lhs.height - rhs.height)
	}
	
	static func - (lhs: CGSize, rhs: CGFloat) -> CGSize {
		return CGSize(width: lhs.width - rhs, height: lhs.height - rhs)
	}
	
	static func * (lhs: CGSize, rhs: CGSize) -> CGSize {
		return CGSize(width: lhs.width * rhs.width, height: lhs.height * rhs.height)
	}
	
	static func * (lhs: CGSize, rhs: CGFloat) -> CGSize {
		return CGSize(width: lhs.width * rhs, height: lhs.height * rhs)
	}
	
	static func / (lhs: CGSize, rhs: CGSize) -> CGSize {
		return CGSize(width: lhs.width / rhs.width, height: lhs.height / rhs.height)
	}
	
	static func / (lhs: CGSize, rhs: CGFloat) -> CGSize {
		return CGSize(width: lhs.width / rhs, height: lhs.height / rhs)
	}
	
	
	static func += (lhs: inout CGSize, rhs: CGSize) {
		lhs = lhs + rhs
	}
	
	static func += (lhs: inout CGSize, rhs: CGFloat) {
		lhs = lhs + rhs
	}
	
	static func -= (lhs: inout CGSize, rhs: CGSize) {
		lhs = lhs - rhs
	}
	
	static func -= (lhs: inout CGSize, rhs: CGFloat) {
		lhs = lhs - rhs
	}
	
	static func *= (lhs: inout CGSize, rhs: CGSize) {
		lhs = lhs * rhs
	}
	
	static func *= (lhs: inout CGSize, rhs: CGFloat) {
		lhs = lhs * rhs
	}
	
	static func /= (lhs: inout CGSize, rhs: CGSize) {
		lhs = lhs / rhs
	}
	
	static func /= (lhs: inout CGSize, rhs: CGFloat) {
		lhs = lhs / rhs
	}
}
