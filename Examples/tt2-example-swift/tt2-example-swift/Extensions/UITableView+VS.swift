//
//  UITableView+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-11-13.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import UIKit



public protocol TableViewReuseable: class {
	
	static var reuseIdentifier: String { get }
}

extension TableViewReuseable {
	
	static var reuseIdentifier: String {
		return String(describing: Self.self)
	}
	
	static var nib: UINib? {
		return UINib(nibName: reuseIdentifier, bundle: Bundle(for: Self.self))
	}
}



public extension UITableView {
	
	func reuseableCell <T: UITableViewCell> (for id: String, at indexPath: IndexPath) -> T {
		return self.dequeueReusableCell(withIdentifier: id, for: indexPath) as! T
	}
	
	
	func reuseableCell <T: UITableViewCell & TableViewReuseable> (at indexPath: IndexPath) -> T {
		return reuseableCell(for: T.reuseIdentifier, at: indexPath)
	}
	
	func reuseableHeaderFooter <T: UITableViewHeaderFooterView> (for id: String) -> T {
		return self.dequeueReusableHeaderFooterView(withIdentifier: id) as! T
	}
	
	func dequeueHeaderFooter <T: UITableViewHeaderFooterView & TableViewReuseable> () -> T {
		return reuseableHeaderFooter(for: T.reuseIdentifier)
	}
	
	func registerCell <T: UITableViewCell & TableViewReuseable> (ofType type: T.Type) {
//		if let nib = type.nib {
//			self.register(nib, forCellReuseIdentifier: type.reuseIdentifier)
//		}
//		else {
		self.register(type, forCellReuseIdentifier: type.reuseIdentifier)
//		}
	}
	
	func registerHeaderFooter <T: UITableViewHeaderFooterView & TableViewReuseable> (ofType type: T.Type) {
		if let nib = type.nib {
			self.register(nib, forHeaderFooterViewReuseIdentifier: type.reuseIdentifier)
		}
		else {
			self.register(type, forHeaderFooterViewReuseIdentifier: type.reuseIdentifier)
		}
	}
	
	
	func setTableHeaderViewWithAutoLayout(_ view: UIView) {
		self.tableHeaderView = view
		view.setNeedsLayout()
		view.layoutIfNeeded()
		view.frame.size = view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
		self.tableHeaderView = view
	}
	
	
	func setTableFooterViewWithAutoLayout(_ view: UIView) {
		self.tableFooterView = view
		view.setNeedsLayout()
		view.layoutIfNeeded()
		view.frame.size = view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
		self.tableFooterView = view
	}
	
	
	
	func deselectSelectedRow(animated: Bool) {
		guard let indexPath = self.indexPathForSelectedRow else {
			return
		}
		
		self.deselectRow(at: indexPath, animated: animated)
	}
	
	
	func deselectSelectedRows(animated: Bool) {
		self.indexPathsForSelectedRows?.forEach {
			self.deselectRow(at: $0, animated: animated)
		}
	}
}
