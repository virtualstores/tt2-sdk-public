//
//  UIView+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-11-13.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import UIKit



extension UIView {
	
	var superViewController: UIViewController? {
		var responder: UIResponder? = self
		
		repeat {
			responder = responder?.next
			if let viewController = responder as? UIViewController {
				return viewController
			}
		} while responder != nil
		
		return nil
	}
	
	var isHiddenStackView: Bool {
		get { self.isHidden }
		set { self.isHidden = newValue ; self.alpha = newValue ? 0 : 1 }
	}
	
	var isVisible: Bool {
		get { !self.isHidden }
		set { self.isHidden = !newValue }
	}
	
	
	func forceLayout() {
		self.setNeedsLayout()
		self.layoutIfNeeded()
	}

	func removeAllSubviews() {
		self.subviews.forEach {
			$0.removeFromSuperview()
		}
	}
	
	func projectedFrame(to view: UIView?) -> CGRect? {
		self.superview?.convert(self.frame, to: view)
	}
}
