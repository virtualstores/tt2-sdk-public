//
//  Date+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-11-15.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import Foundation



public extension Date {
	
	static var now: Date {
		return Date()
	}
	
	
	/// Reversed order of Date.timeIntervalSince(_:)
	func timeIntervalUntil(_ date: Date) -> TimeInterval {
		return date.timeIntervalSince(self)
	}
}
