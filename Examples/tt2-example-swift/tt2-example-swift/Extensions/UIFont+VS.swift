//
//  UIFont+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-11-29.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import UIKit



protocol FontBuildable: RawRepresentable {
	
	var rawValue: String { get }
	init?(rawValue: String)
}

extension FontBuildable {
	
	var fontName: String { return "\(Self.self)" + self.rawValue }
	
	func size(_ size: CGFloat) -> UIFont {
		return UIFont(name: fontName, size: size)!
	}
}



extension UIFont {
	
	enum CircularPro: String, FontBuildable {
		case mediumItalic = "-MediumItalic"
		case blackItalic = "-BlackItalic"
		case black = "-Black"
		case medium = "-Medium"
		case book = "-Book"
		case bold = "-Bold"
		case boldItalic = "-BoldItalic"
		case bookItalic = "-BookItalic"
	}
	
	
	enum ObjektivMk2: String, FontBuildable {
		case medium = "-Medium"
		case bold = "-Bold"
		case regular = "-Regular"
		case boldItalic = "-XBoldItalic"
	}
	
	enum Sentinel: String, FontBuildable {
		case lightItalic = "-LightItalic"
		case bookItalic = "-BookItalic"
		case semiboldItalic = "-SemiboldItalic"
		case mediumItalic = "-MediumItalic"
		case boldItalic = "-BoldItalic"
		case blackItalic = "-BlackItalic"
		case black = "-Black"
		case book = "-Book"
		case bold = "-Bold"
		case light = "-Light"
		case medium = "-Medium"
		case semibold = "-Semibold"
	}
}
