//
//  Kingfisher+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2020-01-10.
//  Copyright © 2020 Philip Fryklund. All rights reserved.
//

import UIKit
import Kingfisher



extension KingfisherWrapper where Base == UIImageView {
	
	private func fallbackImage() {
		self.base.image = #imageLiteral(resourceName: "no_image_avaliable")
	}
	
	private func fallbackUrl(with id: String) {
		self.setImage(with: "https://s3-eu-west-1.amazonaws.com/product-information-storage/images/\(id).jpg" as URL, placeholder: nil) { (result) in
			guard case let .failure(error) = result, !error.isNotCurrentTask else {
				return
			}
			
			self.fallbackImage()
		}
	}
	
	func setImageWithFallback(with resource: Kingfisher.Resource?) {
		guard let resource = resource else {
			return fallbackImage()
		}
				
		self.setImage(with: resource, placeholder: nil) { (result) in
			guard case let .failure(error) = result, !error.isNotCurrentTask else {
				return
			}
			
			// If first url is not already a fallback url
			if resource.downloadURL.host?.contains("aws") == true {
				self.fallbackImage()
			}
			else {
				//TODO: Get id from source
				self.fallbackUrl(with: "07330583991758")
			}
		}
	}
}
