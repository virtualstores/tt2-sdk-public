//
//  CLLocation+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-11-29.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import CoreLocation



public extension CLLocation {
	
	// For VPS
	var accuracy: CLLocationAccuracy {
		self.horizontalAccuracy
	}
	
	var confidence: CLLocationAccuracy {
		self.verticalAccuracy
	}
	
	convenience init(coordinate: CLLocationCoordinate2D) {
		self.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
	}
	
	convenience init(coordinate: CLLocationCoordinate2D, course: CLLocationDirection) {
		self.init(coordinate: coordinate, altitude: 0, horizontalAccuracy: 0, verticalAccuracy: 0, course: course, speed: 0, timestamp: Date())
	}
	
	convenience init(coordinate: CLLocationCoordinate2D, accuracy: CLLocationAccuracy, confidence: Double, course: CLLocationDirection) {
		self.init(coordinate: coordinate, altitude: 0, horizontalAccuracy: accuracy, verticalAccuracy: confidence, course: course, speed: 0, timestamp: Date())
	}
}



public extension CLLocationCoordinate2D {
	
	init(_ latitude: CLLocationDegrees, _ longitude: CLLocationDegrees) {
		self.init(latitude: latitude, longitude: longitude)
	}
}



extension FloatingPoint {
	
	var deg: Self {
		return self * 180 / .pi
	}
	
	var rad: Self {
		return self * .pi / 180
	}
}
