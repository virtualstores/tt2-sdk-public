//
//  Item+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2020-01-10.
//  Copyright © 2020 Philip Fryklund. All rights reserved.
//

import Domain



extension Item {
	
	var actualImageUrl: URL? {
		return self.imageUrl.flatMap { URL(string: $0) }
	}
}
