//
//  Array+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2019-11-13.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import Foundation



public extension Collection {
	
	var isNotEmpty: Bool {
		return !self.isEmpty
	}
}



public extension Collection {
	
	subscript (safe index: Index) -> Iterator.Element? {
		guard self.indices.contains(index) else {
			return nil
		}
		return self[index]
	}
}



public extension RangeReplaceableCollection where Iterator.Element: Equatable {
	
	@discardableResult
	mutating func remove(element: Iterator.Element) -> Iterator.Element? {
		guard let index = self.firstIndex(of: element) else {
			return nil
		}
		return self.remove(at: index)
	}
}



public extension Collection where Iterator.Element: AdditiveArithmetic {
	
	func sum() -> Iterator.Element? {
		guard self.isNotEmpty else {
			return nil
		}
		return self.reduce(Iterator.Element.zero, +)
	}
}



extension BidirectionalCollection where Element: Equatable {
	
	func element(before element: Element) -> Element? {
		guard let index = self.firstIndex(of: element), index > self.startIndex else {
			return nil
		}
		return self[self.index(before: index)]
	}
	
	func element(after element: Element) -> Element? {
		guard let index = self.firstIndex(of: element), index < self.endIndex else {
			return nil
		}
		return self[self.index(after: index)]
	}
}
