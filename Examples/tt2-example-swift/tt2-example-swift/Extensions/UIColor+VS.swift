//
//  UIColor+VS.swift
//  Hemkop
//
//  Created by Jesper Lundqvist on 2019-11-28.
//  Copyright © 2019 Philip Fryklund. All rights reserved.
//

import UIKit



// MARK: Design colors

extension UIColor {
	
	enum TT2 {
		
		static let beige = UIColor(named: "beige")!
		static let black = UIColor(named: "black")!
		static let darkGray = UIColor(named: "dark-gray")!
		static let darkRed = UIColor(named: "dark-red")!
		static let gray = UIColor(named: "gray")!
		static let green = UIColor(named: "green")!
		static let lightGray = UIColor(named: "light-gray")!
		static let pink = UIColor(named: "pink")!
		static let red = UIColor(named: "red")!
		static let yellow = UIColor(named: "yellow")!
		static let transparentBlack = UIColor(named: "black-transparent")!
        static let ahBlue = UIColor(named: "AHBlue")
	}
	
	static var primary: UIColor {
		let primaryColorValue = Bundle.init(for: AppDelegate.self).infoDictionary?["TT2PrimaryColor"] as! Int
		return UIColor(rgb: primaryColorValue)
	}
	
	static var secondary: UIColor {
		let primaryColorValue = Bundle.init(for: AppDelegate.self).infoDictionary?["TT2SecondaryColor"] as! Int
		return UIColor(rgb: primaryColorValue)
	}
	
	var tint: UIColor {
		let ciColor = CIColor(color: self)
		return (ciColor.red*0.299 + ciColor.green*0.587 + ciColor.blue*0.114) > 0.729 ? .black : .white
	}
}



// MARK: Initializers

extension UIColor {
	
	convenience init(red: Int, green: Int, blue: Int) {
		assert(red >= 0 && red <= 255, "Invalid red component")
		assert(green >= 0 && green <= 255, "Invalid green component")
		assert(blue >= 0 && blue <= 255, "Invalid blue component")

		self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
	}

	convenience init(rgb: Int) {
		self.init(
			red: (rgb >> 16) & 0xFF,
			green: (rgb >> 8) & 0xFF,
			blue: rgb & 0xFF
		)
	}
}
