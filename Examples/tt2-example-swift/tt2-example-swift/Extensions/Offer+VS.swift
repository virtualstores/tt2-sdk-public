//
//  Offer+VS.swift
//  Hemkop
//
//  Created by Philip Fryklund on 2020-01-25.
//  Copyright © 2020 Philip Fryklund. All rights reserved.
//

import Domain



extension Offer {
	
	var id: String {
		"\(title)\(offerPrice)"
	}
}
