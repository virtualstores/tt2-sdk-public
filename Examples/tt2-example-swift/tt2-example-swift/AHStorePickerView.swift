//
//  AHStorePickerView.swift
//  tt2-example-swift
//
//  Created by Théodore Roos on 2020-12-07.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit
import Domain

class AHStorePickerView: UIViewController {

    @IBOutlet var tableView: UITableView!
    
    var delegate: StartPageView!
    var stores: [Store]!
    var handleSelectedStore: (() -> ())!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        for store in stores {
            if store.name == "AH Hoofddorp" || store.name == "AH Stadhouderskade" || store.name == "AH Wibautstraat" {
                
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.dismiss(animated: true)
    }
    
    @IBAction func handleCancelButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
}

class StoreCell: UITableViewCell {
    @IBOutlet var addressLabel: UILabel!
}

extension AHStorePickerView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        stores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let store = stores[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "store") as! StoreCell
        if let address = store.address.description {
            cell.addressLabel.text = address
        }
        else {
            tableView.cellForRow(at: indexPath)?.removeFromSuperview()
        }
        
        return cell
    }
}

extension AHStorePickerView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let stores = self.stores else {
            return
        }
        
        delegate.selectedStore = stores[indexPath.row]
        self.dismiss(animated: true) {
            self.handleSelectedStore()
        }
    }
}
