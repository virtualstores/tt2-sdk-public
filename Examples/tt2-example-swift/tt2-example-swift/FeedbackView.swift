//
//  FeedbackView.swift
//  tt2-example-swift
//
//  Created by Théodore Roos on 2020-12-07.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit

class FeedbackView: UIViewController {
    
    @IBOutlet var commentTextField: UITextField!
    
    var q1: Int = -1
    var q2: Int = -1
    override func viewDidLoad() {
        super.viewDidLoad()

        commentTextField.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    @IBAction func handleStarButtonPressed(_ sender: UIButton) {
        guard let subviews = sender.superview?.subviews else {
            return
        }
        
        let superviewTag = sender.superview?.tag
        
        switch superviewTag {
        case 0: self.q1 = sender.tag + 1
        case 1: self.q2 = sender.tag + 1
        default:
            break
        }
        
        switch sender.tag {
        case 0, 1, 2, 3, 4:
            for subview in subviews where subview is UIButton {
                let button = subview as! UIButton
                if button.tag > sender.tag {
                    button.isSelected = false
                }
                else {
                    button.isSelected = true
                }
            }
        default:
            break
        }
    }
    
    @IBAction func handleDismissButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    @IBAction func handleSendDataButtonPressed(_ sender: UIButton) {
        sendData()
        self.dismiss(animated: true)
    }
    
    func sendData() {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "docs.google.com"
        urlComponents.path = "/forms/d/e/1FAIpQLSeP6hPodTM1IRKyT3jCjRLndeBAERc-HuTfOHzOXKtECQxNkw/formResponse"
        urlComponents.queryItems = [
            URLQueryItem(name: "entry.189097570", value: "\(q1)"),
            URLQueryItem(name: "entry.2057081642", value: "\(q2)"),
            URLQueryItem(name: "entry.878098702", value: commentTextField.text),
            URLQueryItem(name: "submit", value: "Submit")
        ]
        
        guard let url = urlComponents.url else {
            return
        }
        
        print(url)
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            //TODO: Finish
//                guard let data = data else {
//                    return
//                }
        }
        
        task.resume()
    }
}

extension FeedbackView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
