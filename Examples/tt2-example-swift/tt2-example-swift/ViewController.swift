//
//  ViewController.swift
//  tt2-example-swift
//
//  Created by Jesper Lundqvist on 2020-03-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit
import TT2_SDK
import Mapbox
import Domain
import Api
import AVFoundation

class ViewController: UIViewController {
    @IBOutlet var openBarcodeScannerButton: AnimatedButton!
    @IBOutlet var routeButtonView: UIView!
    @IBOutlet var routeOneButton: AnimatedButton!
    @IBOutlet var routeTwoButton: AnimatedButton!
    @IBOutlet var routeThreeButton: AnimatedButton!
    @IBOutlet var routeFourButton: AnimatedButton!
    @IBOutlet var routeFivebutton: AnimatedButton!
    @IBOutlet var storePickerButton: AnimatedButton!
    @IBOutlet var currentPositionKitLabel: UILabel!
    @IBOutlet var navigationButton: AnimatedButton!
    @IBOutlet var mapView: MGLMapView!
    @IBOutlet var loadingScreen: UIView!
    @IBOutlet var selectRouteButton: AnimatedButton!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var feedbackButton: UIButton!
    @IBOutlet var startButton: AnimatedButton!
    @IBOutlet var myListAndBonusContstraintBottom: NSLayoutConstraint!
    @IBOutlet var containerViewHeight: NSLayoutConstraint!
    @IBOutlet var mapViewContstraintBottom: NSLayoutConstraint!
    @IBOutlet var myListAndBonusStackView: UIStackView!
    @IBOutlet weak var TestButtonMultiPurpose: UIButton!
    @IBOutlet var popupView: UIView!
    
    private var popupTimer: Timer?
    private var triggeredItems: Set<PointOfInterest> = []
    private var areaEventManager: AreaEventManager!
    private var bottomSheetView: BottomSheet!
    
    var startFlagActive: Bool = true
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? BottomSheet, segue.identifier == "bottomSheet" {
            bottomSheetView = vc
        }
    }
    
    
    var barcodeScanner: BarcodeScannerViewController?
    let serverSettings = Bundle.main.infoDictionary?["ServerSettings"] as! Dictionary<String, Any>
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    var selectedPath: Int?
    var titleOfSelectedRoute: String?
    var infoShown = false
    
    @IBAction func handleRouteOneButtonPressed(_ sender: UIButton) {
        selectPath(route: 1, sender)
    }
    
    @IBAction func handelRouteTwoButtonPressed(_ sender: UIButton) {
        selectPath(route: 2, sender)
    }
    
    @IBAction func handleRouteThreeButtonPressed(_ sender: UIButton) {
        selectPath(route: 3, sender)
    }
    
    @IBAction func handleRouteFourPressed(_ sender: UIButton) {
        selectPath(route: 3, sender)
        if sender.isSelected {
            createItemsForShoppingRoute()
        }
    }
    
    @IBAction func handleRouteFiveButtonPressed(_ sender: UIButton) {
        selectPath(route: 4, sender)
    }
    
    @IBAction func handleRouteSixButtonPressed(_ sender: UIButton) {
        selectPath(route: 5, sender)
    }
    
    @IBAction func handleRouteSevenPressed(_ sender: UIButton) {
        selectPath(route: 5, sender)
        if sender.isSelected {
            createItemsForShoppingRoute()
        }
    }
    
    @IBAction func handleRouteEightPressed(_ sender: UIButton) {
        selectPath(route: 5, sender)
    }
    
    @IBAction func handleRouteNinePressed(_ sender: UIButton) {
        selectPath(route: 5, sender)
    }
    
    @IBAction func handleStorePickerButtonPressed(_ sender: UIButton) {
        TT2.shared.positionManager.stopUpdatingLocation(uploadRecording: false)
        Toaster.shared = nil
        TT2.shared.mapController.delegate = nil
        TT2.shared.positionStateObserverToken = nil
        self.dismiss(animated: true)
        #if TT2EXAMPLE
        buildStorePicker(activeStore: TT2.shared.activeStore) { store in
            self.load(storeId: store.id)
        }
        #elseif TT2VIRTUALSTORES
        TT2.shared.getItemPosition(barcode: "7300206238003") { (dict, error) in
            switch error {
            case .none:
                let dic: Dictionary<TT2Item, [TT2ItemPosition]>? = dict
                print("\nDictionary:\n\(dic ?? [:])\n")
            case .some(let error):
                print("\nError:\n\(error)\n")
            }
        }
        
        let barcodes = ["7300206238003", "7300206233008"]
        TT2.shared.getItemPositions(barcodes: barcodes) { (dict, error) in
            switch error {
            case .none:
                let dic: Dictionary<TT2Item, [TT2ItemPosition]>? = dict
                print("\nDictionary 2:\n\(dic ?? [:])\n")
            case .some(let error):
                let dic: Dictionary<TT2Item, [TT2ItemPosition]>? = dict
                print("\nDictionary 2:\n\(dic ?? [:])\n")
                print("\nError:\n\(error)\n")
            }
        }
        #else
        sender.isHidden = true
        #endif
    }
    
    var myListItems: [ShoppingListItem] = []
    var listIsOrdered = false
    @IBAction func handleMyListButtonPressed(_ sender: UIButton) {
        guard TT2.shared.positionManager.isUpdatingPosition else {
            Toaster.shared?.toast(title: "Start a session", subtitle: nil, iconUrl: nil)
            return
        }
        
//        guard !bottomSheetView.myList else {
//            self.reset(sender: sender)
//            return
//        }
        
        if myListItems.isEmpty {
            self.getMyListItem()
        }
        
        var pointsOfInterest: [PointOfInterest] = []
        for item in myListItems {
            pointsOfInterest.append(item.item!.asPointOfInterest)
        }
        pointsOfInterest = TT2.shared.navigationManager.getBestPath(pois: pointsOfInterest)
        sender.isSelected = !sender.isSelected
        
        var sorted: [ShoppingListItem] = []
        
        for poi in pointsOfInterest {
            if let item = myListItems.first(where: { $0.item!.id == poi.id }) {
                sorted.append(item)
            }
        }
        listIsOrdered = true
        if sender.isSelected {
            reset(sender: sender)
            sender.isSelected = true
            sender.backgroundColor = UIColor.TT2.ahBlue
            bottomSheetView.myListItems = sorted
            bottomSheetView.myList = true
            bottomSheetView.expand(animated: true)
            for listItem in myListItems {
                if let item = listItem.item {
                    TT2.shared.assistant.add(item.asPointOfInterest)
                }
            }
            
            if TT2.shared.positionManager.isUpdatingPosition {
                TT2.shared.assistant.clearOverride()
                if let poi = findClosestPoi(userPostion: TT2.shared.positionManager.lastLocation!, pointsIfInterest: myListItems) {
                    TT2.shared.assistant.override(with: poi)
                }
                TT2.shared.navigationManager.startNavigation()
            }
            
            containerViewHeight.constant = bottomSheetView.myListStop - 20
            myListAndBonusContstraintBottom.constant = (bottomSheetView.myListStop / 2) + 20
            mapViewContstraintBottom.constant = (bottomSheetView.myListStop / 2) + 5
            TT2.shared.mapController.resetCameraToMapBounds()
        }
        else {
            reset(sender: sender)
        }
    }
    
    func findClosestPoi(userPostion: TT2Location, pointsIfInterest: [ShoppingListItem]) -> PointOfInterest? {
        var closestDistance: CGFloat = 1000
        var closestPoi: PointOfInterest?
        
        for listItem in pointsIfInterest {
            let distance = listItem.item!.asPointOfInterest.position!.cgPointwithoutOffset.distance(to: CGPoint(x: userPostion.position.x, y: TT2.shared.navigationSpace.height - userPostion.position.y))
            
            if distance < closestDistance {
                closestDistance = distance
                closestPoi = listItem.item!.asPointOfInterest
            }
        }
        
        return closestPoi
    }
    
    @IBAction func handlePersonalBonusButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            sender.backgroundColor = UIColor.init(named: "AHBlue")
        }
        else {
            sender.backgroundColor = .white
        }
    }
    
    var bonuspois: [PointOfInterest] = []
    @IBAction func handleBonusButtonPressed(_ sender: UIButton) {
        if sender.isSelected {
            reset(sender: sender)
        }
        else {
            reset(sender: sender)
            TT2.shared.api.store?.offerApi.getOffersBy(customerId: "AHEmployee", completion: { result in
                switch result {
                case .success(let offers):
                    let group = DispatchGroup()
                    var success = false
                    
                    for offer in offers {
                        group.enter()
                        guard let poi = offer.asPointOfInterest else {
                            return
                        }
                        
                        TT2.shared.api.store?.itemApi.getItemBy(id: poi.id, completion: { (result) in
                            switch result {
                            case .success(let item):
                                let bonusItem = Item(id: poi.id, name: poi.name, prettyName: item.prettyName, description: poi.description, manufacturerName: item.manufacturerName, imageUrl: offer.imageUrl, alternateIds: item.alternateIds, ecoLabels: offer.ecoLabels, shelfTierIds: item.shelfTierIds, offerShelfTierIds: item.offerShelfTierIds, offerItemId: item.offerItemId, price: offer.offerPrice)
                                self.bonuspois.append(bonusItem.asPointOfInterest)
                                group.leave()
                                success = true
                            case .failure(let error):
                                print(error.localizedDescription)
                                group.leave()
                            }
                        })
                    }
                    
                    group.notify(queue: .main) {
                        if success {
                            sender.isSelected = true
                            sender.backgroundColor = UIColor.init(named: "AHBlue")
                            TT2.shared.assistant.add(self.bonuspois)
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            })
        }
    }
    
    @IBAction func handleStopButtonPressed(_ sender: UIButton) {
        guard TT2.shared.positionManager.isUpdatingPosition else {
            return
        }
        
        let yesAction = UIAlertAction(title: "Yes", style: .default) { _ in
            TT2.shared.positionManager.stopUpdatingLocation(uploadRecording: true)
            if self.selectRouteButton.isSelected {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "questionSheet") as! QuestionSheetView
                if let path = self.selectedPath {
                    vc.selectedPath = path
                }
                if let title = self.titleOfSelectedRoute {
                    vc.routeTitle = title
                }
                vc.mainViewController = self
                vc.isModalInPresentation = true
                self.present(vc, animated: true)
            }
            else {
                TT2.shared.sendData(firstName: nil, lastName: nil, route: nil, gender: nil, age: nil, comments: nil)
            }
            self.startFlagActive = true
        }
        let yesUpload = UIAlertAction(title: "Yes", style: .default) { _ in
            TT2.shared.sendData(firstName: nil, lastName: nil, route: nil, gender: nil, age: nil, comments: nil)
        }
        
        let noAction = UIAlertAction(title: "No", style: .cancel) { _ in
        }
        
//        let yesAction = UIAlertAction(title: "Yes", style: .default) { _ in
//            TT2.shared.positionManager.stopUpdatingLocation(uploadRecording: true)
//            self.ask(title: "Upload?", message: "Would you like to upload your session?", primaryAction: yesUpload, noAction: noAction)
//        }
        
        
        ask(title: "Stop", message: "Would you like to stop your session?", primaryAction: yesAction, secondaryAction: noAction)
    }
    
    @IBAction func handleStartButtonPressed(_ sender: UIButton) {
        if let start = TT2.shared.navigationSpace.startCodes.first {
            TT2.shared.positionManager.synchronize(code: start, syncDirection: true)
        }
        else {
            currentPositionKitLabel.text = "ERROR!!!"
        }
    }
    
    @IBAction func handleNavigationButtonPressed(_ sender: UIButton) {
        guard let firstPointOfInterest = TT2.shared.assistant.pointsOfInterest.first, TT2.shared.positionManager.isUpdatingPosition else {
            return
        }

        TT2.shared.assistant.override(with: firstPointOfInterest)
        if sender.isSelected {
            TT2.shared.navigationManager.stopNavigation()
        }
        else {
            TT2.shared.navigationManager.startNavigation()
        }

        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func handleTestButtonPressed(_ sender: Any) {
        showBehaviourView(primaryImage: #imageLiteral(resourceName: "phone-stance"), secondaryImage: nil, label: "Keep your phone horizontal and heading forward", regular: true)
    }
    
    override func viewWillLayoutSubviews() {
        mapView.attributionButton.isHidden = true
        mapView.logoView.isHidden = true
    }
    
    @IBAction func handleSelectRouteButtonPressed(_ sender: UIButton) {
        #if targetEnvironment(simulator)
        let vc = self.storyboard?.instantiateViewController(identifier: "routePickerView") as! RoutePickerView
        #else
        (mapView.style?.layer(withIdentifier: "start-pos") as? MGLFillStyleLayer)?.fillOpacity = NSExpression(forConstantValue: 1.0)
        let vc = self.storyboard?.instantiateViewController(identifier: "routePickerView") as! RoutePickerView
        vc.mainViewController = self
        if let path = selectedPath {
            vc.selectedPath = path
        }
        if let title = sender.currentTitle {
            vc.routeTitle = title
        }

        self.present(vc, animated: true)
        #endif
    }
    
    @IBAction func handleFeedbackButtonPressed(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "feedback") as! FeedbackView
        vc.isModalInPresentation = true
        present(vc, animated: true)
    }
    
    @IBAction func handleInfoButtonPressed(_ sender: UIButton) {
        showInfo()
    }
    
    @IBAction func handleResetButtonPressed(_ sender: UIButton) {
        TT2.shared.mapController.resetCameraToMapBounds()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTextField.delegate = self
        
        openBarcodeScannerButton.isEnabled = false
//        openBarcodeScannerButton.isHidden = true
        
        let version = Bundle(identifier: "org.cocoapods.PositionKit")?.infoDictionary?["CFBundleShortVersionString"] as? String
        
        #if TT2CITYGROSS
        settingsForMap(routeButtons: true, storePicker: true, scrolling: true, zooming: true)
        if let version = version {
            currentPositionKitLabel.text = "PositionKit: \(version)"
        }
        else {
            currentPositionKitLabel.text = "PositionKit: Unknown"
        }
        #elseif TT2HEMKOP || TT2VIRTUALSTORES
        TT2Position.scale = 59.0
        settingsForMap(routeButtons: true, storePicker: true, scrolling: true, zooming: true)
        if let version = version {
            currentPositionKitLabel.text = "PositionKit: \(version)"
        }
        else {
            currentPositionKitLabel.text = "PositionKit: Unknown"
        }
        #elseif TT2ALBERTHEIJN
        currentPositionKitLabel.isHidden = true
        settingsForMap(scrolling: true, zooming: true)
        #else
        settingsForMap(storePicker: true)
        currentPositionKitLabel.isHidden = true
        #endif
        
        navigationButton.isHidden = true
        feedbackButton.transform = feedbackButton.transform.rotated(by: CGFloat(-Double.pi / 2))
        TestButtonMultiPurpose.isHidden = true
        bottomSheetView.mainController = self
        load()
    }
    
    func load(storeId id: Int64? = nil) {
        #if TT2ALBERTHEIJN
        guard let store = TT2.shared.activeStore else {
            return
        }
        
        TT2.shared.activate(storeId: store.id, mapView: self.mapView) { error in
            switch error {
            case .none:
                TT2.shared.mapController.delegate = self
                Toaster.createShared(from: self.view)
                self.getMyListItem()
                self.dismissLoadingScreen()

                TT2.shared.positionStateObserverToken = TT2.shared.positionManager.observeState { [weak self] state in
                    switch state {
                    case .ready:
                        self?.openBarcodeScannerButton.isEnabled = true
                        self?.dismissLoadingScreen()
                        self?.storePickerButton.setTitle("Change store", for: .normal)
                        if let view = self, !view.infoShown {
                            view.showInfo()
                            view.infoShown = true
                        }
                        (self?.mapView.style?.layer(withIdentifier: "start-pos") as? MGLFillStyleLayer)?.fillOpacity = NSExpression(forConstantValue: 1.0)
                        if self?.startFlagActive == true {
                            (self?.mapView.style?.layer(withIdentifier: "start-pos-img") as? MGLSymbolStyleLayer)?.iconOpacity = NSExpression(forConstantValue: 1.0)
                        }
                        else {
                            (self?.mapView.style?.layer(withIdentifier: "start-pos-img") as? MGLSymbolStyleLayer)?.iconOpacity = NSExpression(forConstantValue: 0.0)
                        }
                    case .stopped:
                        TT2.shared.assistant.clear()
                    default:
                        break
                    }
                }
            case .some(let error):
                let tryAgainAction = UIAlertAction(title: "Try again", style: .default) { _ in
                    self.load()
                }

                self.ask(title: "Could not load map", message: error.localizedDescription, primaryAction: tryAgainAction)
            }
        }
        #else
        let serverURL = serverSettings["ServerURL"] as! String
        let apiKey = serverSettings["APIKey"] as! String
        let clientId = serverSettings["ClientID"] as! Int64
        var storeId = serverSettings["StoreID"] as! Int64
        
        if let id = id {
            storeId = id
        }
        
        TT2.shared.initiate(clientId: clientId, serverUrl: URL(string: serverURL)!, apiKey: apiKey)
        
        TT2.shared.getStores { error in
            switch error {
            case .none:
                TT2.shared.activate(storeId: storeId, mapView: self.mapView) { error in
                    switch error {
                    case .none:
                        TT2.shared.mapController.delegate = self
                        TT2.shared.debugTools.printTappedLocation()
                        
                        TT2.shared.positionStateObserverToken = TT2.shared.positionManager.observeState { [weak self] state in
                            switch state {
                            case .ready:
                                self?.openBarcodeScannerButton.isEnabled = true
                                self?.dismissLoadingScreen()
                                self?.storePickerButton.setTitle(TT2.shared.activeStore?.name, for: .normal)
                            case .stopped:
                                TT2.shared.assistant.clear()
                            default:
                                break
                            }
                        }
                    case .some(let error):
                        let tryAgainAction = UIAlertAction(title: "Try again", style: .default) { _ in
                            self.load()
                        }
                        
                        self.ask(title: "Could not load map", message: error.localizedDescription, primaryAction: tryAgainAction)
                    }
                }
            case .some(let error):
                let tryAgainAction = UIAlertAction(title: "Try again", style: .default) { _ in
                    self.load()
                }
                
                self.ask(title: "Could not load map", message: error.localizedDescription, primaryAction: tryAgainAction)
            }
        }
        #endif
    }
    
    func selectPath(route nr: Int, _ sender: UIButton? = nil) {
        self.removeItemsForShoppingRoute()
        let routeOne = self.mapView.style?.layer(withIdentifier: "route-1") as? MGLFillStyleLayer
        let routeTwo = self.mapView.style?.layer(withIdentifier: "route-2") as? MGLFillStyleLayer
        let routeThree = self.mapView.style?.layer(withIdentifier: "route-3") as? MGLFillStyleLayer
        let routeFour = self.mapView.style?.layer(withIdentifier: "route-4") as? MGLFillStyleLayer
        let routeFive = self.mapView.style?.layer(withIdentifier: "route-5") as? MGLFillStyleLayer
        let routes: [MGLFillStyleLayer?] = [routeOne, routeTwo, routeThree, routeFour, routeFive]
        
        let lineOne = self.mapView.style?.layer(withIdentifier: "route-1") as? MGLLineStyleLayer
        let lineTwo = self.mapView.style?.layer(withIdentifier: "route-2") as? MGLLineStyleLayer
        let lineThree = self.mapView.style?.layer(withIdentifier: "route-3") as? MGLLineStyleLayer
        let lineFour = self.mapView.style?.layer(withIdentifier: "route-4") as? MGLLineStyleLayer
        let lineFive = self.mapView.style?.layer(withIdentifier: "route-5") as? MGLLineStyleLayer
        let routeslines: [MGLLineStyleLayer?] = [lineOne, lineTwo, lineThree, lineFour, lineFive]
        
        if let selectedPath = selectedPath, let button = sender, selectedPath == nr && !button.isSelected {
            (sender?.superview as? UIStackView)?.subviews.forEach { ($0 as? UIButton)?.isSelected = false }
            sender?.isSelected = true
        }
        else if let selectedPath = selectedPath, selectedPath == nr {
            self.selectedPath = nil
            (mapView.style?.layer(withIdentifier: "route-\(nr)") as? MGLFillStyleLayer)?.fillOpacity = NSExpression(forConstantValue: 0.0)
            (mapView.style?.layer(withIdentifier: "route-\(nr)") as? MGLLineStyleLayer)?.lineOpacity = NSExpression(forConstantValue: 0.0)
            sender?.isSelected = false
        }
        else {
            (mapView.style?.layer(withIdentifier: "route-\(nr)") as? MGLFillStyleLayer)?.fillOpacity = NSExpression(forConstantValue: 1.0)
            (mapView.style?.layer(withIdentifier: "route-\(nr)") as? MGLLineStyleLayer)?.lineOpacity = NSExpression(forConstantValue: 1.0)
            for route in routes where route?.identifier != "route-\(nr)" {
                route?.fillOpacity = NSExpression(forConstantValue: 0.0)
            }
            for line in routeslines where line?.identifier != "route-\(nr)" {
                line?.lineOpacity = NSExpression(forConstantValue: 0.0)
            }
            selectedPath = nr
            (sender?.superview as? UIStackView)?.subviews.forEach { ($0 as? UIButton)?.isSelected = false }
            sender?.isSelected = true
        }
        
        titleOfSelectedRoute = sender?.currentTitle?.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func ask(title: String, message: String?, primaryAction: UIAlertAction, secondaryAction: UIAlertAction? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if let secondaryAction = secondaryAction {
            alert.addAction(secondaryAction)
        }
        alert.addAction(primaryAction)
        
        self.present(alert, animated: true)
    }
    
    func settingsForMap(routeButtons: Bool = false, routeOne: Bool = false, routeTwo: Bool = false, routeThree: Bool = false, routeFour: Bool = false, storePicker: Bool = false, scrolling: Bool = false, zooming: Bool = false, tilting: Bool = false, rotating: Bool = false) {
        routeButtonView.isHidden = routeButtons
        routeOneButton.isHidden = routeOne
        routeTwoButton.isHidden = routeTwo
        routeThreeButton.isHidden = routeThree
        routeFourButton.isHidden = routeFour
        storePickerButton.isHidden = storePicker
        mapView.allowsScrolling = scrolling
        mapView.allowsZooming = zooming
        mapView.allowsTilting = tilting
        mapView.allowsRotating = rotating
    }
    
    func dismissLoadingScreen() {
        if self.loadingScreen != nil {
            
            UIView.animate(withDuration: 0.5, animations: {
                self.loadingScreen.alpha = 0.0
            }) { (_) in
                self.loadingScreen.removeFromSuperview()
            }
        }
    }
    
    func showLoadingScreen() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "loadingScreen") as! LoadingScreen
        self.loadingScreen = vc.view
        loadingScreen.alpha = 0.0
        self.view.addSubview(loadingScreen)
        UIView.animate(withDuration: 0.5, animations: {
            self.loadingScreen.alpha = 1.0
        }, completion: { [self] _ in
            self.selectPath(route: 0)
            
            for subview in self.routeButtonView.subviews where subview is UIStackView {
                for subview in subview.subviews {
                    if let button = subview as? UIButton {
                        button.isSelected = false
                    }
                }
            }
        })
    }
    
    func showInfo() {
        (mapView.style?.layer(withIdentifier: "start_pos") as? MGLLineStyleLayer)?.lineOpacity = NSExpression(forConstantValue: 1.0)
        let vc = self.storyboard?.instantiateViewController(identifier: "routePickerView") as! RoutePickerView
        vc.imageURL = "https://virtualstores-navdata.s3.eu-north-1.amazonaws.com/instructions/instructions.png"
        vc.mainViewController = self
        
        self.present(vc, animated: true)
    }
    
    func buildStorePicker(activeStore: Store?, didSelectStore: @escaping (Store) -> Void) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "storepicker") as! StorePickerView
        let presenter = StorePickerPresenter()
        let interactor = StorePickerInteractor(activeStore: activeStore, stores: TT2.shared.stores, hasToChooseStore: false, didSelectStore: didSelectStore, activeColor: .systemBlue)
        
        interactor.onExit = {
            self.showLoadingScreen()
        }

        vc.presenter = presenter
        presenter.view = vc
        presenter.interactor = interactor
        self.present(vc, animated: true)
    }
    
    let items = [
        Item(name: "One", volume: "One", company: "", shelf: 1, imageUrl: nil),
        Item(name: "Two", volume: "Two", company: "", shelf: 2, imageUrl: nil),
        Item(name: "Three", volume: "Three", company: "", shelf: 3, imageUrl: nil),
        Item(name: "Four", volume: "Four", company: "", shelf: 4, imageUrl: nil),
        Item(name: "Five", volume: "Five", company: "", shelf: 5, imageUrl: nil),
        Item(name: "Six", volume: "Six", company: "", shelf: 6, imageUrl: nil)
    ]
    
    func createItemsForShoppingRoute() {
        var pois: [PointOfInterest] = []
        for item in items {
            pois.append(item.asPointOfInterest)
        }
        
        TT2.shared.assistant.add(pois)
    }
    
    func removeItemsForShoppingRoute() {
        for item in items {
            TT2.shared.assistant.delete(item.asPointOfInterest)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func getMyListItem() {
        guard let store = TT2.shared.activeStore else {
            return
        }

        TT2.shared.api.store?.shoppingListApi.getShoppingListsBy(customerId: "AHPersonnel", completion: { (result) in
            switch result {
            case .success(let lists):
                for list in lists {
                    for shoppingItem in list.products {
                        if let item = shoppingItem.item {
                            TT2.shared.api.central.centralApi.getItemPosition(storeId: store.id, itemId: item.id) { (result) in
                                switch result {
                                case .success(let positions):
                                    guard let itemPos = positions.first, let shelfTierId = itemPos.shelfTierId else {
                                        return
                                    }

                                    self.myListItems.append(ShoppingListItem(id: shoppingItem.id, name: shoppingItem.name, quantity: shoppingItem.quantity, price: shoppingItem.price, priceUnitOfMeasure: shoppingItem.priceUnitOfMeasure, imageUrl: shoppingItem.imageUrl, categoryName: shoppingItem.categoryName, brand: shoppingItem.brand, volume: shoppingItem.volume, item: Item(id: item.id, name: item.name, prettyName: item.prettyName, description: item.description, manufacturerName: item.manufacturerName, imageUrl: item.imageUrl, alternateIds: item.alternateIds, ecoLabels: item.ecoLabels, shelfTierIds: [shelfTierId], offerShelfTierIds: item.offerShelfTierIds, offerItemId: item.offerItemId, price: item.price)))
                                case .failure(let error):
                                    print(error.localizedDescription)
                                }
                            }
                        }
                    }

                }
            case .failure(let error):
                print(error.localizedDescription)
//                    self.getMyListItem()
            }
        })
    }
    
    func resetBottomSheetView() {
        TT2.shared.navigationManager.stopNavigation()
        TT2.shared.assistant.clearOverride()
        containerViewHeight.constant = 0
        myListAndBonusContstraintBottom.constant = 80
        mapViewContstraintBottom.constant = 0
        bottomSheetView.myList = false
        bottomSheetView.hardCollapse(animated: false)
        
        for listItem in myListItems {
            if let item = listItem.item {
                TT2.shared.assistant.delete(item.asPointOfInterest)
            }
        }
        
        for poi in bonuspois {
            TT2.shared.assistant.delete(poi)
        }
    }
    
    func reset(sender: UIButton) {
        TT2.shared.navigationManager.stopNavigation()
        TT2.shared.assistant.clearOverride()
        containerViewHeight.constant = 0
        myListAndBonusContstraintBottom.constant = 80
        mapViewContstraintBottom.constant = 0
        bottomSheetView.myList = false
        bottomSheetView.hardCollapse(animated: false)
        
        for listItem in myListItems {
            if let item = listItem.item {
                TT2.shared.assistant.delete(item.asPointOfInterest)
            }
        }
        
        for poi in bonuspois {
            TT2.shared.assistant.delete(poi)
        }
        
        let view = sender.superview
        if let view = view {
            for subview in view.subviews {
                if let button = subview as? UIButton {
                    button.isSelected = false
                    button.backgroundColor = .white
                }
            }
        }
    }
    
    func showBehaviourView(primaryImage: UIImage, secondaryImage: UIImage?, label: String, regular: Bool) {
        let vc = self.storyboard?.instantiateViewController(identifier: "behaviour") as! BehaviourView
        vc.regular = regular
        vc.view.alpha = 0.0
        self.present(vc, animated: false) {
            vc.primaryIconView.image = primaryImage
            if let image = secondaryImage {
                vc.secondaryIconView.image = image
            }
            else {
                vc.secondaryIconView.isHidden = true
            }
            vc.behaviourLabel.text = label
            
            UIView.animate(withDuration: 0.5, animations: {
                vc.view.alpha = 1.0
            }, completion: { _ in
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            })
        }
    }
}

extension ViewController: MapControllerDelegate {
    func didPressMarker(pointOfInterest: PointOfInterest) {
        #if TT2EXAMPLE
        let infoViewController = storyboard?.instantiateViewController(identifier: "info") as! InfoViewController
        infoViewController.pointOfInterest = pointOfInterest
        infoViewController.onCancel = {
            TT2.shared.mapController.deselectMarkers()
        }
        present(infoViewController, animated: true)
        #elseif TT2VIRTUALSTORES
        guard TT2.shared.positionManager.isUpdatingPosition, let position = pointOfInterest.position else {
            return
        }
        
        let point = CGPoint(x: Double(position.x + (position.offsetX * 2)), y: Double(position.y + (position.offsetY * 2)))
        let converter = MapboxCoordinateConverter(withMeterFactor: 50.0)
        let coordinateX = converter.convertFromPixel(Double(point.x))
        let coordinateY = converter.convertFromPixel(Double(point.y))
        let x = converter.convertFromMapCoordinate(coordinateX)
        let y = converter.convertFromMapCoordinate(coordinateY)
        
        TT2.shared.positionManager.synchronize(location: TT2Location(position: TT2Position(x: x, y: y), course: TT2Course(fromDegrees: 0.0), confidence: 0.0, radius: 1.0, validSync: true, timestamp: TT2.shared.date, syncDirection: false))
        TT2.shared.assistant.delete(with: pointOfInterest.id)
        #elseif TT2ALBERTHEIJN
        var listItem: Item!
        let group = DispatchGroup()
        
        group.enter()
        if let item = myListItems.first(where: { $0.item!.id == pointOfInterest.id})?.item {
            listItem = item
            group.leave()
        }
        else {
            TT2.shared.api.store?.itemApi.getItemBy(id: pointOfInterest.id, completion: { (result) in
                switch result {
                case .success(let item):
                    listItem = item
                    group.leave()
                case .failure(let error):
                    print(error.localizedDescription)
                }
            })
        }
        
        group.notify(queue: .main) {
            let vc = self.storyboard?.instantiateViewController(identifier: "AHInfoviewController") as! AHInfoViewController
            vc.item = listItem
            vc.mainController = self
            vc.onSearch = {
                let infoViewController = self.storyboard?.instantiateViewController(identifier: "info") as! InfoViewController
                infoViewController.pointOfInterest = listItem.asPointOfInterest
                infoViewController.onCancel = {
                    TT2.shared.mapController.deselectMarkers()
                }
                self.present(infoViewController, animated: true)
            }
            self.present(vc, animated: true)
        }
        #endif
    }
    
    func didPressMarkers(pointOfInterests: [PointOfInterest]) {
        let vc = storyboard?.instantiateViewController(identifier: "AHClusterController") as! AHClusterController
        let group = DispatchGroup()
        var items: [Item] = []
        
        for poi in pointOfInterests {
            group.enter()
            if let item = myListItems.first(where: { $0.item!.id == poi.id})?.item {
                items.append(item)
                group.leave()
            }
            else {
                TT2.shared.api.store?.itemApi.getItemBy(id: poi.id, completion: { (result) in
                    switch result {
                    case .success(let item):
                        items.append(item)
                        group.leave()
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                })
            }
        }
        
        group.notify(queue: .main) {
            vc.items = items
            vc.mainController = self
            vc.onSearch = {
                
            }
            if vc.items.count == pointOfInterests.count {
                self.present(vc, animated: true)
            }
        }
    }
    
    func handlePositionError(error: PositionError) {
        switch error {
        case .lostPosition:
            break
        case .illegalBehavior:
            if TT2.shared.positionManager.isUpdatingPosition && TT2.shared.navigationManager.navigating && App.shared.regularShown < 3 && App.shared.doNotShowRegular == false {
                showBehaviourView(primaryImage: #imageLiteral(resourceName: "phone-stance"), secondaryImage: nil, label: "Keep your phone horizontal and heading forward", regular: true)
            }
        case .uploadFailure(let error):
            let alert = UIAlertController(title: "Unable to upload to S3", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(.init(title: "Ok", style: .cancel, handler: { action in
                alert.dismiss(animated: true)
            }))
            present(alert, animated: true)
        case .trolley:
            if TT2.shared.positionManager.isUpdatingPosition && App.shared.trolleyShown < 3 && App.shared.doNotShowTrolley == false {
                showBehaviourView(primaryImage: #imageLiteral(resourceName: "trolley-wrong"), secondaryImage: #imageLiteral(resourceName: "trolley-correct"), label: "Hold your phone as shown when using a trolley", regular: false)
            }
        }
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case searchTextField:
            searchTextField.resignFirstResponder()
            let vc = self.storyboard?.instantiateViewController(identifier: "search") as! SearchView
            present(vc, animated: true)
        default:
            break
        }
    }
}

//extension ViewController: ProximityTrigger {
//    func trigger(pointOfInterest: PointOfInterest) {
//        guard !triggeredItems.contains(pointOfInterest) else {
//            return
//        }
//
////        AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) { }
////        let infoViewController = storyboard?.instantiateViewController(identifier: "info") as! InfoViewController
////        infoViewController.pointOfInterest = pointOfInterest
////        present(infoViewController, animated: true)
////        triggeredItems.insert(pointOfInterest)
//    }
//
//    func filter(pointOfInterest: PointOfInterest) -> Bool {
//        return true
//    }
//
//    func distance(for pointOfInterest: PointOfInterest) -> Double {
//        return 6.0
//    }
//
//    var enabled: Bool {
//        true
//    }
//}
//
//extension ViewController: AreaTrigger {
//    func filter(event: AreaEvent) -> Bool {
//        return true
//    }
//
//    func trigger(event: AreaEvent) {
//        print("\(Date.init()): \(event.asPointOfInterest.name)")
//    }
//}

class PassThroughView: UIView {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for subview in subviews {
            if !subview.isHidden && subview.isUserInteractionEnabled && subview.point(inside: convert(point, to: subview), with: event) {
                return true
            }
        }
        return false
    }
}
