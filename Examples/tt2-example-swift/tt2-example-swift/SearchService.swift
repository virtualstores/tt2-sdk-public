//
//  SearchService.swift
//  tt2-example-swift
//
//  Created by Jesper Lundqvist on 2020-06-12.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Foundation
import Fuse
import Domain
import TT2_SDK

class SearchService {
    private let fuse = Fuse()
    private var searchRequest: Cancellable?
    private var queryTimer: Timer?
    private var items: [Item] = [
        Item(name: "Coca-Cola Can", volume: "Coca-cola Can", company: "Coca-cola", shelf: 126, imageUrl: "https://d27tkw4n7wmrj5.cloudfront.net/ha3/ha7/9427692421150/5000112637922_1554603663178_master_axfood_300"),
//        Item(name: "Head&Shoulders Classic Schampo", volume: "Classic Schampo", company: "Head&Shoulders", shelf: 33, imageUrl: "https://d27tkw4n7wmrj5.cloudfront.net/h55/hf6/9341641195550/5410076230068_1548063954177_master_axfood_300"),
//        Item(name: "English Breakfast", volume: "English Breakfast", company: "Lipton", shelf: 89, imageUrl: "https://d27tkw4n7wmrj5.cloudfront.net/h8d/hc9/9227827281950/5000311527208_1536762690870_master_axfood_300")
    ]
    var results: [Item] = []
    
    func query(_ query: String, completion: (Result<[Item], Error>) -> Void) {
        let results = fuse.search(query, in: items.map { $0.name })
        completion(.success(items.filter({ item in
            results.contains { result in
                item.id == items[result.index].id
            }
        })))
    }
    
    func query(_ query: String?, in scope: Int, completion: @escaping (Result<[Item], Error>) -> Void) {
        searchRequest?.cancel()
        queryTimer?.invalidate()
        
        print(scope)
        
        guard let query = query, !query.isEmpty else {
            results = []
            return
        }
        
        queryTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { _ in
            self.searchRequest = TT2.shared.api.store?.searchApi.findBy(withExtension: true, searchvalue: query, onlyWithPosition: false, itemGroupId: nil, limit: 50) { result in
                switch result {
                case .success(let result):
                    completion(.success(result.items))
                case .failure(let error):
                    self.results = []
                    completion(.failure(error))
                }
            }
        }
    }
}

extension Item {
    convenience init(name: String, volume: String, company: String, shelf: Int64, imageUrl: String?) {
        self.init(id: name, name: name, prettyName: volume, description: "", manufacturerName: company, imageUrl: imageUrl, alternateIds: nil, ecoLabels: nil, shelfTierIds: [shelf], offerShelfTierIds: nil, offerItemId: nil, price: nil)
    }
}
