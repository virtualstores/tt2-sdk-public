//
//  RoutePickerView.swift
//  tt2-example-swift
//
//  Created by Théodore Roos on 2020-12-01.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit
import Mapbox

class RoutePickerView: UIViewController {
    
    @IBOutlet var routeOneButton: AnimatedButton!
    @IBOutlet var routeTwoButton: AnimatedButton!
    @IBOutlet var routeThreeButton: AnimatedButton!
    @IBOutlet var routeFourButton: AnimatedButton!
    @IBOutlet var routeFiveButton: AnimatedButton!
    @IBOutlet var routeSixButton: AnimatedButton!
    @IBOutlet var routeSevenButton: AnimatedButton!
    @IBOutlet var routeEightButton: AnimatedButton!
    @IBOutlet var routeNineButton: AnimatedButton!
    @IBOutlet var routeTenButton: AnimatedButton!
    @IBOutlet var routeElevenButton: AnimatedButton!
    @IBOutlet var cancelButton: AnimatedButton!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var viewImage: UIView!
    
    var mainViewController: ViewController?
    var questionSheetViewControler: QuestionSheetView?
    
    var selectedPath = -1
    var selectedGender = -1
    var routeTitle = ""
    var gender = false
    var imageURL = "https://virtualstores-navdata.s3.eu-north-1.amazonaws.com/instructions/recording.png"
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        routeNineButton.isHidden = true
        routeTenButton.isHidden = true
        routeElevenButton.isHidden = true
        
        if routeTitle.contains("1") {
            selectedPath = 1
        }
        else if routeTitle.contains("2") {
            selectedPath = 2
        }
        else if routeTitle.contains("3") {
            selectedPath = 3
        }
        else if routeTitle.contains("4") {
            selectedPath = 4
        }
        else if routeTitle.contains("5") {
            selectedPath = 5
        }
        else if routeTitle.contains("6") {
            selectedPath = 6
        }
        else if routeTitle.contains("7") {
            selectedPath = 7
        }
        else if routeTitle.contains("8") {
            selectedPath = 8
        }
        else if routeTitle.contains("9"){
            selectedPath = 9
        }
        
        if gender {
            selectedGender = defaults.integer(forKey: "Gender")
            
            switch selectedGender {
            case 0: routeOneButton.isSelected = true
            case 1: routeTwoButton.isSelected = true
            default:
                break
            }
        }
        
        selectedPath = selectedPath - 1
        
        switch selectedPath {
        case 0: routeOneButton.isSelected = true
        case 1: routeTwoButton.isSelected = true
        case 2: routeThreeButton.isSelected = true
        case 3: routeFourButton.isSelected = true
        case 4: routeFiveButton.isSelected = true
        case 5: routeSixButton.isSelected = true
        case 6: routeSevenButton.isSelected = true
        case 7: routeEightButton.isSelected = true
        default:
            break
        }
        
        if imageURL.contains("instructions.png") {
            routeOneButton.isHidden = true
            cancelButton.setTitle("Continue", for: .normal)
        }
        
        #if TT2ALBERTHEIJN
        imageView.kf.setImage(with: URL(string: imageURL))
        if !routeOneButton.isSelected {
            routeOneButton.setTitle("Activate route", for: .normal)
            routeOneButton.backgroundColor = .systemGreen
            routeOneButton.setTitleColor(.white, for: .normal)
        }
        else {
            routeOneButton.setTitle("Deactivate route", for: .normal)
        }
        routeTwoButton.isHidden = true
        routeThreeButton.isHidden = true
        routeFourButton.isHidden = true
        routeFiveButton.isHidden = true
        routeSixButton.isHidden = true
        routeSevenButton.isHidden = true
        routeEightButton.isHidden = true
        #endif
    }
    
    @IBAction func handleSelectedRouteButtonPressed(_ sender: UIButton) {
        if let delegate = mainViewController, let routeButton = delegate.selectRouteButton, let startButton = delegate.startButton {
            routeButton.setTitle("Route 1", for: .normal)
            routeButton.isSelected = !routeButton.isSelected
            startButton.setTitle("Start recording", for: .normal)
            delegate.startFlagActive = false
            (delegate.mapView.style?.layer(withIdentifier: "start-pos-img") as? MGLSymbolStyleLayer)?.iconOpacity = NSExpression(forConstantValue: 0.0)
            
            if !routeButton.isSelected {
                routeButton.setTitle("Record", for: .normal)
                startButton.setTitle("Start", for: .normal)
                delegate.startFlagActive = true
                (delegate.mapView.style?.layer(withIdentifier: "start-pos-img") as? MGLSymbolStyleLayer)?.iconOpacity = NSExpression(forConstantValue: 1.0)
            }
            
            self.dismiss(animated: true) {
                self.selectPath(sender)
            }
        }
        else {
            if gender {
                questionSheetViewControler?.selectGenderButton.setTitle(sender.currentTitle, for: .normal)
                if let title = sender.currentTitle, title.contains("Man") {
                    questionSheetViewControler?.selectedGender = 0
                }
                else if let title = sender.currentTitle, title.contains("Woman") {
                    questionSheetViewControler?.selectedGender = 1
                }
            }
            else {
                questionSheetViewControler?.selectRouteButton.setTitle(sender.currentTitle, for: .normal)
            }
            self.dismissView()
        }
    }
    
    @IBAction func handleCancelButtonPressed(_ sender: UIButton) {
        if mainViewController != nil {
            self.dismiss(animated: true)
        }
        else {
            self.dismissView()
        }
    }
    
    func selectPath(_ sender: UIButton) {
        guard let delegate = self.mainViewController, let title = sender.currentTitle else {
            return
        }
        if title.contains("1") {
            delegate.selectPath(route: 1, sender)
        }
        else if title.contains("2") {
            delegate.selectPath(route: 2, sender)
        }
        else if title.contains("3") {
            delegate.selectPath(route: 3, sender)
        }
        else if title.contains("4") {
            delegate.selectPath(route: 3, sender)
            delegate.createItemsForShoppingRoute()
        }
        else if title.contains("5") {
            delegate.selectPath(route: 4, sender)
        }
        else if title.contains("6") {
            delegate.selectPath(route: 5, sender)
        }
        else if title.contains("7") {
            delegate.selectPath(route: 5, sender)
            delegate.createItemsForShoppingRoute()
        }
        else if title.contains("8") {
            delegate.selectPath(route: 1, sender)
        }
        else if title.contains("Activate") {
            delegate.selectPath(route: 1, sender)
        }
        else {
            delegate.selectPath(route: 0, sender)
        }
    }
    
    func dismissView() {
        UIView.animate(withDuration: 0.5, animations: {
            self.view.alpha = 0.0
        }, completion: { _ in
            self.dismiss(animated: true)
        })
    }
}
