//
//  BarcodeScannerViewController.swift
//  tt2-example-swift
//
//  Created by Jesper Lundqvist on 2020-03-29.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit
import TT2_SDK

class BarcodeScannerViewController: UIViewController {
    @IBOutlet var barcodeScannerView: BarcodeScannerView!
    @IBOutlet var messageContainerView: UIView!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var messageIcon: UIImageView!
    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet var loadingBackground: UIVisualEffectView!
    
//    let synchronizer = ItemBarcodeSynchronizer(itemApi: TT2.shared.api.store!.itemApi, navigation: TT2.shared.navigationSpace.navigation, positionManager: TT2.shared.positionManager)
    
    var positionStateObserverToken: Any?
    
    override func viewWillAppear(_ animated: Bool) {
        if TT2.shared.positionManager.isUpdatingPosition {
            messageContainerView.isHidden = true
        }
        else {
            if animated {
                messageContainerView.alpha = 0
                messageContainerView.transform = .init(translationX: 0.0, y: -messageContainerView.bounds.maxY)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if animated {
            messageContainerView.alpha = 1
            UIView.animate(withDuration: 0.3) {
                self.messageContainerView.transform = .init(translationX: 0.0, y: 0.0)
            }
        }
        
        barcodeScannerView.initializeCapturing(scanArea: CGRect(x: 50.0, y: barcodeScannerView.frame.height/2.0-100.0, width: barcodeScannerView.frame.width-100.0, height: barcodeScannerView.frame.width-100.0), barcodeFormats: [.qrCode, .EAN13, .EAN8, .code128, .code39, .code93]) { error in
            switch error {
            case .none:
                break
            case .some:
                print("ERROR")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        barcodeScannerView.delegate = self
        
        positionStateObserverToken = TT2.shared.positionManager.observeState { [weak self] state in
            DispatchQueue.main.async {
                switch state {
                case .ready:
                    self?.dismiss(animated: true)
                case .uploadedRecording:
                    self?.dismiss(animated: true)
                case .beginSync:
                    self?.beginLoading()
                default:
                    break
                }
            }
        }
    }
    
    @IBAction func didPressDismissButton() {
        self.barcodeScannerView.pauseCapture()
        dismiss(animated: true)
    }
    
    func beginLoading() {
        loadingIndicator.startAnimating()
        UIView.animate(withDuration: 0.3) {
            self.loadingBackground.alpha = 1
        }
    }
    
    func endLoading() {
        loadingIndicator.stopAnimating()
        UIView.animate(withDuration: 0.3) {
            self.loadingBackground.alpha = 0
        }
    }
}

extension BarcodeScannerViewController: BarcodeScannerDelegate {
    func scanned(barcode: String, completion: @escaping (Bool) -> Void) {
        guard !barcode.isEmpty else {
            showErrorMessage(message: "Empty bar-/QR-code")
            completion(false)
            return
        }
        
        if barcode.contains("start") {
            if let startCode = TT2.shared.activeStore?.startCodes.first(where: { $0.code == barcode }) {
                TT2.shared.positionManager.synchronize(code: startCode, syncDirection: true)
            }
            completion(true)
        }
        else if barcode.contains("stop") {
            TT2.shared.positionManager.stopUpdatingLocation(uploadRecording: true)
            completion(true)
        }
        else {
            guard let store = TT2.shared.activeStore else {
                return
            }
            getHqIdFromGtin(gtin: barcode) { result in
                switch result {
                case .success(let convertedID):
                    TT2.shared.api.central.centralApi.getItemPosition(storeId: store.id, itemId: convertedID) { (result) in
                        switch result {
                        case .success(let positions):
                            if let itemPosition = positions.first {
                                let angle = Double.pi + (atan2(itemPosition.itemPositionOffsetY!, itemPosition.itemPositionOffsetX!))
                                print(angle)
                                
                                let position = TT2Position(x: Double(itemPosition.itemPositionX! + itemPosition.itemPositionOffsetX!), y: Double(TT2.shared.navigationSpace.height - (itemPosition.itemPositionY! + itemPosition.itemPositionOffsetY!)))
                                let course = TT2Course(fromRadians: Double(angle))
                                
                                if TT2.shared.positionManager.isUpdatingPosition {
                                    let location = TT2Location(position: position, course: course, confidence: 0.0, radius: 0.0, validSync: false, timestamp: .init(), syncDirection: false)
                                    TT2.shared.positionManager.synchronize(location: location)
                                    self.dismiss(animated: true)
                                }
                                else {
                                    let location = TT2Location(position: position, course: course, confidence: 0.0, radius: 0.0, validSync: false, timestamp: .init(), syncDirection: true)
                                    TT2.shared.positionManager.synchronize(location: location)
                                }
                            }
                        case .failure(let error):
                            print(error)
                        }
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
        
//        switch barcode {
//        case let str where str.contains("start"):
//            if let startCode = TT2.shared.activeStore?.startCodes.first(where: { $0.code == barcode }) {
//                let isUpdatingPosition = TT2.shared.positionManager.isUpdatingPosition
//                TT2.shared.positionManager.synchronize(code: startCode, syncDirection: !isUpdatingPosition)
//            }
//            completion(true)
//        case let str where str.contains("stop"):
//            TT2.shared.positionManager.stopUpdatingLocation(uploadRecording: false)
//        default:
//            showErrorMessage(message: "Invalid code")
//            completion(false)
//        }
        
//        switch barcode {
//        case "start/a8187618-f057-4b42-a476-cd8e16cc9167":
//            loadingIndicator.startAnimating()
//            TT2.shared.positionManager.synchronize(location: .init(position: .init(x: 20.3, y: 36.5), course: .init(fromDegrees: 270.0), confidence: 1.0, radius: 0.0, validSync: true, timestamp: .init(), syncDirection: true))
//            completion(true)
//        case "stop/a8187618-f057-4b42-a476-cd8e16cc9167":
//            TT2.shared.positionManager.stopUpdatingLocation(uploadRecording: true)
//        default:
//            showErrorMessage(message: "Invalid code")
//            completion(false)
//        }
    }
    
    private func showErrorMessage(message: String) {
        self.barcodeScannerView.pauseCapture()
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        alert.addAction(.init(title: "Ok", style: .default, handler: { _ in
            alert.dismiss(animated: true)
            self.barcodeScannerView.resumeCapture()
        }))
        present(alert, animated: true)
    }
    
    private func getHqIdFromGtin(gtin: String, completion: @escaping (Result<String, Error>) -> Void) {
        var hqId: String = ""
        let group = DispatchGroup()
        group.enter()
        self.loadData(from: "https://www.ah.nl/widgets/ah-maps-bff/api/v1/products/gtin?gtins=\(gtin)") { result in
            switch result {
            case .success(let data):
                print("success")
                let json = try! JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! Dictionary<String, AnyObject>
                let data = json["data"] as! [NSDictionary]
                if let data = data.first {
                    hqId = String(describing: data["hqid"] as! NSNumber)
                }
                group.leave()
            case .failure(let error):
                print(error)
                group.leave()
            }
        }
        
        group.notify(queue: .main) {
            completion(.success(hqId))
        }
    }
    
    private func loadData(from url: URL, completion: @escaping (Result<Data, Error>) -> Void) {
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data {
                completion(.success(data))
            }
            else if let error = error {
                completion(.failure(error))
            }
        }
        
        task.resume()
    }
}
