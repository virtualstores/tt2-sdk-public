//
//  BehaviourView.swift
//  tt2-example-swift
//
//  Created by Théodore Roos on 2020-12-15.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit

class BehaviourView: UIViewController {
    
    @IBOutlet var primaryIconView: UIImageView!
    @IBOutlet var secondaryIconView: UIImageView!
    @IBOutlet var behaviourLabel: UILabel!
    @IBOutlet var doNotShowAgainButton: UIButton!
    
    var regular = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func handleCloseButtonPressed(_ sender: UIButton) {
        dismissView()
    }
    
    @IBAction func handleShowInfoAgainButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if regular && sender.isSelected {
            App.shared.doNotShowRegular = true
        }
        else if !regular && sender.isSelected {
            App.shared.doNotShowTrolley = true
        }
        else {
            App.shared.doNotShowRegular = false
            App.shared.doNotShowTrolley = false
        }
    }
    
    func dismissView() {
        if regular {
            App.shared.regularShown += 1
        }
        else {
            App.shared.trolleyShown += 1
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.view.alpha = 0.0
        }, completion: { _ in
            self.dismiss(animated: false)
        })
    }
}
