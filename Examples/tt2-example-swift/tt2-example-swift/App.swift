//
//  App.swift
//  tt2-example-swift
//
//  Created by Théodore Roos on 2020-12-16.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Foundation

final class App {
    static let shared = App()
    
    var regularShown: Int
    var doNotShowRegular: Bool
    var trolleyShown: Int
    var doNotShowTrolley: Bool
    
    init() {
        self.regularShown = 0
        self.doNotShowRegular = false
        self.trolleyShown = 0
        self.doNotShowTrolley = false
    }
}
