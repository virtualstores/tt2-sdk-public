//
//  LoadingScreen.swift
//  tt2-example-swift
//
//  Created by Jesper Lundqvist on 2020-06-15.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit

class LoadingScreen: UIViewController {
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 1.0) {
            self.activityIndicator.isHidden = false
            self.activityIndicator.alpha = 1
        }
    }
}
