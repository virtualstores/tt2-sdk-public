//
//  AHInfoviewController.swift
//  tt2-example-swift
//
//  Created by Théodore Roos on 2021-01-13.
//  Copyright © 2021 Virtual Stores. All rights reserved.
//

import UIKit
import Domain
import TT2_SDK

class AHInfoViewController: UIViewController {

    @IBOutlet var iconView: UIImageView!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    var item: Item!
    var delegate: UIViewController!
    var mainController: ViewController!
    var onSearch: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if view.bounds.height > UIScreen.main.bounds.height {
//            view.lac.height.constraintTo(UIScreen.main.bounds.height - 100, relation: .lessThanOrEqual, multiplier: 1.0, priority: UILayoutPriority)
        }

        iconView.kf.setImage(with: item.actualImageUrl)
        if let price = item.price {
            if price == 0.0 {
                priceLabel.isHidden = true
            }
            priceLabel.text = String(price)
        }
        
        titleLabel.text = item.name
        descriptionLabel.text = item.description.htmlToString
    }
    
    @IBAction func handleDismissButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    @IBAction func handleSearchButtonPressed(_ sender: UIButton) {
        mainController.resetBottomSheetView()
        TT2.shared.assistant.clearOverride()
        TT2.shared.assistant.override(with: item.asPointOfInterest)
        TT2.shared.navigationManager.startNavigation()
        self.dismiss(animated: true) {
            guard let search = self.onSearch else {
                return
            }
            
            if !(self.delegate is AHClusterController) {
                search()
            }
        }
        if let delegate = delegate as? AHClusterController {
            delegate.dismiss(animated: true) {
                guard let search = self.onSearch else {
                    return
                }
                
                search()
            }
        }
    }
}

class AHInfoViewControllers: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    
    var items: [Item]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
    }
}

class collectionViewCell: UICollectionViewCell {
    @IBOutlet var iconView: UIImageView!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var pageControl: UIPageControl!
    
    var onRightPressed: (() -> ())?
    var onLeftPressed: (() -> ())?
    
    @IBAction func handleRightButtonPressed(_ sender: UIButton) {
        guard let onRight = onRightPressed else {
            return
        }
        
        onRight()
    }
    
    @IBAction func handleLeftButtonPressed(_ sender: UIButton) {
        guard let onLeft = onLeftPressed else {
            return
        }
        
        onLeft()
    }
}

extension AHInfoViewControllers: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! collectionViewCell
        let index = indexPath.row
        let item = items[index]
        cell.iconView.kf.setImage(with: item.actualImageUrl)
        if let price = item.price {
            cell.priceLabel.text = String(price)
        }
        cell.titleLabel.text = item.name
        cell.descriptionLabel.text = item.description.htmlToString
        cell.pageControl.numberOfPages = items.count
        cell.pageControl.currentPage = index
        
        cell.onRightPressed = {
            print(index)
            let right = index + 1
            if right <= self.items.count - 1 {
                collectionView.scrollToItem(at: .init(row: right, section: 0), at: .centeredHorizontally, animated: true)
            }
            collectionView.reloadData()
        }
        
        cell.onLeftPressed = {
            let left = index - 1
            if left >= 0 {
                collectionView.scrollToItem(at: .init(row: left, section: 0), at: .centeredHorizontally, animated: true)
            }
            collectionView.reloadData()
        }
        
        print(cell.bounds.height)
        
        return cell
    }
}

extension AHInfoViewControllers: UICollectionViewDelegate {
    
}

class AHClusterController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var items: [Item]!
    var mainController: ViewController!
    var onSearch: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @IBAction func handleDismissButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
}

class TableViewCell: UITableViewCell {
    @IBOutlet var iconView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var volumeLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
}

extension AHClusterController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewCell
        let item = items[indexPath.row]
        
        cell.iconView.kf.setImage(with: item.actualImageUrl)
        cell.titleLabel.text = item.name
        if let price = item.price {
            if price == 0.0 {
                cell.priceLabel.isHidden = true
            }
            cell.priceLabel.text = String(price)
        }
        cell.volumeLabel.isHidden = true
        
        
        return cell
    }
}

extension AHClusterController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(identifier: "AHInfoviewController") as! AHInfoViewController
        vc.item = items[indexPath.row]
        vc.delegate = self
        vc.mainController = mainController
        vc.onSearch = {
            let infoViewController = self.storyboard?.instantiateViewController(identifier: "info") as! InfoViewController
            infoViewController.pointOfInterest = self.items[indexPath.row].asPointOfInterest
            infoViewController.onCancel = {
                TT2.shared.mapController.deselectMarkers()
            }
            self.mainController.present(infoViewController, animated: true)
        }
        self.present(vc, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
