//
//  BottomSheet.swift
//  tt2-example-swift
//
//  Created by Jesper Lundqvist on 2020-06-12.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit
import Domain
import Kingfisher
import Mapbox
import TT2_SDK

class BottomSheetContainer: UIView {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return subviews.contains(where: { $0.hitTest(convert(point, to: $0), with: event) != nil })
    }
}

class BottomSheet: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var backButton: UIButton!
    
    private var fullStop: CGFloat = 24.0
    
    private var partialStop: CGFloat {
        view.bounds.height - headerView.bounds.height - 0.0
    }
    
    private var hasAnimatedIn = false
    
    private var search: SearchService = SearchService()
    private var searchResults: [Item] = []
    
    var mainController: ViewController!
    
    var yOffset: CGFloat {
        get {
            self.view.frame.origin.y
        }
        
        set {
            self.view.frame = CGRect(x: 0, y: newValue, width: view.frame.width, height: view.frame.height)
        }
    }
    
    var myListStop: CGFloat = (UIScreen.main.bounds.height / 2) + 40
    var myList = false {
        didSet {
            searchResults.removeAll()
            if myList {
                headerView.isHidden = true
                //fullStop = myListStop
            }
            else {
                headerView.isHidden = false
                fullStop = 24.0
            }
            tableView.reloadData()
        }
    }
    var myListItems: [ShoppingListItem]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        panGesture.delegate = self
        view.addGestureRecognizer(panGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !hasAnimatedIn {
            yOffset = UIScreen.main.bounds.height
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !hasAnimatedIn {
            collapse(animated: true)
            hasAnimatedIn = true
        }
    }
    
    @IBAction func handleBackButtonPressed(_ sender: UIButton) {
        self.collapse(animated: true)
    }
    
    func continuePan(translation: CGFloat) {
        guard yOffset + translation >= fullStop else {
            yOffset = fullStop
            return
        }
        
        yOffset += translation
    }
    
    func finishPan(translation: CGFloat, velocity: CGFloat) {
        let targetY = velocity > 0 ? partialStop : fullStop
        searchBar.resignFirstResponder()
        UIView.animate(withDuration: 0.4) {
            self.yOffset = targetY
            self.view.layoutIfNeeded()
        }
    }
    
    func expand(animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.2) {
                self.yOffset = self.fullStop
            }
        }
        else {
            self.yOffset = self.fullStop
        }
        
        backButton.isHidden = true
    }
    
    func collapse(animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.2) {
                self.yOffset = self.partialStop
            }
        }
        
        backButton.isHidden = true
        searchBar.resignFirstResponder()
    }
    
    func hardCollapse(animated: Bool) {
        self.yOffset = UIScreen.main.bounds.height - headerView.bounds.height - 150//583.0
        backButton.isHidden = true
        searchBar.resignFirstResponder()
    }
    
    @objc func handlePanGesture(_ gesture: UIPanGestureRecognizer) {
        if !myList {
            let translation = gesture.translation(in: view)
            let velocity = gesture.velocity(in: view)
            switch gesture.state {
            case .changed:
                continuePan(translation: translation.y)
                gesture.setTranslation(.zero, in: view)
            case .ended, .failed, .cancelled:
                finishPan(translation: translation.y, velocity: velocity.y)
            default:
                break
            }
        }
    }
}

extension BottomSheet: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        let gesture = (gestureRecognizer as! UIPanGestureRecognizer)
        let direction = gesture.velocity(in: view).y

        let y = view.frame.minY
        if (y == fullStop && tableView.contentOffset.y == 0 && direction > 0) {
            tableView.isScrollEnabled = false
        } else {
            tableView.isScrollEnabled = true
        }

        return false
    }
}

extension BottomSheet: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.expand(animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        querySearch()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        querySearch()
    }
    
    private func querySearch() {
        guard let query = searchBar.text else {
            return
        }
        
        search.query(query, in: 0) { result in
            switch result {
            case .success(let items):
                self.searchResults = items
                self.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
}

class BottomSheetSearchResult: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet var icon: UIImageView!
}

class MyListTableViewCell: UITableViewCell {
    @IBOutlet var iconView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var massLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var checkMarkButton: UIButton!
    
    var onCheckButtonPressed: (() -> ())?
    
    @IBAction func handleCheckButtonPressed(_ sender: UIButton) {
        guard let onCheck = onCheckButtonPressed else {
            return
        }
        
        onCheck()
    }
}

class SearchItemCell: UITableViewCell {
    @IBOutlet var iconView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var volumeLabel: UILabel!
    @IBOutlet var salePriceLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    
    var handleSearchButtonPressed: (() -> ())?
    
    @IBAction func handleSearchButtonPressed(_ sender: UIButton) {
        guard let handleSearch = handleSearchButtonPressed else {
            return
        }
        
        handleSearch()
    }
}

extension BottomSheet: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if myList {
            let vc = storyboard?.instantiateViewController(identifier: "AHInfoviewController") as! AHInfoViewController
            if let item = myListItems[indexPath.row].item {
                vc.item = item
            }
            self.present(vc, animated: true)
            tableView.deselectRow(at: indexPath, animated: true)
        }
        else {
            searchBar.resignFirstResponder()
            let searchItem = searchResults[indexPath.row]
            tableView.deselectRow(at: indexPath, animated: true)
            if let store = TT2.shared.activeStore {
                TT2.shared.api.central.centralApi.getItemPosition(storeId: store.id, itemId: searchItem.id) { (result) in
                    switch result {
                    case .success(let itemPositions):
                        guard let itemPos = itemPositions.first else {
                            return
                        }
                        print(itemPos)
                        let item = Item(id: searchItem.id, name: searchItem.name, prettyName: searchItem.prettyName, description: searchItem.description, manufacturerName: searchItem.manufacturerName, imageUrl: searchItem.imageUrl, alternateIds: searchItem.alternateIds, ecoLabels: searchItem.ecoLabels, shelfTierIds: [itemPos.shelfTierId!], offerShelfTierIds: searchItem.offerShelfTierIds, offerItemId: searchItem.offerItemId, price: searchItem.price)
                        self.collapse(animated: true)
                        let infoViewController = self.storyboard?.instantiateViewController(identifier: "info") as! InfoViewController
                        infoViewController.pointOfInterest = item.asPointOfInterest
                        infoViewController.onCancel = { [weak self] in
                            self?.searchBar.text = nil
                        }
                        self.present(infoViewController, animated: true)
                    case .failure(let error):
                        if let image = searchItem.imageUrl {
                            Toaster.shared?.toast(title: "No position for item", subtitle: nil, iconUrl: URL(string: image))
                        }
                        else {
                            Toaster.shared?.toast(title: "No position for item", subtitle: nil, iconUrl: nil)
                        }
                        print(error.localizedDescription)
                    }
                }
            }
        }
    }
}

extension BottomSheet: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if myList {
            return myListItems.count
        }
        else {
            return searchResults.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if myList {
            let cell = tableView.dequeueReusableCell(withIdentifier: "myList") as! MyListTableViewCell
            
            let listItem = myListItems[indexPath.row]
            if let item = listItem.item {
                cell.titleLabel.text = item.name
                cell.iconView.kf.setImage(with: URL(string: item.imageUrl ?? ""))
                cell.priceLabel.text = String(item.price ?? 0.0)
            }
            
            cell.massLabel.text = listItem.volume
            
            cell.onCheckButtonPressed = {
                guard let item = listItem.item else {
                    return
                }
                
                if TT2.shared.assistant.pointOfInterestOverride == listItem.asPointOfInterest {
                    TT2.shared.assistant.clearOverride()
                }
                
                self.myListItems.remove(at: indexPath.row)
                self.mainController.myListItems = self.myListItems
                TT2.shared.assistant.delete(item.asPointOfInterest)
                if self.mainController.listIsOrdered {
                    guard self.myListItems.first != nil else {
                        TT2.shared.navigationManager.stopNavigation()
                        tableView.reloadData()
                        self.mainController.resetBottomSheetView()
                        return
                    }
                    
                    var pointsOfInterest: [PointOfInterest] = []
                    for item in self.myListItems {
                         pointsOfInterest.append(item.item!.asPointOfInterest)
                     }
                    TT2.shared.navigationManager.getBestPath(pois: pointsOfInterest)
                    
                    TT2.shared.assistant.override(with: (self.myListItems.first?.item!.asPointOfInterest)!)
                } else {
                    if let poi = self.mainController.findClosestPoi(userPostion: TT2.shared.positionManager.lastLocation!, pointsIfInterest: self.myListItems) {
                        TT2.shared.assistant.override(with: poi)
                    }
                }
                
                
                if self.myListItems.isEmpty {
                    TT2.shared.navigationManager.stopNavigation()
                }
                tableView.reloadData()
            }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "result-1") as! SearchItemCell
            let result = searchResults[indexPath.row]
            cell.titleLabel.text = result.name
            cell.volumeLabel.text = result.manufacturerName
            cell.iconView.kf.setImage(with: URL(string: result.imageUrl ?? ""), placeholder: UIImage(systemName: "mappin.circle.fill"))
            cell.priceLabel.text = String(result.price ?? 0.0)
            
            if result.manufacturerName.isEmpty {
                cell.volumeLabel.isHidden = true
            }
            cell.salePriceLabel.isHidden = true
            cell.handleSearchButtonPressed = {
                self.searchBar.resignFirstResponder()
                let searchItem = self.searchResults[indexPath.row]
                tableView.deselectRow(at: indexPath, animated: true)
                if let store = TT2.shared.activeStore {
                    TT2.shared.api.central.centralApi.getItemPosition(storeId: store.id, itemId: searchItem.id) { (result) in
                        switch result {
                        case .success(let itemPositions):
                            guard let itemPos = itemPositions.first else {
                                return
                            }
                            
                            let item = Item(id: searchItem.id, name: searchItem.name, prettyName: searchItem.prettyName, description: searchItem.description, manufacturerName: searchItem.manufacturerName, imageUrl: searchItem.imageUrl, alternateIds: searchItem.alternateIds, ecoLabels: searchItem.ecoLabels, shelfTierIds: [itemPos.shelfTierId!], offerShelfTierIds: searchItem.offerShelfTierIds, offerItemId: searchItem.offerItemId, price: searchItem.price)
                            self.collapse(animated: true)
                            let infoViewController = self.storyboard?.instantiateViewController(identifier: "info") as! InfoViewController
                            infoViewController.pointOfInterest = item.asPointOfInterest
                            infoViewController.onCancel = { [weak self] in
                                self?.searchBar.text = nil
                            }
                            self.present(infoViewController, animated: true)
                        case .failure(let error):
                            if let image = searchItem.imageUrl {
                                Toaster.shared?.toast(title: "No position for item", subtitle: nil, iconUrl: URL(string: image))

                            }
                            else {
                                Toaster.shared?.toast(title: "No position for item", subtitle: nil, iconUrl: nil)
                            }
                            print(error.localizedDescription)
                        }
                    }
                }
            }
            return cell
        }
    }
}
