//
//  SearchView.swift
//  tt2-example-swift
//
//  Created by Théodore Roos on 2020-12-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit
import Domain
import TT2_SDK

class SearchView: UIViewController, UITableViewDelegate {

    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!
    
    private var search: SearchService = SearchService()
    private var searchResults: [Item] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        
        searchBar.delegate = self
        searchBar.becomeFirstResponder()
    }
    
    @IBAction func handleGoBackButtonPressed(_ sender: Any) {
        self.dismiss(animated: true)
    }
}

extension SearchView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "result") as! SearchItemCell
        let result = searchResults[indexPath.row]
        cell.titleLabel.text = result.name
        cell.salePriceLabel.isHidden = true//result.manufacturerName
        if let price = result.price {
            cell.priceLabel.text = String(price)
        }
        else {
            cell.priceLabel.isHidden = true
        }
        
        cell.iconView.kf.setImage(with: URL(string: result.imageUrl ?? ""), placeholder: UIImage(systemName: "mappin.circle.fill"))
        cell.handleSearchButtonPressed = {
            self.dismiss(animated: true)
        }
        return cell
    }
}

extension SearchView: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        querySearch()
    }
    
    private func querySearch() {
        guard let query = searchBar.text else {
            return
        }
        
//        search.query(query) { result in
//            switch result {
//            case .success(let items):
//                self.searchResults = items
//                self.tableView.reloadData()
//            case .failure:
//                break
//            }
//        }
        
        search.query(query, in: 0) { result in
            switch result {
            case .success(let items):
                self.searchResults = items
                self.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
}
