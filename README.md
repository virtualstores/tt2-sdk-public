# TT2 Software Development Kit for iOS

### *Software development kit for building iOS-apps with Virtual Stores advanced indoor positioning system.*

## Table of contents
1. [Getting started](#getting-started)
2. [Dependencies](#dependencies)
3. [Definitions](#definitions)
4. [Structure](#structure)
    1. [Source/Map](#source-map)
    2. [Source/Positioning](#source-positioning)
    3. [Source/Navigation](#source-navigation)
    4. [Source/Event](#source-event)
    5. [Source/Utility](#source-utility)
5. [Examples](#examples)
6. [Development](#development)

## Getting started
To handle depencencies, we use CocoaPods. Therefore you will need CocoaPods installed on your computer before getting started. Some of the dependencies are in our own private CocoaPods spec repository, so you will also need to add it to your installation. Instructions for this can be found in the [VSPodSpecs repo](https://bitbucket.org/virtualstores/vspodspecs/src/master/README.md)

When CocoaPods is installed, you can simply run `pod install` in the root directory to install the dependencies. A tt2-sdk.xcworkspace file should have been created, which you can use to open the project.

Since the SDK is a framework, you can't run it on its own. You can test features using the example app in the "Examples"-folder. Note that you will need to run `pod install` in the example app to install all required dependencies as well.

## Dependencies
The SDK depends on a few different external frameworks, some of which are developed internally at Virtual Stores. These are:

- **Domain:** Contains structures and protocols for interacting with the API. Also contains some of the core business logic that is used throughout the apps.
- **Api:** Implementation of API protocols in Domain for fetching data from the API.
- **PositionKit:** Framework that manages the positioning system (VPS). The SDK provides an abstaction of PositionKit to make it easier to use with a map.
- **Mapbox:** External framework that manages the map. The reason why we use Mapbox instead of the built-in MapKit is so we can build the indoor maps using Mapbox Studio and deploy them to both iOS and Android simultaneously.
- **ClusterKit:** Since Mapbox for iOS does not handle clustering of map markers by default, we use this framework to manage clustering.
- **Firebase:** We use the machine learning models from Firebase to detect barcodes. We also have our own ML-model to detect when a scanned barcode is on a shelf in stores.

## Definitions

- **Navigation Space:** A space in which a user can navigate in. Could be a store, for example.
- **Location:** There is a difference between `TT2Position` and `TT2Location`. `TT2Location` is more than just a position in space, since it also contains information about course and how certain we are of the position.
- **Point of interest:** Something that can be placed on the map. For instance, a shopping list item or a message.
- **Assistant:** The assistant manages points of interest. Can be connected to a map through a `MapController`.

## Structure

### Source/Map
Contains files for handling the map and customizing the behavior and appearance of it. Most importantly, MapController.swift which contains the `MapController`-class which manages a Mapbox map view and position manager so that a user marker is visible.

To customize the user marker or map markers, there are protocols in the UserMarker and Marker folders which you can implement and register in the `MapController`.

In the Pathfinding-folder, you can find various files that are used to draw and calculate the pathfinding line.

### Source/Positioning
This folder contains files that manage positioning. The `PositionManager`-protocol is something that can manage a user location. There is a `VPSManager`, which is an implementation of this protocol, in the VPS-folder.

There is also various files that can be used to synchronize the user position. For example, the `ItemBarcodeSynchronizer`, which checks the position of barcodes with the item API and synchronizes the user position. There is also a default barcode scanner here, which can easily be integrated with the item barcode synchronizer.

### Source/Navigation
The navigation folder has files which are used to navigate the user around a space. There is a `NavigationManager` which calculate a path from the user to the closest point of interest. The `ZoneNavigator` can be used to navigate according to zones, which can be useful in stores where you want users to follow a specific path.

### Source/Event
This folder has files which manages events that can occur while the user walks around the space. Right now we have a area events and proximity events, which both have event managers. These event managers are used to detect when events are triggered. To add an event handler, implement the trigger protocol for the event.

Area events are events which occur when the user is within a area. These areas can be fetched from the information manager API or be provided manually.

Proximity events are events which occur when the user is close to a point of interest. The trigger can be used to add a filter and customize the trigger distance.

### Source/Utility
This folder contains various utility files which are used internally in the SDK. Right now we have some data structures which are useful for the pathfinding algorithm we are using.

## Examples
To set up a map with the VPS controlling the user location, you can use the `MapController`-class.

```
class ViewController: UIViewController {
   let mapController: MapController?
   let mapView: MGLMapView?
   let positionManager: PositionManager?

   func viewDidLoad() {
       // Navigation space can be retrieved from the API
       let navigationSpace = fetchNavigationSpace()

       // Initialize position manager with VPS and use mapfence from the navigation space
       self.positionManager = VPSManager(data: navigationSpace.mapfence)

       // Initialize the map controller
       self.mapController = MapController(mapView: self.mapView, navigationSpace: navigationSpace, positionManager: self.positionManager)

       // Synchronize the position and start the position manager
       self.positionManager.synchronize(location: TT2Location(position: .init(x: 10.0, y: 10.0), course: TT2Course(fromDegrees: 0.0), confidence: 1.0, radius: 0.0, validSync: true))
   }
}
```

If you want to use an assistant with the `MapController`, simply set `mapController.assistant` to a any assistant, and the map will stay synchronized when adding and removing points of interest.

To navigate on the map, you can connect a `NavigationManager` by setting `mapController.navigationManager`. When the navigation manager is turned on, the map will draw a path to the closest point of interest according to the assitant. To choose how the assistant determines the closest point of interest, set the `navigator` variable on the assistant. You can use the default `ZoneNavigator` for simple navigation along a certain path.

You can also customize the appearence of the markers and user marker by registering your own markers.

More information about each class can be found in the code documentation.

## Development
### Branches
We have two primary branches, master and development. Master should always contain the latest released version of the SDK, while development is for versions that are in development. Changes should be pushed to separate branches and be merged into development before merging to master.

### Releasing
To push a new release to the Virtual Stores pod spec repository, read the instructions in the [repository](https://bitbucket.org/virtualstores/vspodspecs/src/master/README.md).

When releasing to an external customer, you should use an .xcframework, so that we can hide proprietary code.
