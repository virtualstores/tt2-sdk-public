Pod::Spec.new do |spec|

  spec.name         = "tt2-sdk"
  spec.version      = "1.1.1"
  spec.summary      = "Indoor positioning for iOS."
  spec.description  = "Software development kit for using the advanced indoor positioning system developed by Virtual Stores."
  spec.homepage     = "http://virtualstores.se"
  spec.author       = {
    "Jesper Lundqvist" => "jesper.lundqvist@virtualstores.se",
    "Théodore Roos" => "theodore.roos@virtualstores.se",
    "Emil Bond" => "emil.bond@virtualstores.se",
    "Philip Fryklund" => "philip.fryklund@virtualstores.se",
    "Carl-Johan Dahlman" => "carljohan.dahlman@virtualstores.se"
  }
  spec.license      = "Proprietary"
  spec.source       = { :git => 'https://bitbucket.org/virtualstores/tt2-sdk-public.git', :tag => "#{spec.version}" }
  spec.static_framework = true

  # Platform
  spec.platform              = :ios
  spec.ios.deployment_target = "12.0"
  spec.swift_version         = "5.0"

  # Source files
  spec.source_files  = "Source/**/*.{swift}"

  # Dependencies
  spec.dependency 'Domain', '~> 1.6.3'
  spec.dependency 'Api', '~>  1.6.3'
  spec.dependency 'PositionKit', '1.0.61'

  spec.dependency 'Mapbox-iOS-SDK', '~> 5.6.0'
  spec.dependency 'ClusterKit/Mapbox', '~> 0.4.1'

  spec.dependency 'GoogleMLKit/BarcodeScanning', '~> 0.62.0'
  spec.dependency 'AWSS3'
end
