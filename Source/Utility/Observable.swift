//
//  Observable.swift
//  tt2-sdk
//
//  Created by Philip Fryklund on 2020-02-28.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Foundation



/**
A weak ref observable
*/
internal class Observable<T> {
    
    typealias Observer = (T) -> Void
    private var observers: [UInt: WeakRef<ObserverToken<Observer>>] = [:]
    private var totalNumberOfObservers: UInt = 0
    
    /// - Returns: An object that must be stored with a strong reference to keep it in memory, once token is released the observer closure is also released.
    final func observe(_ observer: @escaping Observer) -> Any {
        let id = totalNumberOfObservers
        totalNumberOfObservers += 1
        let token = ObserverToken(id: id, observer: observer, observable: self)
        observers[id] = WeakRef(reference: token)
        return token
    }
    
    func notify(_ value: T) {
        observers.forEach { _, weakRef in
            weakRef.reference?.observer(value)
        }
    }
}

extension Observable: ObservableTokenRemovable {
    
    fileprivate final func removeToken(for id: UInt) {
        observers[id] = nil
    }
}



private struct WeakRef<T: AnyObject> {
    weak var reference: T?
}

private class ObserverToken<T> {
    
    let id: UInt
    let observer: T
    weak var observable: ObservableTokenRemovable?
    
    init(id: UInt, observer: T, observable: ObservableTokenRemovable?) {
        self.id = id
        self.observer = observer
        self.observable = observable
    }
    
    deinit {
        observable?.removeToken(for: id)
    }
}

private protocol ObservableTokenRemovable: class {
    func removeToken(for id: UInt)
}
