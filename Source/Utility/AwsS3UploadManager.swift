//
//  AwsS3UploadManager.swift
//  tt2-sdk
//
//  Created by Emil Bond on 2020-11-23.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Foundation
import AWSS3


public class AwsS3UploadManager {
    private var files = Dictionary<String, Data>()
    private var filesInUpload : [String] = []
    typealias Async = (_ success: @escaping () -> Void, _ failure: @escaping (Error) -> Void) -> Void
    private static let MAX_TRIES = 20
    
    init(){
    }
    
    public func prepareDataToSend(identifier: String, data: String) {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HHmmss"
        let timedIdentifier = identifier + timeFormatter.string(from: TT2.shared.date)
        if files[timedIdentifier] == nil {
            if let convertedData = data.data(using: .utf8) {
                files[timedIdentifier] = convertedData
            }
        }
    }
    
    public func addAditionalData(identifier: String, data: String) {
        if files[identifier] == nil {
            if let convertedData = data.data(using: .utf8) {
                files[identifier] = convertedData
            }
        }
    }
    
    public func sendCollectedDataToS3(folderName: String) {
        for (identifier, data) in files {
            if !filesInUpload.contains(identifier) {
                retry(
                    task: { self.sendToS3(key: folderName, identifier: identifier, data: data) },
                    success: {}, // add delegate if you want respond to success/failure
                    failure: { err in print("Failure uploading test files")      
                })
            }
            filesInUpload.append(identifier)
        }
    }
    
    public func clearCollectedData(timeStamp: String) {
        for (identifier, _) in files {
            if identifier.contains(timeStamp) {
                files[identifier] = nil
            }
        }
    }
    
    func retry(_ numberOfTimes: Int = 0, task: @escaping () -> Async, success: @escaping () -> Void, failure: @escaping (Error) -> Void) {
        DispatchQueue.global(qos: .background).async {
            sleep(self.getWaitTimeExp(retryCount: numberOfTimes))
            task()(success, { error in
                if numberOfTimes < AwsS3UploadManager.MAX_TRIES {
                    self.retry(numberOfTimes + 1, task: task, success: success, failure: failure)
                } else {
                    failure(error)
                }
            })
        }
    }
    
    private func getWaitTimeExp(retryCount: Int) -> UInt32 {
        if retryCount == 0 {
            return 0
        }
        let waitTime = pow(2, retryCount)
        
        return UInt32(truncating: waitTime as NSNumber)
    }
    
    private func sendToS3(key: String, identifier: String, data: Data) -> Async {
        return { success, failure in
            let splitIdentifier = identifier.split(separator: ".")
            let strippedIdentifier = splitIdentifier[0].components(separatedBy: CharacterSet.decimalDigits).joined()
            var fileExtension = ".json"
            if splitIdentifier.count > 1 {
                fileExtension = "." + String(splitIdentifier[splitIdentifier.capacity - 1])
            }
            let getPreSignedURLRequest = AWSS3GetPreSignedURLRequest()
            getPreSignedURLRequest.bucket = "product-information-storage"
            getPreSignedURLRequest.key = "data-analyze/" + key + strippedIdentifier + fileExtension
            getPreSignedURLRequest.httpMethod = .PUT
            getPreSignedURLRequest.expires = Date(timeIntervalSinceNow: 3600)
            
            //Important: set contentType for a PUT request.
            let fileContentTypeStr = "text/plain"
            getPreSignedURLRequest.contentType = fileContentTypeStr
            AWSS3PreSignedURLBuilder.default().getPreSignedURL(getPreSignedURLRequest).continueWith { (task:AWSTask<NSURL>) -> Any? in
                if let error = task.error as NSError? {
                    failure(error)
                    return nil
                }
                
                let presignedURL = task.result
                var request = URLRequest(url: presignedURL! as URL)
                request.cachePolicy = .reloadIgnoringLocalCacheData
                request.httpMethod = "PUT"
                request.setValue(fileContentTypeStr, forHTTPHeaderField: "Content-Type")
                URLSession.shared.uploadTask(with: request, from: data) { (responseData, response, error) in
                    if let error = error {
                        print("Failed to upload \(identifier), trying again")
                        failure(error)
                    } else {
                        self.files[identifier] = nil
                        print("Successfully uploaded \(identifier) to S3")
                        success()
                    }
                }.resume()
                return nil
            }
        }
    }
}
