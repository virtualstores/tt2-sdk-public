//
//  BundleExtension.swift
//  tt2-sdk
//
//  Created by Emil Bond on 2020-12-09.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Foundation

public extension Bundle {
    var appName: String {
        return infoDictionary?["CFBundleName"] as! String
    }

    var bundleId: String {
        return bundleIdentifier!
    }

    var versionNumber: String {
        return infoDictionary?["CFBundleShortVersionString"] as! String
    }

    var buildNumber: String {
        return infoDictionary?["CFBundleVersion"] as! String
    }
}
