//
//  DebugTools.swift
//  tt2-sdk
//
//  Created by Théodore Roos on 2020-11-10.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Foundation
import Mapbox

public class DebugTools {
    let mapView: MGLMapView
    
    public init(mapView: MGLMapView) {
        self.mapView = mapView
    }
    
    public func printTappedLocation() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(triggerTouchAction(_:)))
        mapView.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func triggerTouchAction(_ recognizer: UITapGestureRecognizer) {
        let location = recognizer.location(in: nil)
        let coordinate = mapView.convert(location, toCoordinateFrom: mapView)
        let mapPoint = mapView.convert(coordinate, toPointTo: mapView)
        let mapPointTest = mapView.superview?.convert(location, to: mapView)
        print(coordinate)
    }
}
