//
//  ProximityTrigger.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-04-16.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Domain

/**
 Implement this protocol to listen to proximity events.
 
 Works similar to a delegate protocol. When the event is triggered, the trigger() function will be called. You can filter when the event is triggered by implementing the filter() and distance() functions.
 */
public protocol ProximityTrigger {
    /**
     Applies a filter so that only points of interests that fulfill this function can be triggered.
     
     - Parameter pointOfInterest: The point of interest to check if it can trigger this trigger.
     */
    func filter(pointOfInterest: PointOfInterest) -> Bool
    
    /**
     How close the user needs to be a point of interest to trigger it. Specified in meters.
     */
    var triggerDistance: Double { get }
    
    /**
     If this trigger is enabled or not. If a trigger is disabled, it will not recieve events,
     */
    var enabled: Bool { get }
    
    /**
     This function will be called when the event is triggered, which means that the user is close to a point of interest.
     
     - Parameter pointOfInterest: The point of interest that triggered this event.
     */
    func trigger(pointOfInterest: PointOfInterest)
}
