//
//  EventTrigger.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-07-23.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit
import CoreGraphics
import Domain

public protocol EventArea {
    func intersects(point: TT2Position) -> Bool
}

extension CGRect: EventArea {
    public func intersects(point: TT2Position) -> Bool {
        self.contains(CGPoint(x: point.x, y: point.y))
    }
}

extension UIBezierPath: EventArea {
    public func intersects(point: TT2Position) -> Bool {
        self.contains(CGPoint(x: point.x, y: point.y))
    }
}

public protocol AreaEvent {
    var area: EventArea { get }
    var asPointOfInterest: PointOfInterest { get }
}

public protocol AreaTrigger {
    var enabled: Bool { get }
    func filter(event: AreaEvent) -> Bool
    func trigger(event: AreaEvent)
}
