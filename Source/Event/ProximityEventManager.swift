//
//  ProximityEventManager.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-04-16.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Domain

/**
 Manages events that can occur when the user walks close to a trigger.
 */
public class ProximityEventManager {
    private var proximityListeners: [String : ProximityTrigger]
    private var timer: Timer?
    private let assistant: Assistant
    private let positionManager: PositionManager
    private let interval: Double
    private let scale: Double
    
    /**
     Initializes a proximity event manager. Requires and assistant to determine where points of interest are and a position manager to determine where the user is. Checks position every second by default, but can be adjusted.
     
     - Parameter assistant: The assistant which has the points of interest that can trigger events.
     - Parameter positionManager: The position manager which determines the user location.
     - Parameter checkInterval: The interval in which to check if any events have triggered. Once every second by default.
     */
    public init(assistant: Assistant, positionManager: PositionManager, checkInterval: Double = 1.0) {
        self.assistant = assistant
        self.positionManager = positionManager
        self.proximityListeners = [:]
        self.interval = checkInterval
        self.scale = Double(TT2Position.scale)
    }
    
    /**
     Adds an observer that looks for proximity events with the settings specified in the proxmity trigger object.
     
     - Parameter id: The id of the observer. Needs to be unique since it is used to remove this observer.
     - Parameter event: This object contains a function that will be called when the event is triggered. You can also specify trigger distance and filters in the object.
     */
    public func addObserver(id: String, event: ProximityTrigger) {
        if proximityListeners.isEmpty {
            self.timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block: { _ in self.checkEvents() })
        }
        
        proximityListeners[id] = event
    }
    
    /**
     Removes an observer with an id.
     
     - Parameter id: The id of the observer to remove. Needs to be the same as the id used in addObserver.
     */
    public func removeObserver(id: String) {
        proximityListeners.removeValue(forKey: id)
        
        if proximityListeners.isEmpty {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    private func distance(from poi: PointOfInterest) -> Double {
        guard let itemPosition = poi.position, let userLocation = positionManager.lastLocation else {
            return .infinity
        }
        
        let userPosition = userLocation.position
        let mapPosition = CGPoint(x: Double(itemPosition.x) / scale, y: Double(itemPosition.y) / scale)
    
        return sqrt(pow(userPosition.x - Double(mapPosition.x), 2.0) + pow(userPosition.y - Double(mapPosition.y), 2.0))
    }
    
    private func checkEvents() {
        for poi in self.assistant.pointsOfInterest {
            for (_, event) in proximityListeners {
                if event.enabled, event.filter(pointOfInterest: poi), distance(from: poi) < event.triggerDistance {
                    event.trigger(pointOfInterest: poi)
                }
            }
        }
    }
}
