//
//  AreaEventManager.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-07-23.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Domain

/**
 Manages events that can be triggered when the user is in a area on the map. Areas can be added manually or fetched from a server.
 */
public class AreaEventManager {
    private var listeners: [String : AreaTrigger]
    
    private let informationManagerApi: InformationManagerApi?
    private let positionManager: PositionManager
    private let navigation: Navigation
    
    private var timer: Timer?
    private let interval: Double
    
    private var messages: [Message]
    private var latestMessageLoad: Date?
    private let reloadMessageInterval: TimeInterval = 3600.0
    
    /**
     Initializes an area event manager from the information manager API. Messages will be fetched from the API automatically and be reloaded every hour.
     
     - Parameter positionManager: The position manager that determines the user location.
     - Parameter informationManagerApi: The API object that you want to use to fetch the messages.
     - Parameter navigation: The space in which we are navigating in.
     - Parameter checkInterval: The interval in which to check if any events have been triggered.
     */
    public init(positionManager: PositionManager, informationManagerApi: InformationManagerApi, navigationSpace: NavigationSpace, checkInterval: Double = 1.0) {
        self.positionManager = positionManager
        self.informationManagerApi = informationManagerApi
        self.navigation = navigationSpace.navigation
        self.interval = checkInterval
        self.listeners = [:]
        self.messages = []
    }
    
    /**
     Initializes an area event manager from preset messages.
     
     - Parameter positionManager: The position manager that determines the user location.
     - Parameter messages: A list of all possible messages that can be triggered.
     - Parameter navigation: The space in which we are navigating in.
     - Parameter checkInterval: The interval in which to check if any events have been triggered.
     */
    public init(positionManager: PositionManager, messages: [Message], navigation: Navigation, checkInterval: Double = 1.0) {
        self.positionManager = positionManager
        self.informationManagerApi = nil
        self.messages = messages
        self.navigation = navigation
        self.interval = checkInterval
        self.listeners = [:]
    }
    
    /**
     Add an observer which will be notified when the user triggers an event. The AreaTrigger object can be used to customize when the event will be triggered.
     
     - Parameter id: The id of this observer. Needs to be unique since this is used to remove the observer.
     - Parameter event: The event object has a function that will be called when the event is triggered and can also be used to filter events.
     */
    public func addObserver(id: String, event: AreaTrigger) {
        if listeners.isEmpty {
            self.timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block: { _ in self.checkEvents() })
        }
        
        listeners[id] = event
    }
    
    /**
     Removes an observer with a id.
     
     - Parameter id: The id of the observer to remove. Needs to be the same as the id when the observer was added.
     */
    public func removeObserver(id: String) {
        listeners.removeValue(forKey: id)
        
        if listeners.isEmpty {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    private func loadMessages() {
        informationManagerApi?.getMessages { result in
            switch result {
            case .success(let messages):
                self.messages = messages
                self.latestMessageLoad = .init()
            case .failure:
                break
            }
        }
    }
    
    private func loadMessagesIfNeeded() {
        guard informationManagerApi != nil else {
            return
        }
        
        if let latestMessageLoad = latestMessageLoad {
            if latestMessageLoad.timeIntervalSinceNow < -reloadMessageInterval {
                loadMessages()
            }
        }
        else {
            loadMessages()
        }
    }
    
    private func checkEvents() {
        loadMessagesIfNeeded()
        
        guard positionManager.isUpdatingPosition else {
            return
        }
        
        for message in messages {
            for (_, listener) in listeners where listener.enabled {
                if listener.filter(event: message) {
                    if let latestUserLocation = positionManager.lastLocation, let storeHeight = navigation.getRtlsOption()?.height {
                        let latestUserLocationInCoords = TT2Position(x: latestUserLocation.position.x/20.0, y: (Double(storeHeight) - latestUserLocation.position.y)/20.0)
                        if message.area.intersects(point: latestUserLocationInCoords) {
                            listener.trigger(event: message)
                        }
                    }
                }
            }
        }
    }
}

extension Message: AreaEvent {
    public var area: EventArea {
        let curve = UIBezierPath()
        self.zones.forEach { zone in
            guard let firstCoord = zone.first else {
                return
            }
            
            let zoneCurve = UIBezierPath()
            zoneCurve.move(to: firstCoord)
            
            zone[1...].forEach { point in
                zoneCurve.addLine(to: point)
            }
            
            curve.append(zoneCurve)
        }
        return curve
    }
}
