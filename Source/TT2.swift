//
//  TT2.swift
//  tt2-sdk
//
//  Created by Théodore Roos on 2020-10-19.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Api
import Domain
import Mapbox

public class TT2 {
    public static let shared = TT2()
    
    public private(set) var mapController: MapController!
    public private(set) var api: Api!
    public private(set) var positionManager: PositionManager!
    public private(set) var assistant: Assistant!
    public private(set) var navigationSpace: NavigationSpace!
    public private(set) var navigationManager: NavigationManager!
    public private(set) var stores: [Store] = []
    public var activeStore: Store?
    public private(set) var debugTools: DebugTools!
    public private(set) var uploadManager: AwsS3UploadManager!
    
    public var positionStateObserverToken: Any?
    public var date = Date()
    
    public func initiate(clientId: Int64, serverUrl: URL, apiKey: String) {
        let api = ApiImpl(centralConnection: .init(id: 0, storeId: 1, serverUrl: serverUrl.absoluteString, mqttUrl: "", apiKey: apiKey), clientId: clientId)
        self.api = api
    }
    
    public func activate(storeId: Int64, mapView: MGLMapView, completion: @escaping (Error?) -> Void) {
        guard !stores.isEmpty else {
            print("Get stores before you activate the positioning system")
            return
        }
        
        initiateStore(storeId: storeId) { (error) in
            switch error {
            case .none:
                self.mapController = MapController(mapView: mapView, navigationSpace: self.navigationSpace, positionManager: self.positionManager)
                self.mapController.assistant = TT2.shared.assistant
                self.mapController.navigationManager = TT2.shared.navigationManager
                self.mapController.clusterCellSize = 250.0
                
                self.debugTools = DebugTools(mapView: mapView)
                self.validateSession(forStore: storeId)
                self.uploadManager = AwsS3UploadManager()
                
                completion(nil)
            case .some(let error):
                completion(error)
            }
        }
    }
    
    public func getStores(onlyActive: Bool = false, completion: @escaping (Error?) -> Void) {
        api.central.centralApi.getStores { (result) in
            switch result {
            case .success(let stores):
                if onlyActive {
                    stores.forEach { (store) in
                        if !self.stores.contains(where: { $0.name == store.name }), store.active {
                            self.stores.append(store)
                        }
                    }
                }
                else {
                    self.stores = stores
                }
                
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    public func getItemPosition(barcode: String, completion: @escaping (Dictionary<TT2Item, [TT2ItemPosition]>?, Error?) -> Void) {
        guard let store = self.activeStore?.id else {
            print("No store initalized")
            return
        }
        print("StoreID \(store)")
        
        api?.central.centralApi.getItemPosition(storeId: store, itemId: barcode, completion: { (result) in
            switch result {
            case .success(let barcodePositions):
                var tt2Item: TT2Item?
                if let shelve = barcodePositions.first?.shelfTierId, let shelfId = barcodePositions.first?.shelfId {
                    tt2Item = TT2Item(id: String(shelfId), shelfTierIds: [shelve])
                }
                
                var tt2Positions: [TT2ItemPosition] = []
                for positions in barcodePositions {
                    tt2Positions.append(TT2ItemPosition(id: positions.barcode!, x: Float(positions.itemPositionX ?? Double.nan), y: Float(positions.itemPositionY ?? Double.nan), offsetX: Float(positions.itemPositionOffsetX ?? Double.nan), offsetY: Float(positions.itemPositionOffsetY ?? Double.nan)))
                }
                
                
                guard let safeTT2Item = tt2Item else {
                    return
                }
                
                let dict = [safeTT2Item: tt2Positions]
                
                completion(dict, nil)
            case .failure(let error):
                completion(nil, error)
            }
        })
    }
    
    public func getItemPositions(barcodes: [String], completion: @escaping (Dictionary<TT2Item, [TT2ItemPosition]>?, Error?) -> Void) {
        var dict: Dictionary<TT2Item, [TT2ItemPosition]> = [:]
        var testerror: Error?
        let group = DispatchGroup()
        
        for barcode in barcodes {
            group.enter()
            getItemPosition(barcode: barcode) { (tempDict, error) in
                switch error {
                case .none:
                    guard let key = tempDict?.keys.first, dict[key] == nil else {
                        group.leave()
                        return
                    }
                    
                    dict[key] = tempDict?[key]
                    group.leave()
                case .some(let error):
                    testerror = error
                    group.leave()
                }
            }
        }
        
        group.notify(queue: .main) {
            completion(dict, testerror)
        }
    }

    private func validateSession(forStore: Int64) {
        if let sessionTimeout = UserDefaults.standard.object(forKey: "TT2_SESSION_VALIDITY") as? Date, let sessionId = UserDefaults.standard.string(forKey: "TT2_SESSION") {
            let diffComponents = Calendar.current.dateComponents([.hour, .minute], from: sessionTimeout, to: Date())
            let hours = diffComponents.hour ?? 2
            let minutes = diffComponents.minute ?? 60
            if hours > 1 && minutes > 15 {
                activateSession(storeId: forStore, sessionId: sessionId)
            }
        } else {
            activateSession(storeId: forStore, sessionId: UUID().uuidString)
        }
    }
    
    private func activateSession(storeId: Int64, sessionId: String) {
        self.api.central.centralApi.postSessionActivate(storeId: storeId, sessionId: sessionId, device: getDeviceInformation()) { (result) in
            switch result {
            case .none:
                UserDefaults.standard.set(sessionId, forKey: "TT2_SESSION")
                UserDefaults.standard.set(Date() + 60 * 60, forKey: "TT2_SESSION_VALIDITY")
                print("Session Activated")
            case .some(let error):
                print(error)
            }
        }
    }
    
    public func ordersDelivered(storeId: Int64, orderIds: [String]) {
        self.api.central.centralApi.postOrders(storeId: storeId, orderIds: orderIds, device: getDeviceInformation()) { (result) in
            switch result {
            case .none:
                print("Orders delivered: \(orderIds.count) in total")
            case .some(let error):
                print(error)
            }
        }
    }
    
    public func ordersDelivered(storeId: Int64, orderAmount: Int) {
        var orderIds: [String] = []
        for _ in 0...orderAmount {
            orderIds.append(UUID().uuidString)
        }
        self.api.central.centralApi.postOrders(storeId: storeId, orderIds: orderIds, device: getDeviceInformation()) { (result) in
            switch result {
            case .none:
                print("Orders delivered: \(orderAmount) in total, generated")
            case .some(let error):
                print(error)
            }
        }
    }
    
    private func getDeviceInformation() -> DeviceInformation {
        return DeviceInformation(id: UIDevice.current.identifierForVendor?.uuidString ?? "unknown", operatingSystem: UIDevice.current.systemName, osVersion: UIDevice.current.systemVersion, appVersion: "\(Bundle.main.appName)_\(Bundle.main.versionNumber)(\(Bundle.main.buildNumber))@\(Bundle(identifier: "org.cocoapods.TT2-SDK")?.appName)_\(Bundle(identifier: "org.cocoapods.TT2-SDK")?.versionNumber)(\(Bundle(identifier: "org.cocoapods.TT2-SDK")?.buildNumber))", deviceModel: UIDevice.current.modelName)
    }

    public func uploadData(folderName: String) {
        self.uploadManager.sendCollectedDataToS3(folderName: folderName)
    }
    
    public func sendData(firstName: String?, lastName: String?, route: String?, gender: String?, age: String?, comments: String?) {
        let uploadTimeFormatter = DateFormatter()
        let uploadDayFormatter = DateFormatter()
        uploadTimeFormatter.dateFormat = "HHmmss"
        uploadDayFormatter.dateFormat = "yyMMdd"
        let date = uploadDayFormatter.string(from: TT2.shared.date)
        let time = uploadTimeFormatter.string(from: TT2.shared.date)
        var folderName: String = "\(date)/undefined/ios/undefinedRoute/\(time)/"
        var csvData = "day,firstname,surname,device,route,time,gender,age,comments,\n"
        
        if let first = firstName, let last = lastName, let route = route {
            folderName = "\(date)/\(firstName)_\(lastName)/ios/\(route)/\(time)/"
            csvData = csvData + "\(date),\(firstName),\(lastName),ios,\(route),\(time),\(gender),\(age),\(comments),\n"
            self.uploadManager.addAditionalData(identifier: "keyWords.csv", data: csvData)
        }
        else if let route = route {
            folderName = "\(date)/undefined/ios/\(route)/\(time)/"
            csvData = csvData + "\(date),,,ios,\(route),\(time),\(gender),\(age),\(comments),\n"
            self.uploadManager.addAditionalData(identifier: "keyWords.csv", data: csvData)
        }
        self.uploadManager.sendCollectedDataToS3(folderName: folderName)
    }

    private func initiateStore(storeId: Int64, completion: @escaping (Error?) -> Void) {
        self.api.central.centralApi.getNavigationSpace(for: storeId) { result in
            switch result {
            case .success(let space):
                self.navigationSpace = space
                Item.Navigation = space.navigation
                
                self.assistant = Assistant()
                self.assistant.navigator = ZoneNavigator()
                if let navigator = self.assistant.navigator as? ZoneNavigator {
                    if space.mapZones.count > 0 {
                        space.mapZones.forEach { (zoneObject) in
                            let zone = zoneObject.zone
                            let path = UIBezierPath()
                            if zone.count > 0 {
                                path.move(to: zone[0])
                                for i in 1...zone.count - 1 {
                                    path.move(to: zone[i])
                                }
                            }
                            navigator.addZone(zone: NavigationZone(from: path))
                        }
                    }
                    else {
                        if let width = self.navigationSpace.navigation.getRtlsOption()?.width, let height = self.navigationSpace.navigation.getRtlsOption()?.height {
                            let path = UIBezierPath()
                            path.move(to: CGPoint(x: 0.0, y: 0.0))
                            path.move(to: CGPoint(x: 0.0, y: Double(height) / 20.0))
                            path.move(to: CGPoint(x: Double(width) / 20, y: Double(height) / 20))
                            path.move(to: CGPoint(x: Double(width) / 20, y: 0.0))
                            navigator.addZone(zone: NavigationZone(from: path))
                        }
                    }
                }
                
                self.positionManager = VPSManager(with: space.mapfence)
                self.navigationManager = NavigationManager(positionManager: self.positionManager, assistant: self.assistant, navigationSpace: self.navigationSpace)
                
                if let store = self.stores.first(where: { $0.id == storeId }) {
                    if !store.connection.serverUrl.isEmpty {
                        self.api.initStoreApi(storeConnection: store.connection)
                    }
                    self.activeStore = store
                }
                
                
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
}

public struct TT2Item: Hashable {
    public let id: String
    public let name: String
    public let shelf: [Int64]
    public let asPointOfInterest: PointOfInterest
    
    public init(id: String, shelfTierIds: [Int64]) {
        let item = Item(id: id, shelfTierIds: shelfTierIds)
        self.id = item.id
        self.name = item.name
        self.shelf = item.shelfTierIds!
        self.asPointOfInterest = item.asPointOfInterest
    }
}

public struct TT2ItemPosition: Hashable {
    public let id: String
    public let x: Float
    public let y: Float
    public let offsetX: Float
    public let offsetY: Float
    
    init(id: String, x: Float, y: Float, offsetX: Float, offsetY: Float) {
        self.id = id
        self.x = x
        self.y = y
        self.offsetX = offsetX
        self.offsetY = offsetY
    }
}

private extension Item {
    convenience init(id: String, shelfTierIds: [Int64]?) {
        self.init(id: id, name: "", prettyName: "", description: "", manufacturerName: "", imageUrl: nil, alternateIds: nil, ecoLabels: nil, shelfTierIds: shelfTierIds, offerShelfTierIds: nil, offerItemId: nil, price: nil)
    }
}

private extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}
