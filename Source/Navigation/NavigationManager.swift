//
//  NavigationManager.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-07-28.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Domain
import CoreLocation
/**
 The state in which the NavigationManger is in.
 */
public enum NavigationManagerState {
    /**
     The navigation manager has started navigating.
     */
    case started
    
    /**
     The navigation manager has stopped navigating.
     */
    case stopped
}

public enum PathingPart {
    case head
    case body
    case tail
}

/**
 A navigation manager can navigate the user to points of interest in a navigation space.
 */
public class NavigationManager {
    private var positionManager: PositionManager
    private var assistant: Assistant
    private var navigationSpace: NavigationSpace
    
    private var pathfinder: Pathfinder
    private var currentPath: [CGPoint]?
    private var currentBodyPath: [CGPoint]?
    private var currentTailPath: [CGPoint]?
    
    private var timer: Timer?
    private let interval: Double
    private var bodyTimer: Timer?
    private let bodyInterval: Double
    private var tailTimer: Timer?
    private let tailInterval: Double
    
    private var pathObservers: [String:(_ target: PointOfInterest, _ path: [CGPoint], _ PathingPart: PathingPart) -> Void]
    private var stateObservers: [String:(_ state: NavigationManagerState) -> Void]
    
    /**
     Is true if navigation is turned on.
     */
    public private(set) var navigating: Bool

    /**
     Initiializes the navigation manager.
     
     - Parameter positionManager: The position manager that determines the user location.
     - Parameter assistant: The assistant with the points of interest to navigate to.
     - Parameter navigationSpace: The space in which you want to navigate in.
     - Parameter interval: The interval in which to reload the pathfinding line.
     */
    public init(positionManager: PositionManager, assistant: Assistant, navigationSpace: NavigationSpace, interval: Double = 0.5) {
        self.positionManager = positionManager
        self.assistant = assistant
        self.navigationSpace = navigationSpace
        self.interval = interval
        self.bodyInterval = 0.5
        self.tailInterval = 2.0
        self.pathfinder = Pathfinder(fromData: navigationSpace.navgraph, pixelHeight: navigationSpace.height * Double(TT2Position.scale))
        self.navigating = false
        self.pathObservers = [:]
        self.stateObservers = [:]
    }
    
    /**
     Add an observer which listens to path changes. The path will be from the user location to the current target in meters with the origin at the top-left corner.
     
     - Parameter id: A unique id string for this observer. Used when removing the observer. Note that this id is unique for path observers, but can be reused for a state observer.
     - Parameter callback: The function that will be called when the path changes. It sends two values, the target that we are currently navigating to, and a list of all the points in the path.
     */
    public func addPathObserver(id: String, callback: @escaping (_ target: PointOfInterest, _ path: [CGPoint], _ pathingPart: PathingPart) -> Void) {
        self.pathObservers[id] = callback
    }
    
    /**
     Removes an obvserver added with addPathObserver.
     
     - Parameter id: The id of the observer. The same as the id in addPathObserver.
     */
    public func removePathObserver(id: String) {
        self.pathObservers.removeValue(forKey: id)
    }
    
    /**
    Add an observer which listens to navigation state changes.
    
    - Parameter id: A unique id string for this observer. Used when removing the observer. Note that this id is unique for state observers, but can be reused for a path observer.
    - Parameter callback: The function that will be called when the path changes. It sends the current state as a parameter.
    */
    public func addStateObserver(id: String, callback: @escaping (_ state: NavigationManagerState) -> Void) {
        self.stateObservers[id] = callback
    }
    
    /**
    Removes an obvserver added with addStateObserver.
    
    - Parameter id: The id of the observer. The same as the id in addStateObserver.
    */
    public func removeStateObserver(id: String) {
        self.stateObservers.removeValue(forKey: id)
    }
    
    
    /**
     Starts navigation. The state observer will be notifed and the path observer will start being notified in the interval specified in the initializer. The path obverser will continue until navigation is stopped.
     */
    public func startNavigation() {
        self.navigating = true
        self.timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block: { _ in
            self.reloadPathfinding()
        })
        self.bodyTimer = Timer.scheduledTimer(withTimeInterval: bodyInterval, repeats: true, block: { _ in
            guard self.currentBodyPath != nil, self.currentNavigavtionTarget != nil else {
                return
            }
            self.notifyPathObservers(target: self.currentNavigavtionTarget!, path: self.currentBodyPath!, pathingPart: PathingPart.body)
        })
        
        self.tailTimer = Timer.scheduledTimer(withTimeInterval: tailInterval, repeats: true, block: { _ in
            guard self.currentTailPath != nil, self.currentNavigavtionTarget != nil else {
                return
            }
            self.notifyPathObservers(target: self.currentNavigavtionTarget!, path: self.currentTailPath!, pathingPart: PathingPart.tail)
        })
        notifyStateObservers(state: .started)
        self.reloadPathfinding()
    }

    /**
     Stops navigation. The state observer will be notified and the path observer will no longer recieve updates.
     */
    public func stopNavigation() {
        self.navigating = false
        self.timer?.invalidate()
        self.bodyTimer?.invalidate()
        self.tailTimer?.invalidate()
        notifyStateObservers(state: .stopped)
    }
    
    private func computePath(_ userPosition: CGPoint, _ mapPosition: CGPoint) {
        if let path = pathfinder.getPath(startPoint: userPosition, goalPoint: mapPosition) {
            self.currentPath = path
        }
    }
    
    var currentNavigavtionTarget: PointOfInterest?
    private func reloadPathfinding() {
        guard
            let lastPosition = positionManager.lastLocation?.position,
            let height = navigationSpace.navigation.getRtlsOption()?.height,
            let navigationTarget = assistant.pointOfInterestOverride ?? assistant.closestPointOfInterest(from: CGPoint(x: lastPosition.x, y: Double(height) - lastPosition.y)),
            let itemPosition = navigationTarget.position
        else {
            self.currentNavigavtionTarget = nil
            self.currentPath = nil
            return
        }
        let userPoint = CGPoint(x: CGFloat(lastPosition.x), y: CGFloat(lastPosition.y))
        let userCoordinate = CLLocationCoordinate2D(latitude: Double(CGFloat(height) - userPoint.y)/(1000.0 / Double(TT2Position.scale)), longitude: Double(userPoint.x)/(1000.0 / Double(TT2Position.scale)))

        let mapPosition = CGPoint(x: Double(itemPosition.x * TT2Position.scale), y: Double(itemPosition.y * TT2Position.scale))
        let convertedHeight = Double(height) * Double(TT2Position.scale)
        let userPosition = CGPoint(x: userCoordinate.longitude*1000, y: (userCoordinate.latitude*1000))
        
        // Recompute path if user is wandering away from the line
        if let closestPoint = currentPath?.first, userPosition.distance(to: closestPoint) > 200.0 || currentNavigavtionTarget != navigationTarget {
            computePath(userPosition, mapPosition)
            currentNavigavtionTarget = navigationTarget
        }
        
        // Remove points which are close to the user
        if let maxIndexToRemove = self.currentPath?.indices.filter({ userPosition.distance(to: currentPath![$0]) < 80.0 }).max() {
            self.currentPath?.removeFirst(maxIndexToRemove)
        }
        
        if let currentPath = self.currentPath {
            notifyPathObservers(target: navigationTarget, path: currentPath, pathingPart: PathingPart.head)
        }
        else {
            // Recompute the path if we do not have one saved already
            computePath(userPosition, mapPosition)
        }
    }
    
    private func notifyPathObservers(target: PointOfInterest, path: [CGPoint], pathingPart: PathingPart) {
        self.pathObservers.forEach{ $0.value(target, path, pathingPart) }
    }
    
    private func notifyStateObservers(state: NavigationManagerState) {
        self.stateObservers.forEach{ $0.value(state) }
    }
    
    public func getBestPath(pois: [PointOfInterest]) -> [PointOfInterest] {
        var goals: [CGPoint] = []
        var userPosition = CGPoint(x: Double((TT2.shared.positionManager.lastLocation?.position.x)!), y: TT2.shared.navigationSpace.height - Double((TT2.shared.positionManager.lastLocation?.position.y)!))
        var goalPosition = CGPoint(x: Double(TT2.shared.navigationSpace.stopCodes.first!.coordinates!.x), y: TT2.shared.navigationSpace.height - Double(TT2.shared.navigationSpace.stopCodes.first!.coordinates!.y))
        goals.append(userPosition)
        for poi in pois {
            goals.append(CGPoint(x: Double(poi.position!.x), y: Double(poi.position!.y)))
        }
        goals.append(goalPosition)
        
        var distances = Array(repeating: Array(repeating: 0.0, count: goals.count), count: goals.count)
        var nodes = Array(repeating: (0.0, 0.0), count: goals.count)
        
        for i in 0...goals.count - 1 {
            for j in 0...goals.count - 1 {
                if (j > i) {
                    if let path = pathfinder.getPath(startPoint: CGPoint(x: Double(goals[i].x) * Double(TT2Position.scale), y: Double(goals[i].y) * Double(TT2Position.scale)), goalPoint: CGPoint(x: Double(goals[j].x) * Double(TT2Position.scale), y: Double(goals[j].y) * Double(TT2Position.scale))) {
                        guard path.count > 1 else {
                            continue
                        }

                        var pathDistance: CGFloat = 0.0
                        for pathIndex in 1...path.count - 1 {
                            pathDistance += sqrt(pow(path[pathIndex].x - path[pathIndex - 1].x, 2) + pow(path[pathIndex].y - path[pathIndex - 1].y, 2))
                        }
                        
                        distances[i][j] = Double(pathDistance)
                        distances[j][i] = Double(pathDistance)
                    }
                }

                nodes[i] = (Double(goals[i].x), Double(goals[i].y))
                distances[i][i] = Double.infinity
            }
        }
        guard goals.count > 2 else {
            return []
        }
        let tsp = TSP(size: goals.count, dist: distances, nodes: nodes)
        let order = tsp.solve()
        
        var orderedGoals: [CGPoint] = []
        var orderedPois: [PointOfInterest] = []
        for index in order {
            orderedGoals.append(CGPoint(x: Double(goals[index].x), y: Double(goals[index].y)))
            if !(index == 0 || index == order.count - 1) {
                orderedPois.append(pois[index - 1])
            }
        }
        
        
        guard !(orderedPois.count < 2) else {
            currentTailPath = nil
            currentBodyPath = nil
            return orderedPois
        }
        
        var tailPath: [CGPoint] = []
        var bodyPath: [CGPoint] = []
        for i in 1...orderedPois.count - 1 {
            let poi = orderedPois[i - 1]
            let poi2 = orderedPois[i]
            let start = CGPoint(x: Double(poi.position!.x) * Double(TT2Position.scale), y: Double(poi.position!.y) * Double(TT2Position.scale))
            let goal = CGPoint(x: Double(poi2.position!.x) * Double(TT2Position.scale), y: Double(poi2.position!.y) * Double(TT2Position.scale))
            if let path = pathfinder.getPath(startPoint: start, goalPoint: goal) {
                if i <= 2 {
                    bodyPath.append(contentsOf: path)
                } else {
                    tailPath.append(contentsOf: path)
                }
            }
        }
        // fix ending
        let poi = orderedPois[orderedPois.count - 1]
        let start = CGPoint(x: Double(poi.position!.x) * Double(TT2Position.scale), y: Double(poi.position!.y) * Double(TT2Position.scale))
        let goal = CGPoint(x: Double(orderedGoals.last!.x) * Double(TT2Position.scale), y: Double(orderedGoals.last!.y) * Double(TT2Position.scale))
        
        if let path = pathfinder.getPath(startPoint: start, goalPoint: goal) {
            tailPath.append(contentsOf: path)
        }
        self.currentTailPath = tailPath
        self.currentBodyPath = bodyPath
        guard self.currentTailPath != nil, self.currentNavigavtionTarget != nil else {
            return orderedPois
        }
        self.notifyPathObservers(target: self.currentNavigavtionTarget!, path: self.currentTailPath!, pathingPart: PathingPart.tail)
        
        return orderedPois
    }
}
