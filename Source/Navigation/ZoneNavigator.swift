//
//  ZoneNavigator.swift
//  Hemkop
//
//  Created by Jesper Lundqvist on 2020-02-07.
//  Copyright © 2020 VirtualStores. All rights reserved.
//

import Domain

public protocol NavigationBounds {
    func contains(_ point: CGPoint) -> Bool
}

extension CGRect: NavigationBounds { }
extension UIBezierPath: NavigationBounds { }

public class NavigationZone {
    let bounds: NavigationBounds
    
    init(from bounds: NavigationBounds, previous: NavigationZone? = nil) {
        self.bounds = bounds
        self.previous = previous
        previous?.next = self
    }
    
    weak var previous: NavigationZone?
    var next: NavigationZone?
}

/**
 Default navigation class. Tries to navigate users through zones in a specific order.
 
 This will navigate to all points of interest in the zone in which the users is currently located before moving on to the points of interest in the next zone. This can be used to make users follow a preset customer route.
 */
public class ZoneNavigator: AssistantNavigator {
    private var zones: [NavigationZone] = []
    
    private var recursionCounter = 0
    
    /**
     Adds a zone to this navigator. Zones should not interesect or else they might produce unexpected behavior.
     
     - Parameter zone: The zone to add.
     */
    public func addZone(zone: NavigationZone) {
        zones.append(zone)
    }
    
    private func zone(for point: CGPoint) -> NavigationZone? {
        return zones.first { $0.bounds.contains(point) }
    }
    
    private func getPosition(for poi: PointOfInterest) -> CGPoint? {
        poi.position?.cgPointwithoutOffset
    }
    
    private func closest(from position: CGPoint, in list: Set<PointOfInterest>, zone: NavigationZone) -> PointOfInterest? {
        var closestPoi: PointOfInterest?
        var closestDistance: CGFloat = .infinity
        
        for poi in list where poi.addedByUser {
            if let poiPosition = getPosition(for: poi), zone.bounds.contains(poiPosition) {
                let distance = position.distance(to: poiPosition)
                if distance < closestDistance {
                    closestDistance = distance
                    closestPoi = poi
                }
            }
        }
        
        if closestPoi == nil, let nextZone = zone.next {
            if recursionCounter > zones.count {
                return nil
            }
            
            recursionCounter += 1
            return closest(from: position, in: list, zone: nextZone)
        }
        
        return closestPoi
    }
    
    /**
     Determines the closest point of interest for a specific position according to the zone algorithm.
     
     Will not always be the closest since the algorithm will follow the zone order.
     
     - Parameter position: The position to check from.
     - Parameter list: The list of points of interest.
     */
    public func closestPointOfInterest(from position: CGPoint, in list: Set<PointOfInterest>) -> PointOfInterest? {
        if let fromZone = zone(for: position) {
            recursionCounter = 0
            return closest(from: position, in: list, zone: fromZone)
        }
        else {
            return nil
        }
    }
}
