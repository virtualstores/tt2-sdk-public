//
//  MapController.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-03-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Mapbox
import Domain
import ClusterKit

/**
 Manages the Mapbox MGLMapView so that the map is loaded correctly and the user is placed in the right spot. Can also be connected to an Assistant to enable points of interest and a NavigationManager to enable pathfinding.
 
 ## Connecting an Assistant
 The assistant is a class that manages points of interests that can be placed on the map. To connect an assistant, you can set the value of the "assistant" property. When you set this property, all points of interests added to the assistant will be automatically placed on the map. When a point of interest is removed or modified, the map will be updated so that it is always synchronized with the assistant. If you want to handle presses on markers, you can implement the didPressMarker()-function in the MapControllerDelegate.
 
 ## Connecting a NavigationManager
 A navigation manager will navigate to points in the map. To connect a navigation manager, set the value of the "navigationManager" property. When the navigation manager starts navigating, the map will automatically update and draw a path from the user to the point. Further customization of the navigation can be done in the NavigationManager class.
 
 ## Clustering markers
 Markers of points of interest will build clusters if they are too close. To adjust the clustering, you can use the "clusterCellSize"-property. When navigating to a point of interest, this marker will be temporarily removed from the cluster while the user is navigating.
 
 ## Custom markers
 The SDK provides default views for the user marker, point of interest markers and cluster views. This can be customized using the register()-functions. If you want to make your own custom user marker view, create a new class that implements the UserMarkerView protocol and then use register(userMarkerType: ClassName.Type) to register it. Read more specifics about implementing custom markers on the protocols.
 
 ## Basic example
 ```
 class ViewController: UIViewController {
    let mapController: MapController?
    let mapView: MGLMapView?
    let positionManager: PositionManager?
 
    func viewDidLoad() {
        // Navigation space can be retrieved from the API
        let navigationSpace = fetchNavigationSpace()
 
        // Initialize position manager with VPS and use mapfence from the navigation space
        self.positionManager = VPSManager(data: navigationSpace.mapfence)
 
        // Initialize the map controller
        self.mapController = MapController(mapView: self.mapView, navigationSpace: navigationSpace, positionManager: self.positionManager)
 
        // Synchronize the position and start the position manager
        self.positionManager.synchronize(location: TT2Location(position: .init(x: 10.0, y: 10.0), course: TT2Course(fromDegrees: 0.0), confidence: 1.0, radius: 0.0, validSync: true))
    }
 }
 ```
 */
public class MapController: NSObject {
    /**
     Implement the delegate protocol and set this property if you want to respond to customize the behavior of the MapController further.
     */
    public var delegate: MapControllerDelegate?
    
    private let uuid: UUID = .init()
    
    private var mapView: MGLMapView
    private var loadingView: UIView
    private var loadingIndicator: UIActivityIndicatorView
    private let scale: Double
    
    private var navigationSpace: NavigationSpace
    private var positionManager: PositionManager
    private var positionObserverToken: Any?
    private var positionStateObserverToken: Any?
    
    private var userMarkerView: UserMarkerViewProtocol
    private var userMarkerAnnotation: UserMarkerAnnotation
    private var userMarkerType: UserMarkerViewProtocol.Type {
        didSet {
            userMarkerView = self.userMarkerType.init(annotation: userMarkerAnnotation)
        }
    }
    
    private var markerViewType: MarkerViewProtocol.Type
    private var clusterViewType: ClusterViewProtocol.Type
    
    private var path: PathRenderer
    private var bodyPath: PathRenderer
    private var tailPath: PathRenderer
    
    /**
     This value will adjust the cell size for each cluster. If this value is large, markers will cluster more easily, and if it's small they will not cluster as frequently.
     
     This value is passed directly to ClusterKit, which manages the clustering. More specifically, we use the CKNonHierarchicalDistanceBasedAlgorithm and this value will be passed to algorithms cell size.
     */
    public var clusterCellSize: Float {
        didSet {
            let clusterAlgorithm = CKNonHierarchicalDistanceBasedAlgorithm()
            clusterAlgorithm.cellSize = CGFloat(clusterCellSize)
            self.mapView.clusterManager.algorithm = clusterAlgorithm
            self.mapView.clusterManager.animationDuration = 0.2
        }
    }
    
    /**
     If the MapController has an non-nil assistant, the points of interest in the assistant will be automatically added to the map and stay synchronized when the assistant changes.
     
     If you set this value to nil, all markers on the map will be removed.
     */
    public var assistant: Assistant? {
        didSet {
            assistant?.removeObserver(with: self.uuid.uuidString)
            assistant?.addObserver(with: self.uuid.uuidString, observer: self.assistantObserver)
        
            if let assistant = assistant {
                // Add any existing markers
                assistantObserver(Array(assistant.pointsOfInterest), .added)
            }
            else {
                // Remove all current markers
                if let mapAnnotations = mapView.annotations {
                    mapView.removeAnnotations(mapAnnotations.filter({ $0 is MarkerAnnotation }))
                }
                mapView.clusterManager.removeAnnotations(mapView.clusterManager.annotations.filter({ $0 is MarkerAnnotation }))
            }
        }
    }
    
    /**
     When this value is set, the map will stay synchronized with the navigation manager and draw a pathfinding line when the navigation manager is navigating to something.
     
     To further customize the navigation behavior, you can use the navigation manager.
     */
    public var navigationManager: NavigationManager? {
        didSet {
            navigationManager?.removePathObserver(id: self.uuid.uuidString)
            navigationManager?.addPathObserver(id: self.uuid.uuidString, callback: self.navigationPathObserver)
            
            navigationManager?.removeStateObserver(id: self.uuid.uuidString)
            navigationManager?.addStateObserver(id: self.uuid.uuidString, callback: self.navigationStateObserver)
        }
    }
    
    /**
     This value controls whether the user location is currenly visible. The position manager may change this value when its state changes.
     */
    public var isShowingUserLocation: Bool = false {
        didSet {
            if oldValue != self.isShowingUserLocation {
                if self.isShowingUserLocation {
                    mapView.addAnnotation(userMarkerAnnotation)
                }
                else {
                    mapView.removeAnnotation(userMarkerAnnotation)
                }
            }
        }
    }
    
    /**
     Initializes a MapController. Requires a MGLMapView, a NavigationSpace and a PositionManager. To enable the assistant or navigation manager, set the properties after initializing.
     
     - Parameter mapView: The MGLMapView that this map should be drawn into.
     Note that the MapController will overwrite the styleURL, minimumZoomLevel, maximumZoomLevel and also set its delegate. Do not change the delegate for the MGLMapView, since this class requires it.
     
     - Parameter navigationSpace: The space that we are navigating in. If you want to change this after initializing, you will need to create a new MapController.
     
     - Parameter positionManager: The object that manages the user's position.
     
     */
    public init(mapView: MGLMapView, navigationSpace: NavigationSpace, positionManager: PositionManager) {
        self.positionManager = positionManager
        self.navigationSpace = navigationSpace
        self.loadingView = .init(frame: mapView.frame)
        self.loadingIndicator = UIActivityIndicatorView(frame: loadingView.frame)
        
        self.scale = Double(TT2Position.scale)
        self.mapView = mapView
        self.mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.mapView.styleURL = navigationSpace.mapUrl
        self.mapView.minimumZoomLevel = 6
        self.mapView.maximumZoomLevel = 11
        
        self.clusterCellSize = 250.0
        let clusterAlgorithm = CKNonHierarchicalDistanceBasedAlgorithm()
        clusterAlgorithm.cellSize = CGFloat(clusterCellSize)
        self.mapView.clusterManager.algorithm = clusterAlgorithm
        self.mapView.clusterManager.animationDuration = 0.2
        
        self.userMarkerAnnotation = UserMarkerAnnotation()
        self.userMarkerType = TT2UserMarkerView.self
        self.userMarkerView = userMarkerType.init(annotation: userMarkerAnnotation)
        
        self.markerViewType = TT2MarkerView.self
        self.clusterViewType = TT2ClusterView.self
        
        self.path = PathRenderer()
        self.bodyPath = PathRenderer()
        self.tailPath = PathRenderer()
        
        TT2Position.navigationSpaceHeight = Float(navigationSpace.height)
                
        super.init()
        
        self.createLoadingActivityIndicator()
        self.mapView.addSubview(self.loadingView)
        
        positionManager.startUpdatingLocation()
        
        self.mapView.delegate = self
        positionObserverToken = self.positionManager.observeLocation(observer: self.positionManagerObserver)
        positionStateObserverToken = self.positionManager.observeState(observer: self.positionStateObserver)
        resetCameraToMapBounds()
    }
    
    deinit {
        assistant?.removeObserver(with: self.uuid.uuidString)
    }
    
    /**
     Registers a custom user marker. The view needs to conform to UserMarkerViewProtocol.
     
     - Parameter userMarkerType: The type of the custom marker.
     */
    public func register(userMarkerType: UserMarkerViewProtocol.Type) {
        self.userMarkerType = userMarkerType
    }
    
    /**
     Registers a custom point of interest marker. The view needs to conform to MarkerViewProtocol.
     
     - Parameter markerType: The type of the custom marker.
    */
    public func register(markerType: MarkerViewProtocol.Type) {
        self.markerViewType = markerType
    }
    
    /**
     Registers a custom cluster view. The view needs to conform to ClusterViewProtocol.
     
     - Parameter clusterViewType: The type of the custom view.
     */
    public func register(clusterViewType: ClusterViewProtocol.Type) {
        self.clusterViewType = clusterViewType
    }
    
    /**
     Selects a marker by referencing its point of interest.
     
     - Parameter pointOfInterest: The point of interest associated with the marker that should be selected.
     */
    public func selectMarker(pointOfInterest: PointOfInterest) {
        mapView.annotations?.forEach {
            if let marker = $0 as? MarkerAnnotation, marker.pointOfInterest == pointOfInterest {
                mapView.selectAnnotation(marker, animated: true, completionHandler: nil)
            }
        }
    }
    
    /**
     Deselects all markers.
     */
    public func deselectMarkers() {
        mapView.selectedAnnotations.forEach({ mapView.deselectAnnotation($0, animated: true) })
    }
    
    private func positionManagerObserver(_ result: Result<TT2Location, PositionError>) {
        switch result {
        case .success(let location):
            guard !location.position.x.isNaN, !location.position.y.isNaN else {
                break
            }
            
            isShowingUserLocation = true
            
            let point = CGPoint(x: CGFloat(location.position.x), y: CGFloat(self.navigationSpace.height - location.position.y))
            UIView.animate(withDuration: 0.2) {
                self.userMarkerAnnotation.location = .init(latitude: Double(point.y)/(1000.0/self.scale), longitude: Double(point.x)/(1000.0/self.scale))
                self.userMarkerView.userLocationDidChange(location)
            }
            
        case .failure(let error):
            delegate?.handlePositionError(error: error)
            switch error {
            case .lostPosition:
                isShowingUserLocation = false
            case .illegalBehavior:
                break
            case .uploadFailure(_):
                break
            case .trolley:
                break
            }
        }
    }
    
    private func positionStateObserver(_ state: PositionState) {
        DispatchQueue.main.async {
            switch state {
            case .beginSync:
                self.mapView.addSubview(self.loadingView)
            case .ready:
                self.dismissLoadingScreen()
            case .started:
                break
            case .stopped:
                self.isShowingUserLocation = false
            case .uploadedRecording(let error):
                self.dismissLoadingScreen()
                if let error = error {
                    self.delegate?.handlePositionError(error: .uploadFailure(error))
                }
                else {
                    self.isShowingUserLocation = false
                }
            case .sendData:
                self.dismissLoadingScreen()
            }
        }
    }
    
    private func assistantObserver(_ pointsOfInterest: [PointOfInterest], _ state: AssistantObserverState) {
        switch state {
        case .added:
            for poi in pointsOfInterest {
                if let itemPosition = poi.position {
                    let annotation = MarkerAnnotation(pointOfInterest: poi)
                    let mapPosition = CGPoint(x: Double(itemPosition.x), y: Double(itemPosition.y))
                    
                    let lat = (mapPosition.y/1000) * CGFloat(self.scale)
                    let long = (mapPosition.x/1000) * CGFloat(self.scale)
                    
                    annotation.coordinate = .init(latitude: Double(lat), longitude: Double(long))
                    mapView.clusterManager.addAnnotation(annotation)
                }
            }
        case .removed:
            mapView.annotations?.first(where: {
                if $0 is MarkerAnnotation {
                    let markerAnnotation = $0 as! MarkerAnnotation
                    if markerAnnotation.pointOfInterest == pointsOfInterest.first {
                        mapView.removeAnnotation(markerAnnotation)
                        return true
                    }
                }
                else if $0 is CKCluster {
                    var index = 0
                    let cluster = $0 as! CKCluster
                    while index < cluster.annotations.count {
                        let annotation = cluster.annotation(at: UInt(index))
                        
                        if let markerAnnotation = annotation as? MarkerAnnotation {
                            if markerAnnotation.pointOfInterest == pointsOfInterest.first {
                                mapView.clusterManager.removeAnnotation(markerAnnotation)
                                return true
                            }
                        }
                        
                        index += 1
                    }
                }
                return false
            })
        case .edited:
            break
        }
    }
    
    private func reloadPathfindingTarget(target: PointOfInterest?) {
        // Remove target from cluster
        mapView.annotations?.forEach { annotation in
            if let cluster = annotation as? CKCluster, cluster.count > 1 {
                let clusterAnnotations: [AnyObject] = cluster.annotations
                let clusterMarkers = clusterAnnotations.compactMap { $0 as? MarkerAnnotation }
                clusterMarkers.filter({ $0.pointOfInterest == target }).forEach({ marker in
                    mapView.clusterManager.removeAnnotation(marker)
                    mapView.addAnnotation(marker)
                })
            }
        }
        
        // Add all markers that isn't the target to cluster manager
        mapView.annotations?.forEach { annotation in
            if let marker = annotation as? MarkerAnnotation, marker.pointOfInterest != target {
                mapView.removeAnnotation(marker)
                mapView.clusterManager.addAnnotation(marker)
            }
        }
    }
    
    var currentTarget: PointOfInterest? = nil
    private func navigationPathObserver(_ target: PointOfInterest, path: [CGPoint], pathingPart: PathingPart) {
        self.reloadPathfindingTarget(target: target)
        switch pathingPart {
        case .head:
            if TT2.shared.assistant.pointsOfInterest.count <= 1 {
                bodyPath.isHidden = true
                tailPath.isHidden = true
            }
            self.path.update(coordinates: path.map { point in
                return CLLocationCoordinate2D(latitude: (Double(point.y)/1000.0), longitude: (Double(point.x)/1000.0))
            })
        case .body:
            self.bodyPath.update(coordinates: path.map { point in
                return CLLocationCoordinate2D(latitude: (Double(point.y)/1000.0), longitude: (Double(point.x)/1000.0))
            })
        case .tail:
            self.tailPath.update(coordinates: path.map { point in
                return CLLocationCoordinate2D(latitude: (Double(point.y)/1000.0), longitude: (Double(point.x)/1000.0))
            })
        default:
            self.path.update(coordinates: path.map { point in
                return CLLocationCoordinate2D(latitude: (Double(point.y)/1000.0), longitude: (Double(point.x)/1000.0))
            })
        }
    }
    
    private func navigationStateObserver(_ state: NavigationManagerState) {
        switch state {
        case .started:
            path.isHidden = false
            bodyPath.isHidden = false
            tailPath.isHidden = false
        case .stopped:
            path.isHidden = true
            bodyPath.isHidden = true
            tailPath.isHidden = true
            reloadPathfindingTarget(target: nil)
        }
    }
    
    public func resetCameraToMapBounds() {
        if let width = navigationSpace.navigation.getRtlsOption()?.width, let height = navigationSpace.navigation.getRtlsOption()?.height {
            let sw = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
            let ne = CLLocationCoordinate2D(latitude: Double(height)/20.0, longitude: Double(width)/20.0)
            
            // TODO: better way to calculate padding
            let camera = self.mapView.cameraThatFitsCoordinateBounds(.init(sw: sw, ne: ne), edgePadding: .init(top: 30.0, left: 5.0, bottom: 30.0, right: 5.0))
            self.mapView.setCamera(camera, withDuration: 0.0, animationTimingFunction: .none)
            let scale: CGFloat = 1.5 //CGFloat(mapView.zoomLevel/mapView.minimumZoomLevel)
            self.userMarkerView.mapViewDidChange(self.mapView, scale: scale)
        }
        else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.resetCameraToMapBounds()
            }
        }
    }
    
    private func _resetCameraToTargetBounds(target: CLLocationCoordinate2D) {
        var smallX = 0.0
        var bigX = 0.0
        var smallY = 0.0
        var bigY = 0.0
        
        let userPosition = CLLocationCoordinate2D(latitude: (self.positionManager.lastLocation?.position.inCoordinates.latitude)! / 1000, longitude: (self.positionManager.lastLocation?.position.inCoordinates.longitude)! / 1000)
        
        if userPosition.latitude < target.latitude {
            smallY = userPosition.latitude
            bigY = target.latitude
        } else {
            smallY = target.latitude
            bigY = userPosition.latitude
        }
        if userPosition.longitude < target.longitude {
            smallX = userPosition.longitude
            bigX = target.longitude
        } else {
            smallX = target.longitude
            bigX = userPosition.longitude
        }
        let sw = CLLocationCoordinate2D(latitude: smallY, longitude: smallX)
        let ne = CLLocationCoordinate2D(latitude: bigY, longitude: bigX)
        
        let padding: CGFloat = 50.0
        let camera = self.mapView.cameraThatFitsCoordinateBounds(.init(sw: sw, ne: ne), edgePadding: .init(top: padding, left: padding, bottom: padding, right: padding))
        //self.mapView.setCamera(camera, withDuration: 0.0, animationTimingFunction: .none)
        
        self.mapView.setCamera(camera, animated: true)
    }
    
    private func dismissLoadingScreen() {
        UIView.animate(withDuration: 0.5, animations: {
                self.loadingView.alpha = 0.0
                self.loadingIndicator.alpha = 0.0
        }) { (_) in
            self.loadingView.removeFromSuperview()
            self.loadingView.alpha = 1.0
            self.loadingIndicator.alpha = 1.0
        }
    }
    
    private func createLoadingActivityIndicator() {
        self.loadingIndicator.startAnimating()
        if #available(iOS 13.0, *) {
            self.loadingView.backgroundColor = .secondarySystemBackground
            loadingIndicator.style = .large
        } else {
            self.loadingView.backgroundColor = .lightGray
        }
        self.loadingView.addSubview(loadingIndicator)
        loadingIndicator.centerXAnchor.constraint(equalTo: self.loadingView.centerXAnchor).isActive = true
        loadingIndicator.centerYAnchor.constraint(equalTo: self.loadingView.centerYAnchor).isActive = true
    }
    
    func polygonCircleForCoordinate(coordinate: CLLocationCoordinate2D, withMeterRadius: Double) {
        let degreesBetweenPoints = 8.0
        //45 sides
        let numberOfPoints = floor(360.0 / degreesBetweenPoints)
        let distRadians: Double = withMeterRadius / 6371000.0
        // earth radius in meters
        let centerLatRadians: Double = coordinate.latitude * Double.pi / 180
        let centerLonRadians: Double = coordinate.longitude * Double.pi / 180
        var coordinates = [CLLocationCoordinate2D]()
        //array to hold all the points
        for index in 0 ..< Int(numberOfPoints) {
            let degrees: Double = Double(index) * Double(degreesBetweenPoints)
            let degreeRadians: Double = degrees * Double.pi / 180
            let pointLatRadians: Double = asin(sin(centerLatRadians) * cos(distRadians) + cos(centerLatRadians) * sin(distRadians) * cos(degreeRadians))
            let pointLonRadians: Double = centerLonRadians + atan2(sin(degreeRadians) * sin(distRadians) * cos(centerLatRadians), cos(distRadians) - sin(centerLatRadians) * sin(pointLatRadians))
            let pointLat: Double = pointLatRadians * 180 / Double.pi
            let pointLon: Double = pointLonRadians * 180 / Double.pi
            let point: CLLocationCoordinate2D = CLLocationCoordinate2DMake(pointLat, pointLon)
            coordinates.append(point)
        }
        let polygon = MGLPolygon(coordinates: &coordinates, count: UInt(coordinates.count))
        self.mapView.addAnnotation(polygon)
    }
}

extension MapController: MGLMapViewDelegate {
    public func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        self.path.add(to: mapView, identifier: "path_polyline", alpha: 1.0)
        self.bodyPath.add(to: mapView, identifier: "bodyPath_polyline", alpha: 0.2)
        self.tailPath.add(to: mapView, identifier: "tailPath_polyline", alpha: 0.2)
    }
    
    public func mapViewRegionIsChanging(_ mapView: MGLMapView) {
        let scale: CGFloat = 1.5 //CGFloat(mapView.zoomLevel/mapView.minimumZoomLevel)
//        let zoomStops = [
//            10: NSExpression(format: "\(scale) / 30"),
//            13: NSExpression(format: "\(scale) / 10")
//        ]
//        let testScale = NSExpression(format: "mgl_interpolate:withCurveType:parameters:stops:($zoomLevel, 'linear', nil, %@)", zoomStops)
        
        self.userMarkerView.mapViewDidChange(mapView, scale: scale)
    }
    
    public func mapView(_ mapView: MGLMapView, shouldChangeFrom oldCamera: MGLMapCamera, to newCamera: MGLMapCamera) -> Bool {
        let mapBounds = MGLCoordinateBounds(rect: CGRect(origin: .zero, size: CGSize(width: self.navigationSpace.width / 20, height: self.navigationSpace.height / 20)))
        if !(mapBounds.contains(coordinate: newCamera.centerCoordinate)) {
            mapView.setCenter(oldCamera.centerCoordinate, animated: false)
            return false
        }
        
        return true
    }
    
    public func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        if annotation is UserMarkerAnnotation {
            return self.userMarkerView
        }
        else if let marker = annotation as? MarkerAnnotation {
            let view = mapView.dequeueReusableAnnotationView(withIdentifier: marker.id)
            
            if let view = view {
                return view
            }
            
            return self.markerViewType.init(pointOfInterest: marker.pointOfInterest)
        }
        else if let cluster = annotation as? CKCluster {
            if cluster.count > 1 {
                return self.clusterViewType.init(cluster: cluster)
            }
            
            if let marker = cluster.firstAnnotation as? MarkerAnnotation {
                return self.markerViewType.init(pointOfInterest: marker.pointOfInterest)
            }
            
            return nil
        }
        else {
            return nil
        }
    }
    
    public func mapView(_ mapView: MGLMapView, regionDidChangeAnimated animated: Bool) {
        mapView.clusterManager.updateClustersIfNeeded()
    }
    
    public func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        if let markerAnnotation = annotation as? MarkerAnnotation {
            if let markerView = mapView.view(for: annotation) as? MarkerViewProtocol {
                markerView.wasSelected()
            }
            delegate?.didPressMarker(pointOfInterest: markerAnnotation.pointOfInterest)
        }
        else if let cluster = annotation as? CKCluster, cluster.count == 1, let markerAnnotation = cluster.firstAnnotation as? MarkerAnnotation {
            self.mapView(mapView, didSelect: markerAnnotation)
        }
        else if let cluster = annotation as? CKCluster, cluster.count > 1 {
            // Good zoom level for when pressing on a cluster
            //let padding: CGFloat = 150.0
            //let camera = mapView.cameraThatFitsCluster(cluster, edgePadding: UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding))
            //mapView.setCamera(camera, animated: true)
            var pois: [PointOfInterest] = []
            for i in 0...cluster.count - 1 {
                if let annotation = cluster[i] as? MarkerAnnotation {
                    pois.append(annotation.pointOfInterest)
                } else {
                    print("Poop")
                }
            }
            
            delegate?.didPressMarkers(pointOfInterests: pois)
        }
    }
    
    public func mapView(_ mapView: MGLMapView, didDeselect annotationView: MGLAnnotationView) {
        if let markerView = annotationView as? MarkerViewProtocol {
            markerView.wasDeselected()
        }
    }
}

/**
 This delegate can be used if you want to customize the behavior of the MapController.
 */
public protocol MapControllerDelegate {
    /**
     Handle a error that the position manager produces.
     */
    func handlePositionError(error: PositionError)
    
    /**
     Handle a marker press.
     
     - Parameter pointOfInterest: The point of interest that is associated with the marker that was pressed.
     */
    func didPressMarker(pointOfInterest: PointOfInterest)
    
    func didPressMarkers(pointOfInterests: [PointOfInterest])
}

public extension MGLCoordinateBounds {
    
    var center: CLLocationCoordinate2D {
        let width = ne.longitude - sw.longitude
        let height = ne.latitude - sw.latitude
        return CLLocationCoordinate2D(latitude: sw.latitude + height/2, longitude: sw.longitude + width/2)
    }
    
    func contains(coordinate: CLLocationCoordinate2D) -> Bool {
        return MGLCoordinateInCoordinateBounds(coordinate, self)
    }
    
    init(coordinates: [CLLocationCoordinate2D]) {
        guard coordinates.count > 1 else {
            let coordinate = coordinates.first ?? .init(latitude: 0, longitude: 0)
            self.init(sw: coordinate, ne: coordinate)
            return
        }
        
        var minLon = -Double.infinity
        var maxLon = Double.infinity
        var minLat = -Double.infinity
        var maxLat = Double.infinity
        for point in coordinates {
            if point.longitude < minLon {
                minLon = point.longitude
            }
            if point.longitude > maxLon {
                maxLon = point.longitude
            }
            if point.latitude < minLat {
                minLat = point.latitude
            }
            if point.latitude > maxLat {
                maxLat = point.latitude
            }
        }
        self.init(
            sw: CLLocationCoordinate2D(latitude: minLat, longitude: minLon),
            ne: CLLocationCoordinate2D(latitude: maxLat, longitude: maxLon)
        )
    }
    
    
    init(rect: CGRect) {
        self.init(
            sw: CLLocationCoordinate2D(latitude: Double(rect.minY), longitude: Double(rect.minX)),
            ne: CLLocationCoordinate2D(latitude: Double(rect.maxY), longitude: Double(rect.maxX))
        )
    }
}
