//
//  PathRenderer.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-07-28.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Mapbox
import CoreGraphics

class PathRenderer {
    private var polylineSource: MGLShapeSource?
    private var styleLayer: MGLStyleLayer?
    
    var isHidden: Bool = false {
        didSet {
            if isHidden {
                styleLayer?.isVisible = false
            }
            else {
                styleLayer?.isVisible = true
            }
        }
    }
    
    func add(to mapView: MGLMapView, identifier: String, alpha: CGFloat) {
        let polylineSource = MGLShapeSource(identifier: identifier, shape: nil, options: nil)
        mapView.style?.addSource(polylineSource)
        
        let lineStyleLayer = MGLLineStyleLayer(identifier: identifier, source: polylineSource)
        lineStyleLayer.lineJoin = NSExpression(forConstantValue: "round")
        lineStyleLayer.lineCap = NSExpression(forConstantValue: "round")
        lineStyleLayer.lineColor = NSExpression(forConstantValue: UIColor.systemBlue.withAlphaComponent(alpha))
        lineStyleLayer.lineWidth = NSExpression(format: "mgl_interpolate:withCurveType:parameters:stops:($zoomLevel, 'linear', nil, %@)", [mapView.minimumZoomLevel: 5, mapView.maximumZoomLevel: 14])
        
        mapView.style?.addLayer(lineStyleLayer)
        
        self.polylineSource = polylineSource
        self.styleLayer = lineStyleLayer
    }
    
    private func cutCorners(on path: [CGPoint], iterations: UInt) -> [CGPoint] {
        if iterations == 0 {
            return path
        }
        
        var newPath: [CGPoint] = []
        for (i, point) in path.enumerated() {
            if i == 0 {
                newPath.append(point)
            }
            else {
                let previousPoint = path[i-1]
                if i < path.count-1 {
                    let nextPoint = path[i+1]
                    
                    let firstVector = CGPoint(x: point.x - previousPoint.x, y: point.y - previousPoint.y)
                    newPath.append(CGPoint(x: previousPoint.x + (firstVector.x * 0.8), y: previousPoint.y + (firstVector.y*0.8)))
                    
                    let secondVector = CGPoint(x: nextPoint.x - point.x, y: nextPoint.y - point.y)
                    newPath.append(CGPoint(x: point.x + (secondVector.x * 0.2), y: point.y + (secondVector.y*0.2)))
                }
                else {
                    newPath.append(point)
                }
            }
        }
        
        return cutCorners(on: newPath, iterations: iterations-1)
    }
    
    private func reduceComplexity(of path: [CGPoint]) -> [CGPoint] {
        var newPath: [CGPoint] = []
        
        var didPlaceLast = false
        for (i, point) in path.enumerated() {
            if i > 0 && i < path.count-1 {
                let nextPoint = path[i+1]
                let previousPoint = path[i-1]
                let firstAngle = atan2(nextPoint.y - point.y, nextPoint.x - point.x)
                let secondAngle = atan2(point.y - previousPoint.y, point.x - previousPoint.x)
                
                if firstAngle != secondAngle && !didPlaceLast {
                    newPath.append(point)
                    didPlaceLast = true
                }
                else {
                    didPlaceLast = false
                }
            }
            else {
                newPath.append(point)
            }
        }
        
        return newPath
    }
    
    func update(coordinates: [CLLocationCoordinate2D]) {
        var pathWithoutFirst = coordinates.map { CGPoint(x: $0.longitude, y: $0.latitude) }
        if coordinates.count > 1 {
            pathWithoutFirst.removeFirst(1)
        }
        let smoothCoordinates = cutCorners(on: reduceComplexity(of: pathWithoutFirst), iterations: 12).map {
            CLLocationCoordinate2D(latitude: Double($0.y), longitude: Double($0.x))
        }
        
        polylineSource?.shape = MGLPolylineFeature(coordinates: smoothCoordinates, count: UInt(smoothCoordinates.count))
    }
}
