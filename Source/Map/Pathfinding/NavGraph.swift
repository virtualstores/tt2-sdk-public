//
//  NavGraph.swift
//  tt2-sdk
//
//  Created by Emil Bond on 2019-12-11.
//  Copyright © 2019 Felix Andersson. All rights reserved.
//

import Foundation
import UIKit

class NavGraph {
    
    private let spacing: Int
    private let numVertices: Int
    private let vertices: Dictionary<String, Vertex>
    
    init( spacing: Int,
          numVertices: Int,
          vertices: Dictionary<String, Vertex>) {
        self.spacing = spacing
        self.numVertices = numVertices
        self.vertices = vertices
    }
    
    class Vertex {
        public let id: String
        private let x: CGFloat
        private let y: CGFloat
        public let cost: Int
        public let neighborCosts: Dictionary<String, Int>
        
        init(id: String,
             x: CGFloat,
             y: CGFloat,
             cost: Int,
             neighborCosts: Dictionary<String, Int>) {
            self.id = id
            self.x = x
            self.y = y
            self.cost = cost
            self.neighborCosts = neighborCosts
        }
        
        func equals(other: Any?) -> Bool {
            if type(of: other) != Vertex.self {
                return false
            }
            let other = other as! Vertex
            return self.id == other.id
        }
        
        func getCostTo(vertex: Vertex) -> Int? {
            return getCostToId(id: vertex.id)
        }
        
        func getCostToId(id: String) -> Int? {
            return neighborCosts[id]
        }
        
        func toPointF() -> CGPoint {
            return CGPoint(
                x: self.x,
                y: self.y
            )
        }
    }
    
    func getVertex(id: String) -> Vertex? {
        return vertices[id]
    }
    
    func getCost(fromVertexId: String, toVertexId: String) -> Int? {
        return vertices[fromVertexId]?.getCostToId(id: toVertexId)
    }
    
    func closestVertex(point: CGPoint) -> Vertex? {
        var minVertex: Vertex? = nil
        var minValue: CGFloat = 1000
        
        for(vertex) in vertices.values {
            let value = point.distance(to: vertex.toPointF())
            if(value < minValue) {
                minVertex = vertex
                minValue = value
            }
        }
        return minVertex
    }
}

extension CGPoint {
    func distance(to other: CGPoint) -> CGFloat {
        return sqrt(pow(self.x - other.x, 2) + pow(self.y - other.y, 2))
    }
}
