//
//  TSP.swift
//  tt2-sdk
//
//  Created by Emil Bond on 2021-01-13.
//  Copyright © 2021 Virtual Stores. All rights reserved.
//

import Foundation

class TSP {
    private let tourSize: Int
    private let dist: [[Double]]
    private let nodes: [(Double, Double)]
    init(size: Int, dist: [[Double]], nodes: [(Double, Double)]) {
        self.tourSize = size
        self.dist = dist
        self.nodes = nodes
    }
    
    private func greedy() -> [Int] {
        var leftToVisit = Array(0...tourSize - 1)
        var currentNode = leftToVisit.remove(at: 0)
        var solution = Array(0...tourSize - 1)
        solution[0] = currentNode
        
        var index = 1
        while index < (tourSize - 1) {
            var minI = 0
            var minVal = dist[currentNode][leftToVisit[0]]
            var innerIndex = 0
            while innerIndex < leftToVisit.count {
                let node = leftToVisit[innerIndex]
                if minVal > dist[currentNode][node] {
                    minI = innerIndex
                    minVal = dist[currentNode][node]
                }
                innerIndex += 1
            }
            currentNode = leftToVisit.remove(at: minI)
            solution[index] = currentNode
            index += 1
        }
        return solution
    }
    
    private func routeDistance(solution: [Int]) -> Double {
        var result = 0.0
        var index = 1
        while index < solution.count - 1 {
            result += dist[solution[index - 1]][solution[index]]
            index += 1
        }
        return result
    }
    
    private func reverse(array: [Int], start: Int, end: Int) -> [Int] {
        let halfLength = Int(floor(Double(((end + 1 - start) / 2))))
        var resultArray = array
        var index = 0
        while index < halfLength {
            let tmp = array[end - index]
            resultArray[end - index] = array[start + index]
            resultArray[start + index] = tmp
            index += 1
        }
        return resultArray
    }
    
    /**
     * Wiki definitions https://en.wikipedia.org/wiki/3-opt
     */
    private func threeOpt(tour: [Int]) -> [Int] {
        var solution = Array(0...tour.count - 1)
        var improvementDone = true
        
        while (improvementDone) {
            improvementDone = false
            for i in 1...solution.count - 1 where i + 2 <= solution.count - 1 {
                for j in (i + 2)...(solution.count - 1) where j + 2 <= solution.count - 1 {
                    for k in (j + 2)...(solution.count - 1) {
                        
                        let a = solution[i - 1]
                        let b = solution[i]
                        let c = solution[j - 1]
                        let d = solution[j]
                        let e = solution[k - 1]
                        let f = solution[k]

                        let d0 = dist[a][b] + dist[c][d] + dist[e][f]
                        let d1 = dist[a][c] + dist[b][d] + dist[e][f]
                        let d2 = dist[a][b] + dist[c][e] + dist[d][f]
                        let d3 = dist[a][d] + dist[e][b] + dist[c][f]
                        let d4 = dist[f][b] + dist[c][d] + dist[e][a]
                        
                        if d0 > d1 {
                            solution = reverse(array: solution, start: i, end: j - 1)
                            improvementDone = true
                        }
                        else if d0 > d2 {
                            solution = reverse(array: solution, start: j, end: k - 1)
                            improvementDone = true
                        }
                        else if d0 > d4 {
                            solution = reverse(array: solution, start: i, end: k - 1)
                            improvementDone = true
                        }
                        else if d0 > d3 {
                            let tmp = Array(solution[(j)...(k - 1)]) + Array(solution[(i)...(j - 1)])
                            solution.replaceSubrange((i)...(k - 1), with: tmp)
                            improvementDone = true
                        }
                        else {
                            improvementDone = false
                        }
                    }
                }
            }
        }
        return solution
    }
    
    private func isIntersect(
        p1: (Double, Double),
        q1: (Double, Double),
        p2: (Double, Double),
        q2: (Double, Double)
    ) -> Bool {
        let o1 = orientation(p: p1, q: q1, r: p2)
        let o2 = orientation(p: p1, q: q1, r: q2)
        let o3 = orientation(p: p2, q: q2, r: p1)
        let o4 = orientation(p: p2, q: q2, r: q1)

        return o1 != o2 && o3 != o4
    }
    
    private func orientation(
        p: (Double, Double),
        q: (Double, Double),
        r: (Double, Double)
    ) -> Int {
        let value = (q.1 - p.1) * (r.0 - q.0) - (q.0 - p.0) * (r.1 - q.1)
        if abs(value) <  1e-10 {
            return 0
        } else if value > 0 {
            return 1
        } else{
            return 2
        }
    }
    
    // This one is needed since our problem is not a regular TSP problem
    private func removeIntersects(tour: [Int]) -> [Int] {
        var solution = tour

        for i in 0...(solution.count - 1) where i + 2 < solution.count - 1 {
            for j in (i + 2)...(solution.count - 1) where j + 2 < solution.count - 1 {
                if (dist[solution[i]][solution[i + 1]] + dist[solution[j]][solution[j + 1]] - (dist[solution[i]][solution[j]] + dist[solution[i + 1]][solution[j + 1]]) > -1e-10) {
                    let p1 = nodes[solution[i]]
                    let q1 = nodes[solution[i + 1]]
                    let p2 = nodes[solution[j]]
                    let q2 = nodes[solution[j + 1]]

                    if isIntersect(p1: p1, q1: q1, p2: p2, q2: q2) {
                        solution = reverse(array: solution, start: i + 1, end: j)
                    }
                }
            }
        }
        return solution
    }
    
    func solve() -> [Int] {
        let greedySolution = self.threeOpt(tour: self.greedy())

        var optimalSolution = greedySolution
        var minDistance = routeDistance(solution: greedySolution)

        for _ in 0...20 {
            var randomTour = Array(1...tourSize - 2)
            randomTour.shuffle()
            randomTour.insert(0, at: 0)
            randomTour.append(tourSize - 1)
            
            let solution = threeOpt(tour: randomTour)
            if (routeDistance(solution: solution) < minDistance) {
                minDistance = routeDistance(solution: solution)
                optimalSolution = solution
            }
        }

        let solution = removeIntersects(tour: optimalSolution)
        return solution
    }
}
