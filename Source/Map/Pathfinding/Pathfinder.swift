//
//  Pathfinder.swift
//  tt2-sdk
//
//  Created by Emil Bond on 2019-12-12.
//  Copyright © 2019 Felix Andersson. All rights reserved.
//

import Foundation
import UIKit

private typealias PathResult = ([String: NavGraph.Vertex?], [String: Int])
//typealias PathFinder = (CGPoint) -> [CGPoint]?

private typealias Path = (vertex: NavGraph.Vertex, priority: Int)

private func < (m1: Path, m2: Path) -> Bool {
    return m1.priority < m2.priority
}

internal class Pathfinder {
    private let graph: NavGraph
    
    init(fromFile: String, pixelHeight: Double) {
        graph = GraphLoader().getGraph(fromFile: fromFile, pixelHeight: pixelHeight)
    }
    
    init(fromData: Data, pixelHeight: Double){
        graph = GraphLoader().getGraph(fromData: fromData, pixelHeight: pixelHeight)
    }
  
    //TODO: Change this to return a lamda as in android
//    func getPathFinderTo(start: CGPoint, goal: CGPoint) -> [CGPoint]? {
//        return getPath(startPoint: start, goalPoint: goal)
//    }
    
    func getPath(startPoint: CGPoint, goalPoint: CGPoint) -> [CGPoint]? {
        guard let start = graph.closestVertex(point: startPoint), let goal = graph.closestVertex(point: goalPoint) else {
            print("ERROR, cant find start or end for pathfinding")
            return nil
        }
        
        let pathResult = pathFind(graph: graph, start: start, goal: goal)
        let reconstructedPath = reconstructPath(cameFrom: pathResult!.0 as! [String: NavGraph.Vertex], start: start, goal: goal)
        return reconstructedPath.map {
            $0.toPointF()
        }
    }
    
    private func pathFind(graph: NavGraph, start: NavGraph.Vertex, goal: NavGraph.Vertex) -> PathResult? {
        var frontier = PriorityQueue<Path>(sort: <)
        
        var cameFrom = Dictionary<String, NavGraph.Vertex>()
        var costSoFar = Dictionary<String, Int>()
        
        frontier.enqueue((start, 0))
        cameFrom[start.id] = nil
        costSoFar[start.id] = 0
        
        while (!frontier.isEmpty) {
            let current = frontier.dequeue()
            if(current!.vertex.id == goal.id){
                break
            }
            
            current?.vertex.neighborCosts.keys.forEach({id in
                let next = graph.getVertex(id: id)!
                var newCost = 0
                if(costSoFar.keys.contains((current?.vertex.id)!)){
                    newCost += costSoFar[(current?.vertex.id)!]!
                }
                newCost += (current?.vertex.getCostToId(id: next.id))! + next.cost
                if(!costSoFar.keys.contains(next.id) || newCost < costSoFar[next.id]!) {
                    costSoFar[next.id] = newCost
                    frontier.enqueue((next, newCost))
                    cameFrom[next.id] = current?.vertex
                }
            })
        }
        if(cameFrom.keys.count == 1){
            return nil
        }
        
        return (cameFrom, costSoFar)
        
    }
    
    private func reconstructPath(cameFrom: Dictionary<String, NavGraph.Vertex>, start: NavGraph.Vertex, goal: NavGraph.Vertex) -> [NavGraph.Vertex]{
        
        var current = goal
        var path: [NavGraph.Vertex] = []
        while (current.id != start.id){
            path.append(current)
            current = cameFrom[current.id]!
        }
        path.append(start)
        path.reverse()
        return path
    }
}
