//
//  GraphLoader.swift
//  MapLibrary
//
//  Created by Emil Bond on 2019-12-12.
//  Copyright © 2019 Felix Andersson. All rights reserved.
//

import Foundation

internal class GraphLoader {
    func getGraph(fromFile: String, pixelHeight: Double) -> NavGraph {
        return GraphDeserializer.deserialize(fromJsonFile: fromFile, pixelHeight: pixelHeight)
    }
    
    func getGraph(fromData: Data, pixelHeight: Double) -> NavGraph {
        return GraphDeserializer.deserialize(fromJsonData: fromData, pixelHeight: pixelHeight)
    }
}
