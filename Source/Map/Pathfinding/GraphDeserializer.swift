//
//  GraphDeserializer.swift
//  MapLibrary
//
//  Created by Emil Bond on 2019-12-11.
//  Copyright © 2019 Felix Andersson. All rights reserved.
//

import Foundation
import UIKit

private typealias VertexMap = Dictionary<String, NavGraph.Vertex>
private typealias JsonData = Dictionary<String, AnyObject>
private typealias NeighborData = Dictionary<String, Int>

internal class GraphDeserializer: NSObject {
    internal static func deserialize(fromJsonFile: String, pixelHeight: Double) -> NavGraph{
        let filePath = Bundle.main.path(forResource: fromJsonFile, ofType: "json")!
        let data = try! Data(referencing: NSData(contentsOfFile: filePath))
        return deserialize(fromJsonData: data, pixelHeight: pixelHeight)
    }
    
    internal static func deserialize(fromJsonData data: Data, pixelHeight: Double) -> NavGraph{
        let json = try! JSONSerialization.jsonObject(with: data,
                                                     options: JSONSerialization.ReadingOptions.allowFragments) as! JsonData
        let numvertices = json["num_vertices"] as! Int
        let spacing = json["spacing"] as! Int
        var output = VertexMap()
        let polygons = json["vertices"] as! [JsonData]
        for object in polygons{
            let id = object["id"] as! String
            let x = object["x"] as! CGFloat
            let y = object["y"] as! CGFloat
            let cost = object["cost"] as! Int
            let neighborCost = object["neighbors"] as! NeighborData
            
            let vertice = NavGraph.Vertex(id: id, x: x, y: CGFloat(pixelHeight) - y, cost: cost, neighborCosts: neighborCost)
            output.updateValue(vertice, forKey: id)
        }
        
        return NavGraph(spacing: spacing, numVertices: numvertices, vertices: output)
    }
}
