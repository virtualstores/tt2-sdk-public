//
//  UserMarkerAnnotation.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-03-23.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Mapbox

/**
 The Mapbox annotation that represents the user.
 
 To customize the look of the user marker, implement the UserMarkerViewProtocol.
 */
public class UserMarkerAnnotation: MGLPointAnnotation {
    public var location: CLLocation {
        didSet {
            self.coordinate = location.coordinate
        }
    }

    override init() {
        self.location = CLLocation()
        super.init()
        self.coordinate = location.coordinate
        self.title = "User"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
