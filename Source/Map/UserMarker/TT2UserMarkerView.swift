//
//  DefaultUserMarkerView.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-03-23.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Mapbox

/**
 Default user marker view.
 
 Is displayed as a transparent blue circle that expands with the uncertainty. An arrow will point in the direction of the user.
 */
class TT2UserMarkerView: MGLAnnotationView, UserMarkerViewProtocol {
    private var userMarkerAnnotation: UserMarkerAnnotation
    
    private let arrowLayer: CAShapeLayer
    private let innerLayer: CAShapeLayer
    private let dotView: CAShapeLayer //= UIView(frame: CGRect(origin: .zero, size: CGSize(width: 20, height: 20)))
    private let dotAccentView: CAShapeLayer //= UIView(frame: CGRect(origin: .zero, size: CGSize(width: 14, height: 14)))
    
    private let REAL_METERS_PER_LATITUDE = 111320.0 // 1 latitude degree = 111.32 km
    private let converter = MapboxCoordinateConverter(withMeterFactor: 50.0)
    
    private var scale: CGFloat = 1.0
    private var tilt: CGFloat = 0.0
    private var rotation: CGFloat = 0.0
    private var uncertainty: CGFloat = 0.0
    private var uncertaintyScale: CGFloat {
        max((uncertainty / 20.0), 1.0)
    }
    
    private var mapRotation: CGFloat = 0.0
    
    private var metersPerPoint: Double = 0.0
    
    private let color = UIColor.systemOrange
    
    required init(annotation: UserMarkerAnnotation) {
        self.userMarkerAnnotation = annotation
        self.arrowLayer = .init()
        self.innerLayer = .init()
        self.dotView = .init()
        self.dotAccentView = .init()
        
        super.init(frame: .init(x: 0.0, y: 0.0, width: 20.0, height: 20.0))
        
//        dotView.layer.cornerRadius = dotView.bounds.width / 2
//        dotView.layer.backgroundColor = UIColor.white.cgColor
//        self.addSubview(dotView)
//
//        dotAccentView.center = dotView.bounds.center
//        dotAccentView.layer.cornerRadius = dotAccentView.bounds.width / 2
//        dotAccentView.layer.backgroundColor = color.cgColor
//        dotView.addSubview(dotAccentView)
        
        self.updateArrowLayer()
        self.dotView.addSublayer(arrowLayer)
        self.updateInnerLayer()
        self.layer.addSublayer(innerLayer)
        self.layer.addSublayer(dotView)
        self.dotView.addSublayer(dotAccentView)
        
//        self.backgroundColor = color.withAlphaComponent(0.2)
        
        self.isUserInteractionEnabled = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = bounds.width / 2
    }
    
    func mapViewDidChange(_ mapView: MGLMapView, scale: CGFloat) {
        self.scale = scale
        self.tilt = max(0, mapView.camera.pitch) * .pi / 180
        self.mapRotation = CGFloat(TT2Course(fromDegrees: -mapView.camera.heading).radians)
        
        self.metersPerPoint = mapView.metersPerPoint(atLatitude: userMarkerAnnotation.location.coordinate.latitude)
        
        self.updateTransform()
    }
    
    func userLocationDidChange(_ userLocation: TT2Location) {
        self.rotation = CGFloat(userLocation.course.radians)
//        self.uncertainty = CGFloat(5.0 * userLocation.radius / (metersPerPoint/1000.0)) // TODO: gör bättre
//        self.uncertainty = CGFloat(userLocation.radius * (REAL_METERS_PER_LATITUDE / (1000.0 / 50.0)) / 1000)
//        self.uncertainty = CGFloat(userLocation.radius / (metersPerPoint / 1000.0))
        self.uncertainty = CGFloat(converter.convertFromMapCoordinate(userLocation.radius))
//        print(uncertainty)
//        print(uncertaintyScale)
        self.updateTransform()
    }
    
    private func updateArrowLayer() {
        let arrowPath = UIBezierPath()
        let radius = self.bounds.width / 2
        let midAngle: CGFloat = .pi * 1.5
        let arrowSize = CGSize(width: 12, height: 5.0)
        let offsetAnglePoint = CGPoint(x: arrowSize.width/2, y: arrowSize.height)
        let offsetAngle: CGFloat = cos(atan2(offsetAnglePoint.x, offsetAnglePoint.y))
        
        arrowPath.addArc(withCenter: .zero, radius: radius, startAngle: midAngle - offsetAngle, endAngle: midAngle + offsetAngle, clockwise: true)
        arrowPath.addLine(to: CGPoint(x: 0, y: -(radius + arrowSize.height)))
        arrowPath.close()
        
        arrowLayer.path = arrowPath.cgPath
        arrowLayer.position = .init(x: bounds.midX, y: bounds.midY)
        arrowLayer.fillColor = color.cgColor
    }
    
    private func updateInnerLayer() {
        let inset: CGFloat = 1.5
        let path = UIBezierPath(arcCenter: .zero, radius: (self.bounds.width / 2) - inset, startAngle: 0, endAngle: .pi * 2, clockwise: true)
        innerLayer.path = path.cgPath
        innerLayer.position = CGPoint(x: self.bounds.width - bounds.midX, y: self.bounds.height - bounds.midY)
        innerLayer.fillColor = color.withAlphaComponent(0.4).cgColor
        
        let dotPath = UIBezierPath(ovalIn: bounds.insetBy(dx: inset, dy: inset))
        dotView.path = dotPath.cgPath
        dotView.position = .zero
        dotView.fillColor = UIColor.white.cgColor
        
        let dotAccentInset: CGFloat = inset + 3.0
        let dotAccentPath = UIBezierPath(ovalIn: bounds.insetBy(dx: dotAccentInset, dy: dotAccentInset))
        dotAccentView.path = dotAccentPath.cgPath
        dotAccentView.position = .zero
        dotAccentView.fillColor = color.cgColor
    }
    
    private func updateTransform() {
        var transform = CATransform3DIdentity
        
        // Rotate
        transform = CATransform3DRotate(transform, self.rotation + self.mapRotation, 0, 0, 1)
        
        // Scale
        transform = CATransform3DScale(transform, scale, scale, 1.0)
        
//        // Uncertainty scale
//        transform = CATransform3DScale(transform, uncertaintyScale, uncertaintyScale, 1.0)
        updateArrowLayer()
        updateInnerLayer()
        
        // Tilt
        transform = CATransform3DRotate(transform, tilt, 1.0, 0.0, 0.0)
        transform = CATransform3DTranslate(transform, 0, -tilt / 20, 0)
        transform = CATransform3DTranslate(transform, 0, -tilt / 12, 0)
        
        layer.transform = transform
        
        var testTransform = CATransform3DIdentity
        testTransform = CATransform3DScale(testTransform, uncertaintyScale/* * scale*/, uncertaintyScale/* * scale*/, 1.0)
        self.innerLayer.transform = testTransform
    }
}

public class MapboxCoordinateConverter: NSObject {
    
    private let meterFactor: Double // Map cms "meters" scale
    private var factor: Double {
        pixelFactor / meterFactor
    }
    private let pixelFactor: Double = 1000.0 // = 1 deg
    private var realMeterFactor: Double {
        111320 / meterFactor // = 1 deg
    }
    // 1000 pixels = 1 deg
    // 59 pixels = ~1 meters
    
    public init(withMeterFactor meterFactor: Double = 59.0) {
        self.meterFactor = meterFactor
    }
    
    public func convertToMapCoordinate(_ input: Double) -> Double {
        return input / factor
    }
    
    public func convertFromMapCoordinate(_ input: Double) -> Double {
        return input * factor
    }
    
    // From deg
    public func convertToPixel(_ input: Double) -> Double {
        return input * pixelFactor
    }
    
    // To deg
    public func convertFromPixel(_ input: Double) -> Double {
        return input / pixelFactor
    }
    
    
    public func convertToMeter(_ input: Double) -> Double {
        return input * meterFactor
    }
    
    public func convertFromMeter(_ input: Double) -> Double {
        return input / meterFactor
    }
}
