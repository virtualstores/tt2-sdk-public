//
//  UserMarkerView.swift
//  tt2-sdk
//
//  Created by Philip Fryklund on 2020-02-28.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Mapbox

/**
 Protocol for implementing a custom user marker view.
 */
public protocol UserMarkerViewProtocol: MGLAnnotationView {
    init(annotation: UserMarkerAnnotation)
    
    /// Update user marker UI when mapview camera updates zoom, tilt etc
    
    /**
     This function will be called every time the map changes. This is useful if you want to rotate or scale the user marker with the camera.
     
     - Parameter mapView: The map view that this user marker is attached to.
     */
    func mapViewDidChange(_ mapView: MGLMapView, scale: CGFloat)
    
    /**
     This function will be called when the user location is updated.
     
     - Parameter userLocation: The user location object.
     */
    func userLocationDidChange(_ userLocation: TT2Location)
}
