//
//  TT2ClusterView.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-07-27.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Mapbox
import ClusterKit

/**
 Default cluster view.
 
 Is displayed as a circle with a shadow containing a number the counts the markers in this cluster.
 */
class TT2ClusterView: MGLAnnotationView, ClusterViewProtocol {
    let amountLabel = UILabel()
    
    private let backgroundView = UIView()
    
    required public init(cluster: CKCluster) {
        super.init(annotation: cluster, reuseIdentifier: nil)
        
        self.frame = .init(x: 0.0, y: 0.0, width: 45.0, height: 45.0)
        
        self.layer.shadowOffset = .zero
        self.layer.shadowOpacity = 0.4
        
        self.backgroundView.frame = self.frame
        self.backgroundView.backgroundColor = .white
        self.backgroundView.layer.cornerRadius = self.frame.width/2.0
        
        self.addSubview(backgroundView)
        
        self.amountLabel.frame = self.frame
        self.amountLabel.text = "\(cluster.count)"
        self.amountLabel.font = UIFont.systemFont(ofSize: 20.0, weight: .semibold)
        self.amountLabel.textAlignment = .center
        
        self.addSubview(amountLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
