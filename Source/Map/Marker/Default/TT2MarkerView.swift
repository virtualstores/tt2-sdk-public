//
//  TT2MarkerView.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-03-24.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Mapbox
import Domain


/**
 Default marker view.
 
 Is displayed as a bubble with a shadow containing an image icon of the point of interest image.
 */
class TT2MarkerView: MGLAnnotationView, MarkerViewProtocol {
    public var iconView: UIImageView
    
    required public init(pointOfInterest: PointOfInterest) {
        self.iconView = UIImageView()
        
        super.init(reuseIdentifier: pointOfInterest.id)
        self.frame = .init(x: 0.0, y: 0.0, width: 42.0, height: 50.0)
        
        self.centerOffset = .init(dx: 0.0, dy: -self.frame.height/2.0)
        
        self.isOpaque = false
        self.layer.shadowOpacity = 0.4
        self.layer.shadowOffset = .zero
        
        // TODO: Cacha bilder (använd bibliotek?)
        if let imageUrl = pointOfInterest.imageUrl {
            DispatchQueue.global().async { [weak self] in
                if let data = try? Data(contentsOf: imageUrl) {
                    if let image = UIImage(data: data) {
                        DispatchQueue.main.async {
                            self?.iconView.image = image
                        }
                    }
                }
            }
        }
        
        iconView.frame = .init(x: 0, y: 0, width: 42, height: 42)
        iconView.frame = iconView.frame.insetBy(dx: 7.0, dy: 7.0)
        self.addSubview(iconView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func wasSelected() {
        UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
            self.transform = CGAffineTransform(translationX: 0.0, y: -7.0).concatenating(.init(scaleX: 1.4, y: 1.4))
        }, completion: nil)
    }
    
    public func wasDeselected() {
        UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
            self.transform = .init(scaleX: 1.0, y: 1.0)
        }, completion: nil)
    }
    
    public override func draw(_ rect: CGRect) {
        let bezierPath = UIBezierPath()
        
        // Ändra?
        bezierPath.move(to: CGPoint(x: 35.87, y: 35.35))
        bezierPath.addCurve(to: CGPoint(x: 21, y: 50), controlPoint1: CGPoint(x: 31.77, y: 39.39), controlPoint2: CGPoint(x: 21, y: 50))
        bezierPath.addLine(to: CGPoint(x: 6.12, y: 35.35))
        bezierPath.addLine(to: CGPoint(x: 6.15, y: 35.37))
        bezierPath.addCurve(to: CGPoint(x: 6.15, y: 6.12), controlPoint1: CGPoint(x: -2.05, y: 27.29), controlPoint2: CGPoint(x: -2.05, y: 14.2))
        bezierPath.addCurve(to: CGPoint(x: 35.85, y: 6.12), controlPoint1: CGPoint(x: 14.35, y: -1.96), controlPoint2: CGPoint(x: 27.65, y: -1.96))
        bezierPath.addCurve(to: CGPoint(x: 35.85, y: 35.37), controlPoint1: CGPoint(x: 44.05, y: 14.2), controlPoint2: CGPoint(x: 44.05, y: 27.29))
        bezierPath.addLine(to: CGPoint(x: 35.87, y: 35.35))
        bezierPath.close()

        UIColor.white.setFill()
        bezierPath.fill()
    }
}
