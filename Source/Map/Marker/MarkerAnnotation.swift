//
//  MarkerAnnotation.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-03-23.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Mapbox
import MapKit
import Domain

/**
 A marker is an annotation on the map which is associated with a point of interest.
 
 By default, the marker will be displayed with the TT2MarkerView-class. To customize how the marker is displayed, implement the MarkerViewProtocol.
 */
public class MarkerAnnotation: MGLPointAnnotation {
    public private(set) var id: String
    public private(set) var pointOfInterest: PointOfInterest

    init(pointOfInterest: PointOfInterest) {
        self.id = pointOfInterest.id
        self.pointOfInterest = pointOfInterest
        super.init()
        self.title = pointOfInterest.name
        self.subtitle = pointOfInterest.subtitle
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
