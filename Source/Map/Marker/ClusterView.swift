//
//  ClusterView.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-07-27.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Mapbox
import ClusterKit

/**
 Protocol for implementing custom cluster views.
 
 Clusters appear when several markers are close to each other. Information about the cluster can be retrieved from the CKCluster-object.
 */
public protocol ClusterViewProtocol: MGLAnnotationView {
    /**
     Initializes the custom cluster view.
     
     - Parameter cluster: The cluster which this view is displaying. This object contains information about the amount of markers in the cluster and which markers are in the cluster.
     */
    init(cluster: CKCluster)
}
