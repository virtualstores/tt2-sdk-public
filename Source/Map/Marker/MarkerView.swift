//
//  MarkerView.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-03-23.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Mapbox
import Domain

/**
 Protocol for implementing custom marker views.
 
 A marker is always associated with a point of interest, which means that you can use the information from the point of interest to display the marker. You can also listen to events when the user selects and deselects the marker.
 */
public protocol MarkerViewProtocol: MGLAnnotationView {
    /**
     Initializes a marker view.
     
     - Parameter pointOfInterest: The point of interest associated with this marker.
     */
    init(pointOfInterest: PointOfInterest)
    
    /**
     Gets called when the user selects the marker view.
     
     This can happen programatically or when the user taps this view.
     */
    func wasSelected()
    
    /**
     Gets called when the user deselects the marker view.
     
     This can happen programmatically or when the user selects another marker or taps the map background.
     */
    func wasDeselected()
}
