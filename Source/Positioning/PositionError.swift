//
//  PositionError.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-03-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

/**
 Errors that can be produced by the position manager.
 */
public enum PositionError: Error {
    /**
     This means that the position manager does not know where the user is anymore.
     */
    case lostPosition
    
    /**
     This means that the user has done something incorrectly for the position manager to work. The user should be notified to prevent further errors.
     */
    case illegalBehavior
    
    /**
     This means that the position manager failed to upload data to an external server.
     
     Will attach a more detailed error message as an associated value.
     */
    case uploadFailure(Error)
    
    case trolley
}
