//
//  PositionState.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-04-20.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

/**
 The state of a position manager.
 */
public enum PositionState {
    /**
     The position manager has started synchronizing.
     */
    case beginSync
    
    /**
     The position manager is ready to be used.
     */
    case ready
    
    /**
     The position manager has been stopped,
     */
    case stopped
    
    /**
     The position manager has been started.
     */
    case started
    
    /**
     The position manager has finished uploading. If there was an error, the associated value will be set.
     */
    case uploadedRecording(Error?)
    
    case sendData
}
