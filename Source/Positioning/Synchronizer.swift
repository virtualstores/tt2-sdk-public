//
//  Synchronizer.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-03-30.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

/**
 A protocol that can be used to build something that can synchronize a position manager.
 */
public protocol Synchronizer {
    /**
     The type of the input to this synchronizer.
     */
    associatedtype Input
    
    /**
     The position manager associated with this synchronizer.
     */
    var positionManager: PositionManager { get }
    
    /**
     Synchronize the position manager with a input. Calls the optional completion handler when finished.
     
     - Parameter input: The input that should be used when synchronizing.
     - Parameter completion: Will be called when synchronization is complete.
     */
    func synchronize(with input: Input, completion: ((Error?) -> Void)?)
    
    /**
     Returns a TT2Location for a input.
     
     - Parameter input: The input that we want to know the location of.
     - Parameter completion: Will be called when we have determined the location of this input. The parameter contains the location if successfull or a error if not.
     */
    func position(for input: Input, completion: ((Result<TT2Location, Error>) -> Void)?)
}

public extension Synchronizer {
    func synchronize(with input: Input, completion: ((Error?) -> Void)?) {
        self.position(for: input) { result in
            switch result {
            case .success(let location):
                self.positionManager.synchronize(location: location)
                completion?(nil)
            case .failure(let error):
                completion?(error)
            }
        }
    }
}
