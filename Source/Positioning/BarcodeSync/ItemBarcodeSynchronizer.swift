//
//  ItemBarcodeSynchronizer.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-04-16.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Domain

/**
 Errors for the ItemBarcodeSynchronizer.
 */
public enum ItemBarcodeSynchronizerError: Error {
    /**
     The barcode was not valid. Usually means that the item API could not find a item with the barcode.
     */
    case invalidItem
    
    /**
     The item associated with the barcode does not have a position in the item API, so we could not synchronize.
     */
    case noPosition
}

/**
 Implementation of a Synchronizer that works on barcodes and is connected to the item API. This can be used to synchronize the user location with barcodes.
 */
public class ItemBarcodeSynchronizer: Synchronizer {
    public typealias Input = String
    
    private let itemApi: ItemApi
    private let navigation: Navigation
    public let positionManager: PositionManager
    
    /**
     Initializes a item barcode synchronizer with a item API.
     
     - Parameter itemApi: The item API to use when determining the location of items.
     - Parameter navigationSpace: The navigation space that we are navigating in.
     - Parameter positionManager: The position manager that determines the user location. This will be updated when the synchronize function is called.
     */
    public init(itemApi: ItemApi, navigationSpace: NavigationSpace, positionManager: PositionManager) {
        self.itemApi = itemApi
        self.navigation = navigationSpace.navigation
        self.positionManager = positionManager
    }
    
    /**
     Determines the location for a barcode.
     
     - Parameter input: The barcode as a string.
     - Parameter completion: This completion handler will be called when the synchronizer knows the location of the barcode. It will recieve a result with either the location or an error.
     */
    public func position(for input: String, completion: ((Result<TT2Location, Error>) -> Void)?) {
        itemApi.getItemBy(id: input) { result in
            switch result {
            case .success(let item):
                if let shelfTierIds = item.shelfTierIds, let firstShelf = shelfTierIds.first, let itemPosition = self.navigation.getItemPosition(shelfTierId: firstShelf) {
                    let scaleFactor = 1.0 / Double(TT2Position.scale)
                    let angle = atan2(itemPosition.offsetY, itemPosition.offsetX)

                    let position = TT2Position(x: Double(itemPosition.x)*scaleFactor, y: Double(itemPosition.y)*scaleFactor)
                    let course = TT2Course(fromRadians: Double(angle))

                    let location = TT2Location(position: position, course: course, confidence: 0.0, radius: 0.0, validSync: false, timestamp: .init())
                    completion?(.success(location))
                }
                else {
                    completion?(.failure(ItemBarcodeSynchronizerError.noPosition))
                }
            case .failure:
                completion?(.failure(ItemBarcodeSynchronizerError.invalidItem))
            }
        }
    }
}
