//
//  BarcodeScanner.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-03-28.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import UIKit
import AVFoundation
import MLKitVision
import MLKitBarcodeScanning

/**
 Errors that can occur with the barcode scanner.
 */
public enum BarcodeScannerError: LocalizedError {
    /**
     The user did not grant permission to use the camera.
     */
    case noPermission
    
    /**
     This device has no valid camera.
     */
    case noCamera
    
    /**
     The barcode scanner was unable to output video from the camera.
     */
    case videoOutput
    
    /**
     The barcode scanner was unable to ouput metadata from the camera.
     */
    case metadataOutput
}

/**
 Delegate for customizing the barcode scanner.
 */
public protocol BarcodeScannerDelegate {
    /**
     This function will be called whenever the barcode scanned has successfully read a code. When this function is finished, it should call the completion handler and notify the barcode scanner if it was a valid scan. This is so that the same code will not be notified again and again.
     
     - Parameter barcode: The barcode that was read as a string.
     - Parameter completion: A completion handler which should be called when this function is finished. Pass a parameter if the function was successful or not.
     */
    func scanned(barcode: String, completion: @escaping (_ wasSuccessful: Bool) -> Void)
}

/**
 This view can has a camera which will scan for barcodes. When a barcode is successfully read, it will notify its delegate.
 
 You can set which barcode formats this view will recognize in initializeCapturing().
*/
public class BarcodeScannerView: UIView {
    private var metadataOutput: AVCaptureMetadataOutput?
    private var barcodeDetector: BarcodeScanner?
    private var isDetecting: Bool = false
    
    private var scannedBarcodes: Set<String> = []
    private var latestDetect: Date = .distantPast
    private let detectRate: TimeInterval = 1/5
    private var scanArea: CGRect?
    private var cutoutLayer: CALayer?
    private var scanAreaBorderLayer: CAShapeLayer?
    private var featureLayer: CAShapeLayer?
    
    public var delegate: BarcodeScannerDelegate?
    
    private var scanAreaActive = false {
        didSet {
            if scanAreaActive != oldValue {
                let animation = CABasicAnimation(keyPath: "strokeColor")
                animation.fromValue = oldValue ? UIColor.white.cgColor : UIColor.white.withAlphaComponent(0.4).cgColor
                animation.toValue = scanAreaActive ? UIColor.white.cgColor : UIColor.white.withAlphaComponent(0.4).cgColor
                animation.duration = detectRate
                animation.repeatCount = 0
                animation.fillMode = .forwards
                animation.isRemovedOnCompletion = false
                
                scanAreaBorderLayer?.add(animation, forKey: "strokeColor")
            }
        }
    }
    
    public override class var layerClass: AnyClass {
        AVCaptureVideoPreviewLayer.self
    }
    
    
    final var previewLayer: AVCaptureVideoPreviewLayer {
        self.layer as! AVCaptureVideoPreviewLayer
    }
    
    
    public var isCaptureSessionInitialized: Bool {
        captureSession != nil
    }
    
    public var isCapturing: Bool {
        captureSession?.isRunning == true
    }
    
    private var captureSession: AVCaptureSession? {
        previewLayer.session
    }
    
    var videoOutput: AVCaptureVideoDataOutput?
    
    func extraInputs(in session: AVCaptureSession) throws {
        metadataOutput = AVCaptureMetadataOutput()
        guard let metadataOutput = self.metadataOutput, session.canAddOutput(metadataOutput) else {
            throw BarcodeScannerError.metadataOutput
        }
        
        videoOutput!.setSampleBufferDelegate(self, queue: .main)
    }
        
    private func initializeScanArea(area: CGRect) {
        cutoutLayer?.removeFromSuperlayer()
        
        let cutout = UIBezierPath(roundedRect: area, cornerRadius: 16.0)
        let overlay = UIBezierPath(rect: bounds)
        overlay.append(cutout.reversing())
        
        let layer = CAShapeLayer()
        layer.path = overlay.cgPath
        layer.fillColor = UIColor.black.withAlphaComponent(0.7).cgColor
        layer.position = .zero
        
        self.layer.addSublayer(layer)
        cutoutLayer = layer
        
        let borderWidth: CGFloat = 4.0
        let border = UIBezierPath(roundedRect: area.insetBy(dx: -borderWidth/2.0, dy: -borderWidth/2.0), cornerRadius: 16.0 + borderWidth/2.0)
        let borderLayer = CAShapeLayer()
        borderLayer.strokeColor = UIColor.white.withAlphaComponent(0.4).cgColor
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.lineWidth = borderWidth
        borderLayer.path = border.cgPath
        
        self.layer.addSublayer(borderLayer)
        scanAreaBorderLayer = borderLayer
        scanAreaActive = false
        
        let featureLayer = CAShapeLayer()
        featureLayer.strokeColor = UIColor.white.withAlphaComponent(0.8).cgColor
        featureLayer.fillColor = UIColor.clear.cgColor
        featureLayer.lineWidth = 2.0
        self.featureLayer = featureLayer
        
        self.layer.addSublayer(featureLayer)
    }
    
    /**
     Initializes this view. Calls the completion handler when finished.
     
     - Parameter scanArea: The area in which valid barcodes can be scanned. If nil, the entire camera will be used.
     - Parameter barcodeFormats: The barcode formats you want to recognize. Only these formats will be captured. Common values can be [.EAN13, qrCode, .code128].
     - Parameter completion: This function will be called when finished. If there was an error, the parameter will be set, otherwise it will be nil.
     */
    public func initializeCapturing(scanArea: CGRect?, barcodeFormats: BarcodeFormat, completion: @escaping (BarcodeScannerError?) -> Void) {
        let barcodeOptions = BarcodeScannerOptions(formats: barcodeFormats)
        self.scanArea = scanArea
        
        if let scanArea = scanArea {
            self.initializeScanArea(area: scanArea)
        }
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .notDetermined: // The user has not yet been asked for camera access.
            AVCaptureDevice.requestAccess(for: .video) { granted in
                DispatchQueue.main.async {
                    granted ? self.initializeCapturing(scanArea: scanArea, barcodeFormats: barcodeFormats, completion: completion) : completion(.noPermission)
                }
            }
        case .denied, .restricted:
            completion(.noPermission)
        default:
            break
        }
        
        let session = AVCaptureSession()
        session.beginConfiguration()
        
        // Camera input
        guard
            let device = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back),
            let deviceInput = try? AVCaptureDeviceInput(device: device), session.canAddInput(deviceInput)
        else {
            return completion(.noCamera)
        }
        session.addInput(deviceInput)
        
        // Video data output
        let videoOutput = AVCaptureVideoDataOutput()
        videoOutput.alwaysDiscardsLateVideoFrames = true
        
        guard session.canAddOutput(videoOutput) else {
            return completion(.videoOutput)
        }
        session.addOutput(videoOutput)
        
        videoOutput.connection(with: .video)
        self.videoOutput = videoOutput
        do {
            try extraInputs(in: session)
        }
        catch let error as BarcodeScannerError {
            completion(error)
        }
        catch {
            assertionFailure()
        }
        
        session.sessionPreset = .hd1920x1080
        session.commitConfiguration()
            
        previewLayer.session = session
        previewLayer.videoGravity = .resizeAspectFill
        previewLayer.connection!.videoOrientation = .portrait
        
        DispatchQueue.main.async {
            self.resumeCapture()
        }
        
        // Initialize Firebase Vision
        barcodeDetector = BarcodeScanner.barcodeScanner(options: barcodeOptions)
        
        completion(nil)
    }
    
    /**
     Pauses the camera and the barcode scanner.
     */
    public func pauseCapture() {
        captureSession?.stopRunning()
    }
    
    /**
     Resumes the camera and the barcode scanner.
     */
    public func resumeCapture() {
        captureSession?.startRunning()
    }
}

extension BarcodeScannerView: AVCaptureVideoDataOutputSampleBufferDelegate {
    public func captureOutput(
        _ output: AVCaptureOutput,
        didOutput sampleBuffer: CMSampleBuffer,
        from connection: AVCaptureConnection
    ) {
        guard let cvBuffer = CMSampleBufferGetImageBuffer(sampleBuffer), let barcodeDetector = self.barcodeDetector, !isDetecting, Date.init().timeIntervalSince(latestDetect) > detectRate else {
            return
        }
        
        self.latestDetect = Date.init()
        
        let image = VisionImage(buffer: sampleBuffer)
        image.orientation = .right // Device is always assumed to be in portrait and using back camera
        
        isDetecting = true
        barcodeDetector.process(image) { features, error in
            guard error == nil, let features = features, !features.isEmpty, let delegate = self.delegate else {
                self.isDetecting = false
                self.scanAreaActive = false
                return
            }
            
            // Uncomment the featurePaths-code if you want to see outlines of detected codes
//            let featurePaths = UIBezierPath()
            for feature in features {
                let withinArea: Bool
                if let corners = feature.cornerPoints, let scanArea = self.scanArea {
                    let bufferSize = CGRect(origin: .zero, size: .init(width: CVPixelBufferGetWidth(cvBuffer), height: CVPixelBufferGetHeight(cvBuffer)))
                    let normalizedScanArea = self.previewLayer.metadataOutputRectConverted(fromLayerRect: scanArea)
                    let bufferScanArea = CGRect(x: normalizedScanArea.origin.x * bufferSize.size.width, y: normalizedScanArea.origin.y * bufferSize.size.height, width: normalizedScanArea.width * bufferSize.size.width, height: normalizedScanArea.height * bufferSize.size.height)
                    
                    let points = corners.map { $0.cgPointValue }
                    withinArea = points.allSatisfy { bufferScanArea.contains($0) }
                    
//                    let path = UIBezierPath()
//                    points.map({
//                        self.previewLayer.layerPointConverted(fromCaptureDevicePoint: CGPoint(x: $0.x / bufferSize.width, y: $0.y / bufferSize.height))
//                    }).enumerated().forEach { (i, p) in
//                        if i == 0 {
//                            path.move(to: p)
//                        } else {
//                            path.addLine(to: p)
//                        }
//                    }
//                    path.close()
//
//                    featurePaths.append(path)
                }
                else {
                    withinArea = true
                }
                
                guard withinArea else {
                    self.isDetecting = false
                    continue
                }
                
                self.scanAreaActive = true
                
                if let barcode = feature.rawValue, !self.scannedBarcodes.contains(barcode) {
                    self.scannedBarcodes.insert(barcode)
                    self.pauseCapture()
                    delegate.scanned(barcode: barcode) { [weak self] wasSuccessful in
                        self?.resumeCapture()
                        if wasSuccessful {
                            self?.scannedBarcodes.remove(barcode)
                        }
                        else {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                                self?.scannedBarcodes.remove(barcode)
                            })
                        }
                    }
                }
            }
            
//            self.featureLayer?.path = featurePaths.cgPath
            
            self.isDetecting = false
        }
    }
}
