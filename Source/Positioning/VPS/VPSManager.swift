//
//  VPSManager.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-03-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import PositionKit
import qps
import AWSS3

/**
 This is a position manager implementation for the VPS (Virtual Positioning System). The VPS uses the sensors in the phone to detect where the user is in a indoor space. The accuracy can be improved by synchronizing the user's location. In a store setting, this is best done with barcode scanning.
 
 The VPS requires a mapfence to work properly. This describes the layout of the space so that the positioning system knows how the space looks like.
 
 - Note:
 Since the iPhone will automatically turn off the sensors 30 seconds after exiting the app, the VPS will automatically be turned off when this happens.
 */
public class VPSManager: PositionManager {
    public private(set) var lastLocation: TT2Location?
    public private(set) var isUpdatingPosition: Bool = false
    
    public private(set) var state: PositionState
    
    private let positionKit: PositionKitImpl
    private var nilBundles: Int = 0
    
    private var currentDirectionOffset = TT2Course(fromRadians: 0.0)
    
    private var latestExit: Date?
    
    /**
     Initializes the VPS with a mapfence. The mapfence describes the layout of the space so that the positioning system knows how the space looks like.
     
     - Parameter mapFence: The mapfence in JSON-format as a Data object. This can be retrieved from the navigation space object.
     */
    public init(with mapFence: Data) {
        state = .stopped
        
        positionKit = PositionKitImpl()
        if TT2Position.scale != 50.0 {
            positionKit.setScale(scale: CGPoint(x: CGFloat(TT2Position.scale), y: CGFloat(TT2Position.scale)))
        }
        positionKit.setMapFence(mapData: mapFence)
        positionKit.shouldRecord = true
        //positionKit.shouldPublishToS3(allow: true)
        positionKit.addObserver(observer: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleAppExited), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleAppReturned), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    public func startUpdatingLocation() {
        guard !isUpdatingPosition else {
            return
        }
        
        positionKit.start()
        positionKit.startRecording()
    }

    private func startUpdatingLocation(at position: TT2Position, with course: TT2Course) {
        notifyStateObservers(with: .beginSync)
        TT2.shared.date = Date()
        currentDirectionOffset = .init(fromRadians: course.radians)
        startUpdatingLocation()
        isUpdatingPosition = true
        positionKit.startNavigation(startPosition: .init(x: Float(position.x), y_: Float(position.y)), startAngle: course.degeees)
        notifyStateObservers(with: .started)
    }
    
    public func synchronize(location: TT2Location) {
        if isUpdatingPosition {
            let x: Float
            let y: Float
            
            if location.syncDirection {
                notifyStateObservers(with: .beginSync)
                x = Float(cos(location.course.radians + Double.pi))
                y = Float(sin(location.course.radians))
                currentDirectionOffset = .init(fromRadians: location.course.radians)
            }
            else {
                x = Float(cos(currentDirectionOffset.radians))
                y = Float(sin(currentDirectionOffset.radians))
            }
            
            if location.syncDirection {
                currentDirectionOffset = .init(fromRadians: location.course.radians)
            }
            
            positionKit.setPosition(point: .init(x: Float(location.position.x), y_: Float(location.position.y)), direction: .init(x: x, y_: y), syncDirection: location.syncDirection)
        }
        else {
            startUpdatingLocation(at: location.position, with: location.course)
        }
    }
    
    public func stopUpdatingLocation(uploadRecording: Bool = true) {
        if uploadRecording {
            notifyStateObservers(with: .beginSync)
            positionKit.stopRecording()
        }
        isUpdatingPosition = false
        positionKit.stop()
        lastLocation = nil
        notifyStateObservers(with: .stopped)
    }
    
    private let locationObservers: Observable<LocationObserverType> = Observable()
    private let stateObservers: Observable<PositionState> = Observable()
    
    /// - Returns: Token which must be stronly referenced by the caller to receive notifications
    public func observeLocation(observer: @escaping (LocationObserverType) -> Void) -> Any {
        locationObservers.observe(observer)
    }
    
    public func observeState(observer: @escaping (PositionState) -> Void) -> Any {
        stateObservers.observe(observer)
    }
    
    private func notifyLocationObservers(with value: LocationObserverType) {
        locationObservers.notify(value)
    }
    
    private func notifyStateObservers(with value: PositionState) {
        if case PositionState.uploadedRecording = value {
            // Do not set state variable
        }
        else {
            state = value
        }
        
        stateObservers.notify(value)
    }
    
    @objc private func handleAppExited() {
        latestExit = .init()
    }
    
    @objc private func handleAppReturned() {
        guard let latestExit = latestExit else {
            return
        }
        
        if latestExit.timeIntervalSinceNow < -30.0 {
            stopUpdatingLocation(uploadRecording: false)
        }
    }
    
    public func sendData() {
        notifyStateObservers(with: .sendData)
    }
}

extension VPSManager: PositionKitObserver {
    public func sendDataToApplication(identifier: String, data: String) {
        TT2.shared.uploadManager.prepareDataToSend(identifier: identifier, data: data)
        notifyStateObservers(with: .uploadedRecording(nil))
    }
    
    public func notifyFileUploaded(_ error: Error?) {
        notifyStateObservers(with: .uploadedRecording(error))
    }
    
    public var id: String {
        UUID().uuidString
    }
    
    public func notifyNewPosition(_ positionBundle: PositionBundle) {
        guard let newPosition = positionBundle.position, let newRadius = positionBundle.radius, let newConfidence = positionBundle.confidence else {
            if (nilBundles > 5) {
                notifyLocationObservers(with: .failure(.lostPosition))
            }
            nilBundles += 1
            return
        }
        nilBundles = 0
        if var lastLocation = lastLocation {
            lastLocation.position = .init(x: Double(newPosition.x), y: Double(newPosition.y))
            lastLocation.confidence = newConfidence
            lastLocation.radius = newRadius
            lastLocation.timestamp = .init()
            self.lastLocation = lastLocation
            notifyLocationObservers(with: .success(lastLocation))
        }
        else {
            let location = TT2Location(position: .init(x: Double(newPosition.x), y: Double(newPosition.y)), course: .init(fromDegrees: 0.0), confidence: newConfidence, radius: newRadius, validSync: true, timestamp: .init())
            self.lastLocation = location
            print("NEW: (\(location.position.x), \(location.position.y))")
            notifyLocationObservers(with: .success(location))
        }
    }
    
    public func notifyNewDirection(_ directionBundle: DirectionBundle) {
        guard var lastLocation = self.lastLocation else {
            return
        }
        
        let direction = (Double.pi/2 - (currentDirectionOffset.radians + directionBundle.direction)).remainder(dividingBy: Double.pi*2.0)
        
        lastLocation.course = .init(fromRadians: direction)
        self.lastLocation = lastLocation
        notifyLocationObservers(with: .success(lastLocation))
    }
    
    public func notifyNewDeviceOrientation(_ orientation: IQPSDeviceOrientation) {
        switch orientation {
        case .swing:
            notifyLocationObservers(with: .failure(.illegalBehavior))
        case .trolley:
            notifyLocationObservers(with: .failure(.trolley))
        default:
            break
        }
    }
    
    public func notifyIllegalBehaviour() {
        notifyLocationObservers(with: .failure(.illegalBehavior))
    }
    
    public func notifyBadStepLength() {
        notifyLocationObservers(with: .failure(.illegalBehavior))
    }
    
    public func notifyLog(_ text: String) {
    }
    
    public func notifySensorsInitiated() {
        notifyStateObservers(with: .ready)
    }
}
