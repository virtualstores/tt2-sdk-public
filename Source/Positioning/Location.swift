//
//  Location.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-03-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Foundation
import CoreGraphics
import CoreLocation

// Should always be in meters with origin at top-left

/**
 A position in the navigation space in meters and with the origin at the top-left corner.
 */
public struct TT2Position {
    /**
     The x-value, i.e. how far away from the leftmost corner this position is.
     */
    public var x: Double = 0.0
    
    /**
     The y-value, i.e. how far away from the topmost corner this position is.
     */
    public var y: Double = 0.0
    
    /**
     Initializes a position with x and y values.
     */
    public init(x: Double, y: Double) {
        self.x = x
        self.y = y
    }
    
    /**
     The scale of the current navigation space. Is 50 by default.
     
     More specifically, this is the ratio of coordinates to meters. If the scale is 50, that means that 1 degree is 50 meters.
     */
    public static var scale: Float = 50.0
    
    /**
     The height of the navigation space. This is used to convert to/from positions with the origin at the bottom-left corner.
     
     This will be set by the MapController, which means it cannot be used before the MapController has been initialized.
     */
    public static var navigationSpaceHeight: Float = 0.0
    
    /**
     Converts the position to the unit used by the server.
     */
    public var inServerPixels: CGPoint {
        return CGPoint(x: x*Double(TT2Position.scale), y: y*Double(TT2Position.scale))
    }
    
    /**
     Converts the position to coordinates, which can be used by Mapbox.
     
     - Note: This cannot be used until the MapController has been initialized.
     */
    public var inCoordinates: CLLocationCoordinate2D {
        let factor: Double = Double(1000.0/TT2Position.scale)
        return CLLocationCoordinate2D(latitude: (Double(TT2Position.navigationSpaceHeight) - x)*factor, longitude: (Double(TT2Position.navigationSpaceHeight) - y)*factor)
    }
}

/**
 A course.
 */
public struct TT2Course {
    /**
     The course in radians.
     */
    public var radians: Double = 0.0
    
    /**
     The course in degrees.
     */
    public var degeees: Double {
        get {
            radians * (180.0 / .pi)
        }
        
        set {
            radians = newValue * (.pi / 180.0)
        }
    }
    
    /**
     Initializes a course with radians.
     
     - Parameter fromRadians: The value to initialize with in radians.
     */
    public init(fromRadians radians: Double) {
        self.radians = radians
    }
    
    /**
    Initializes a course with degrees.
    
    - Parameter fromDegrees: The value to initialize with in degrees.
    */
    public init(fromDegrees degrees: Double) {
        self.degeees = degrees
    }
}

/**
 A location that contains a position, a course and some values that determine how certain this location is.
 */
public struct TT2Location {
    /**
     The position in meters, relative to the top-left corner.
     */
    public var position: TT2Position
    
    /**
     The course in which this location is pointing towards.
     */
    public var course: TT2Course
    
    /**
     How confident we are of this location.
     */
    public var confidence: Double
    
    /**
     A radius in which the location is within.
     */
    public var radius: Double
    
    /**
     If this location is a valid sync location. Can be used when synchronizing a position manager.
     */
    public var validSync: Bool
    
    /**
     The time in which this location was valid. Can be nil.
     */
    public var timestamp: Date?
    public var syncDirection: Bool
    
    /**
     Initialize a location.
     
     - Parameter position: The position in meters, relative to the top-left corner.
     - Parameter course: The course in which this location is pointing towards.
     - Parameter confidence: How confident we are of this location.
     - Parameter radius: A radius in which the location is within.
     - Parameter validSync: If this location is a valid sync location. Can be used when synchronizing a position manager.
     - Parameter timestamp: The time in which this location was valid. Can be nil.
     */
    public init (position: TT2Position, course: TT2Course, confidence: Double, radius: Double, validSync: Bool, timestamp: Date?, syncDirection: Bool = false) {
        self.position = position
        self.course = course
        self.confidence = confidence
        self.radius = radius
        self.validSync = validSync
        self.timestamp = timestamp
        self.syncDirection = syncDirection
    }
}
