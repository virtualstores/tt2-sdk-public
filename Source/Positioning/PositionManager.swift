//
//  PositionManager.swift
//  tt2-sdk
//
//  Created by Jesper Lundqvist on 2020-03-02.
//  Copyright © 2020 Virtual Stores. All rights reserved.
//

import Domain

/**
 Protocol for classes that can manage the user location.
 */
public protocol PositionManager {
    /**
     The last known location of the user. Can be nil if we do not know.
     */
    var lastLocation: TT2Location? { get }
    
    /**
     If the position manager is currently updating the user postiion.
     */
    var isUpdatingPosition: Bool { get }
    
    /**
     The current state of the position manager.
     */
    var state: PositionState { get }
    
    /**
     Start updating the location of the user.
     
     This should set isUpdatingPosition to true and begin notifying observers that are interested in the user location.
     */
    func startUpdatingLocation()
    
    /**
     Synchronize the position manager with a currently known position.
     
     - Parameter location: The current location that we should update to.
     */
    func synchronize(location: TT2Location)
    
    /**
     Synchronize the position manager with a positioned code.
     
     This can occur when the user scans a code and we can safely update the location to the location of the code.
     
     - Parameter code: The positioned code. Needs to have non-nil coordinates and direction to successfully synchronize.
     */
    func synchronize(code: PositionedCode, syncDirection: Bool)
    
    /**
     Stop updating the user location.
     
     This should set isUpdatingPosition to false and stop notifying the location observers.
    */
    func stopUpdatingLocation(uploadRecording: Bool)
    
    typealias LocationObserverType = Result<TT2Location, PositionError>
    
    /**
     Can be used to observe the location of the user. The observer will be notified every time the location updates.
     
     - Parameter observer: The function that will be called when the location is updated. As a parameter, this will recieve the location if successful or an error if there was an failure.
     - Returns: An object that must be stored as a strong reference as long as we want to observe the location. When the object is released, the observer will not be notified any more.
     */
    func observeLocation(observer: @escaping (LocationObserverType) -> Void) -> Any
    
    /**
     Can be used to observe state changes in the position manager.
     
     - Parameter observer: The function that will be called when the state is changed.
     - Returns: An object that must be stored as a strong reference as long as we want to observe the state. When the object is released, the observer will not be notified any more.
    */
    func observeState(observer: @escaping (PositionState) -> Void) -> Any
    
    func sendData()
}

public extension PositionManager {
    func synchronize(code: PositionedCode, syncDirection: Bool = false) {
        guard let position = code.coordinates, let direction = code.direction else {
            return
        }
        
        let location = TT2Location(position: TT2Position(x: Double(position.x), y: Double(position.y)), course: TT2Course(fromDegrees: direction), confidence: 1.0, radius: 0.0, validSync: true, timestamp: Date(), syncDirection: syncDirection)
        synchronize(location: location)
    }
}
